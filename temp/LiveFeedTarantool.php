<?php

namespace Tvoydenvnik\Posts\Models;



use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

/*
 * Живая лента
 *
 * 1 - id автоикремент. Индексы primary
 * 2 - post_id
 * 3 - date Дата добавления. Или дата последнего добавленного комментария.
 * 4 - comments Массив id последних комментариев. Не более 10.
 *
 * Индексы:
 *
 * Для обновления date, при добавлении комменатриев
 *  post - post_id
 *
 * Вывод ленты. Удаление старых сообщений. Например, старше 30 дней.
 * date - date
 *
 */
class LiveFeedTarantool{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'live_feed';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'NUM'), true);
    }


    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}