<?php

namespace Tvoydenvnik\Posts\Tests;

use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\PostsCommonFeedService;

class PostsCommonFeedServiceTest  extends \PHPUnit_Framework_TestCase{



    /**
     * @var PostsCommonFeedService
     */
    private $feed = null;

    public function setUp()
    {

        $cache = new PostsCommonFeedService();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->feed =  $cache;

    }

    public function tearDown()
    {
        $this->feed->truncate();
        $this->feed = null;
    }


    public function testTruncate(){
        //$this->feed->truncate();
        $this->assertEquals($this->feed->spaceLength(), 0);
    }

    public function testAddPost(){

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->assertEquals($this->feed->spaceLength(), 1);

        $this->feed->addPost(EntityPost::create(array("id"=>2, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->assertEquals($this->feed->spaceLength(), 2);


        $this->feed->addPost(EntityPost::create(array("id"=>3, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>4, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>333334, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>5, "author_id"=>12, 'created_at'=>'2010-01-01 12:12:10')));

        $this->assertEquals($this->feed->getCountOfPostsInFeed(), 6);


        $this->assertEquals(2, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(3, $this->feed->getCountOfPostsInFeed(2));
        $this->assertEquals(1, $this->feed->getCountOfPostsInFeed(12));
        $this->assertEquals(0, $this->feed->getCountOfPostsInFeed(12333));



    }

    public function testDeletePost(){


        $this->feed->truncate();

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->deletePost(1);

        $this->assertEquals($this->feed->spaceLength(), 0);




    }


    public function testGetFeed(){

        $this->feed->truncate();

        for($i=1;$i<=1000; $i++){


            if($i<=100){
                $author_id = 1;
            }elseif($i<=500){
                $author_id = 2;
            }else{
                $author_id = 300;
            }
            $this->feed->addPost(EntityPost::create(array("id"=>$i, "author_id"=>$author_id, 'created_at'=>'2010-01-01 12:12:10')), $i);

        }


        $this->assertEquals(1000, $this->feed->getCountOfPostsInFeed());
        $this->assertEquals(100, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(400, $this->feed->getCountOfPostsInFeed(2));
        $this->assertEquals(500, $this->feed->getCountOfPostsInFeed(300));



        $byDate = $this->feed->getPostsByDateForAuthor(300);
        $this->assertEquals(500, $byDate["2010-01-01"]["count"]);

        /*
         * ОБЩАЯ ЛЕНТА
         */


        $arResultCommon = $this->feed->getFeed();
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10
        ),$arResultCommon );


        $arResultCommon = $this->feed->getFeed(null,100);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10
        ),$arResultCommon );


        /*
         * Такой корзны нет, то выдаем последнюю
         */
        $arResultCommon = $this->feed->getFeed(null,1000);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10
        ),$arResultCommon );

        $arResultCommon = $this->feed->getFeed(null,1);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                10, 9, 8, 7, 6, 5 , 4, 3, 2, 1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10
        ),$arResultCommon );


        $arResultCommon = $this->feed->getFeed(null,99);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                990, 989, 988, 987, 986, 985 , 984, 983, 982, 981
            ),
            "numberOfBasket"=>99,
            "basketSize"=>10
        ),$arResultCommon );




        $arResultCommon = $this->feed->getFeed(null, null, 3);
        $this->assertEquals(array(
            "countOfBasket"=>334,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998
            ),
            "numberOfBasket"=>334,
            "basketSize"=>3
        ),$arResultCommon );




        /*
         * ЛЕНТА В РАЗРЕЗЕ ПОЛЬЗОВАТЕЛЕЙ
         */


        $arResultUser = $this->feed->getFeed(1);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                100, 99, 98, 97, 96, 95 , 94, 93, 92, 91
            ),
            "numberOfBasket"=>10,
            "basketSize"=>10
        ),$arResultUser );


        $arResultUser = $this->feed->getFeed(1, 10);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                100, 99, 98, 97, 96, 95 , 94, 93, 92, 91
            ),
            "numberOfBasket"=>10,
            "basketSize"=>10
        ),$arResultUser );

        $arResultUser = $this->feed->getFeed(1,1);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                10, 9, 8, 7, 6, 5 , 4, 3, 2, 1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10
        ),$arResultUser );



        $arResultUser = $this->feed->getFeed(300);
        $this->assertEquals(array(
            "countOfBasket"=>50,
            "countOfPosts"=>500,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>50,
            "basketSize"=>10
        ),$arResultUser );





    }

}