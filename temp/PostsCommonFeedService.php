<?php

namespace Tvoydenvnik\Posts\Models\Posts;


use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsCommonFeedService;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class PostsCommonFeedService implements IPostsCommonFeedService{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    /* *******************************
  * Общая лента.  posts_common
  *   Цель:
  *      Хранение списка сообщений.
  *
  *   Структура:
  *      - postId (1) - id сообщения
  *      - authorId (2) - id автора сообщения
  *      - createdAt (3)- дата создания
  *      - createdAtAsString (4) '2012-01-01'
  *
  *
  *  Использование:
  *      - Блог пользователя - получить все сообщения в порядке создания (createdAt) для заданного пользователя.
  *              Возможность получить кол-во сообщений с группировокй по createdAt.
  *
  *      - Общий блог - получить все сообщения в порядке создания для всего сайта.
  *
  *
  *
  */
    
    
    public static function getFeedForHDru(){
    
        $feed = new PostsCommonFeedService();
        $feed->space_name = $feed->space_name . "_101";
        return $feed;
        
    }

    public static function getFeedForTvoydnevnik(){

        $feed = new PostsCommonFeedService();
        $feed->space_name = $feed->space_name . "_100";
        return $feed;

    }


    public $space_name = 'posts_common';


    public function initSchema($sUserName = 'app'){

        TarantoolHelper::createSpace($this->_connection, $this->space_name, array('user'=>$sUserName, 'if_not_exists'=>true));

        //postId
        TarantoolHelper::createIndex($this->_connection, $this->space_name , 'primary', 'hash', true, array(1, 'NUM'), true);

        //authorId createdAt
        TarantoolHelper::createIndex($this->_connection, $this->space_name , 'author', 'tree', false, array(2, 'NUM', 3, 'NUM'), true);

        //createdAt
        TarantoolHelper::createIndex($this->_connection, $this->space_name , 'created_at', 'tree', false, array(3, 'NUM'), true);


    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }



    public function spaceLength(){
        $result =  $this->_connection->evaluate("return box.space.".$this->space_name.":len()");
        if(is_array($result) && count($result)>0){
            return $result[0];
        }
        return 0;
    }

    /**
     * Удалить все данные из спейсов
     */
    public function truncate(){
        try{

            TarantoolHelper::truncate($this->_connection,  $this->space_name);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, $this->space_name)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  $this->space_name);
        }

    }


    public function drop(){
        try{

            TarantoolHelper::drop($this->_connection,  $this->space_name);
            return true;

        }catch (\Exception $e){
            return false;
        }

    }



    public function addPost(EntityPost $entityPost, $nCreatedAtTimestamp = null){

         /*
         * Приведим переменные в соответсвии с типами спейсов
         */
        $nAuthorId = $entityPost->getAuthorId();
        $nPostId = $entityPost->getId();

        if($nCreatedAtTimestamp != null){
            $nPostCreatedAt = intval($nCreatedAtTimestamp);
        }else{
            $nPostCreatedAt = $entityPost->getCreatedAtAsTimestamp();
        }

        /*
         * Найдем канала, если его нет, то создадим
         */
        $arData = array($nPostId, $nAuthorId, $nPostCreatedAt, $entityPost->getCreatedAtAsDateString());
        try{

            $this->_connection->insert($this->space_name, $arData);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, $this->space_name)===false){
                $this->initSchema();
                $this->_connection->insert($this->space_name, $arData);
            }
        }

    }

    public function deletePost($nPostId){

        $nPostId = intval($nPostId);
        $this->_connection->delete($this->space_name, array($nPostId) , 'primary');

    }



    public function getCountOfPostsInFeed($nAuthorId = null){

        $sEval = '';

        if($nAuthorId === null){

            $sEval = 'return box.space.'.$this->space_name.'.index.primary:count({})';

        }else{

            $sEval = 'return box.space.'.$this->space_name.'.index.author:count({'.$nAuthorId.'})';

        }


        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) === 0){
            return 0;
        }else{
            return $lResult[0];
        }

    }



    private function  _getOffset($nNumberOfBasket, $nCountOfPosts, $nBasketSize){

        $nCountOfBasket = ceil( $nCountOfPosts / $nBasketSize);
        if($nCountOfBasket === 0){
            $nCountOfBasket = 1;
        }

        $limit = $nBasketSize;
        $offset = null;
        if($nNumberOfBasket === null || $nNumberOfBasket > $nCountOfBasket || $nCountOfBasket === 0){
            $offset = null;
            $nNumberOfBasket = $nCountOfBasket;
        }else{
            $offset = ($nCountOfBasket - $nNumberOfBasket) * $nBasketSize;
        }

        return array(
            "limit"=>$nBasketSize,
            "offset"=>$offset,
            "countOfBasket"=> $nCountOfBasket,
            "numberOfBasket"=>$nNumberOfBasket
        );
    }


    public function getFeed($nAuthorId = null, $nNumberOfBasket = null, $nBasketSize = 10){

        /*
         * -- получим кол-во сообщений
         */

        $lCount = $this->getCountOfPostsInFeed($nAuthorId);



        if($lCount === 0){
            return false;
        }else{

            $arOffset = $this->_getOffset($nNumberOfBasket, $lCount, $nBasketSize);


            if($nAuthorId === null){

                $arResult = $this->_connection->select($this->space_name, array(), "created_at", $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);

            }else{

                $arResult = $this->_connection->select($this->space_name, array($nAuthorId), "author", $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);

            }


            $arPostsIds = array();

            for($i=0, $length=count($arResult); $i<$length; $i++){
                array_push($arPostsIds, $arResult[$i][0]);
            }


            return array(
                "countOfBasket"=> $arOffset['countOfBasket'],
                "countOfPosts" => $lCount,
                "postIds"=>$arPostsIds,
                "numberOfBasket"=>$arOffset['numberOfBasket'],
                "basketSize"=>$nBasketSize
            );


        }


    }


    public function getPostsByDateForAuthor($nAuthorId)
    {

        $lResultEval =  $this->_connection->call("PostsCommonFeedServiceGetPostsByDateForAuthor", array($nAuthorId));

        if(is_array($lResultEval) && count($lResultEval)>0){

           return $lResultEval[0][0];
        }
        return array();
    }


}