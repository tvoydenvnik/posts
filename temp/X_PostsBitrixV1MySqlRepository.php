<?php

namespace Tvoydenvnik\Posts\Models;

use Phalcon\Db\Column;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\MetaData;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsDBService;

class PostsBitrixV1MySqlRepository extends Model implements IPostsDBService{


    /**
     * @var $id integer
     */
    private $id;
    private function setId($val){
        $this->id = intval($val);
    }

    private function getId(){
        return intval($this->id);
    }


    /**
     * @var $post_type_id integer
     */
    private $post_type_id;


    /**
     * @var $author_id integer
     */
    private $author_id;

    /**
     * @var $title string
     */
    private $title;


    /**
     * @var $message string
     */
    private $message;


    /**
     *
     * Сообщение в массив.
     * Используется при сохранении в кэш, при выводе
     * @param PostsBitrixV1MySqlRepository $oPost
     * @return array
     */
    private function postToArray($oPost/*, TextParser $parser = null*/){
        return array(
            "id"=>intval($oPost->id),
            "post_type_id"=>intval($oPost->post_type_id),
            "author_id"=>intval($oPost->author_id),
            "title"=> $oPost->title,
            "message"=>$oPost->message,
            "sections"=>$oPost->sections,
            "params"=>$oPost->params,
            "like_pos"=>intval($oPost->like_pos),
            "like_neg"=>intval($oPost->like_neg),
            "reposts"=>intval($oPost->reposts),
            "views"=>intval($oPost->views),
            "favorite"=>intval($oPost->favorite),
            "external_id"=>intval($oPost->external_id),
            "external_date"=>$oPost->external_date,
            "comments"=>intval($oPost->comments),
            "disable_comments"=>intval($oPost->disable_comments),
            "joined"=>intval($oPost->joined),
            "DATE_CREATE"=>$oPost->created_at,
            "updated_at"=>$oPost->updated_at
        );
    }


    /**
     * @var $attaches string
     */
    private $sections;

    /**
     * @var $like_pos integer
     */
    private $like_pos;

    /**
     * @var $like_neg integer
     */
    private $like_neg;


    /**
     * @var $joined integer
     */
    private $joined;

    /**
     * @var $reposts integer
     */
    private $reposts;


    /**
     * @var $views integer
     */
    private $views;


    /**
     * @var $favorite integer
     */
    private $favorite;

    /**
     * @var $external_id integer
     */
    private $external_id;


    /**
     * @var $external_date string
     */
    private $external_date;

    /**
     * @var $comments integer
     */
    private $comments;

    /**
     * @var $disable_comments
     */
    private $disable_comments;


    /**
     * @var $params
     */
    private $params;

    /**
     * @var $deleted integer
     */
    private $deleted;

    /**
     * @var $created_at integer
     */
    private $created_at;

    /**
     * @var $updated_at integer
     */
    private $updated_at;


    private static function _emptyStringToNull($value){
        return ($value==''?null:$value);
    }
    private static function _zeroToNull($value){
        return ($value==0?null:$value);
    }

    /**
     * @param EntityPost $oEntityPost
     * @return bool|EntityPost
     */
    public function addPost(EntityPost $oEntityPost){


        $newPost = new PostsBitrixV1MySqlRepository();

        $newPost->author_id = $oEntityPost->getAuthorId();
        $newPost->post_type_id = $oEntityPost->getPostTypeId();
        $newPost->title = self::_emptyStringToNull($oEntityPost->getTitle());
        $newPost->message = self::_emptyStringToNull($oEntityPost->getMessage());
        $newPost->sections = $oEntityPost->getSectionsForSaveInRepository();
        $newPost->params = $oEntityPost->getParamsForSaveInRepository();
        $newPost->views = self::_zeroToNull($oEntityPost->getViews());
        $newPost->reposts = self::_zeroToNull($oEntityPost->getReposts());
        $newPost->favorite = self::_zeroToNull($oEntityPost->getFavorite());
        $newPost->external_id = self::_zeroToNull($oEntityPost->getExternalId());
        $newPost->external_date = self::_emptyStringToNull($oEntityPost->getExternalDate());
        $newPost->comments = self::_zeroToNull($oEntityPost->getComments());
        $newPost->like_neg = self::_zeroToNull($oEntityPost->getLikeNeg());
        $newPost->like_pos = self::_zeroToNull($oEntityPost->getLikePos());
        //$newPost->disable_comments = (self::_zeroToNull($oEntityPost->getDisableComments())==null?"N":"Y");



        //$newPost->setSections($oEntityPost);
        $bSave = $newPost->save();
        if($bSave == false){
            return false;
        }else{
            $oEntityPost->setId($newPost->getId());
            $oEntityPost->setCreatedAt($newPost->created_at);
            return $oEntityPost;
        }

    }

    public function updatePost(EntityPost $oEntityPost, $bIsAdmin){

        $oPost = PostsBitrixV1MySqlRepository::byId($oEntityPost->getId());

        if($oPost!==false && ( intval($oPost->author_id) === intval($oEntityPost->getAuthorId()) || $bIsAdmin === true) ){

            $oPost->title = self::_emptyStringToNull($oEntityPost->getTitle());
            $oPost->message = self::_emptyStringToNull($oEntityPost->getMessage());
            $oPost->sections = $oEntityPost->getSectionsForSaveInRepository();
            $bResult = $oPost->update();

            if($bResult!== false){
                return $this->postToArray($oPost);
            }

        }

        return false;


    }

    public function markDeleted($nPostId, $nAuthorId, $bIsAdmin){

        $oPost = PostsBitrixV1MySqlRepository::byId($nPostId);

        if($oPost!==false && ( intval($oPost->author_id) === intval($nAuthorId) || $bIsAdmin === true) ){


            return $oPost->update(array('deleted'=>1));

        }

        return false;

    }



    public function getSource()
    {
        return "b_blog_post";
    }




    private function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date('Y-m-d H:i:s');
    }

    private function beforeUpdate()
    {
        // Set the modification date
        $this->updated_at = date('Y-m-d H:i:s');
    }

    /**
     * @param $nPostId
     * @return PostsBitrixV1MySqlRepository
     */
    private static function byId($nPostId){
        $result = PostsBitrixV1MySqlRepository::find($nPostId);
        if($result->count() === 0){
            return false;
        }else{
            $result->rewind();
            while ($result->valid()) {
                return $result->current();
            }

        }
    }

    /**
     * @param array $arPostsId
     * @return array<EntityPost>
     */
    public function getPosts(array $arPostsId){

        $result = PostsBitrixV1MySqlRepository::find(
            array(
                'id IN ({ids:array})',
                'bind' => array(
                    'ids' => array_values($arPostsId)
                )
            )
        );

        $arResult = array();

        $result->rewind();
        while ($result->valid()) {

            /**
             * @var $currentPost PostsBitrixV1MySqlRepository
             */
            $currentPost = $result->current();

            $arPostArray = $currentPost->postToArray($currentPost);

            $arResult[$arPostArray['id']] =  EntityPost::create($arPostArray);

            $result->next();
        }

        return $arResult;

    }

    private static function _toArray($val){
        try{
            if($val === null) {
                return array();
            }elseif(is_array($val)){
                return $val;
            }else{
                return json_decode($val, true);
            }
        }catch (\Exception $e){
            return array();
        }
    }


    public function columnMap()
    {
        // Ключи - реальные имена в таблице и
        // значения - их имена в приложении
        return array(
            'ID' => 'id',
            'post_type_id'=>'post_type_id',
            'AUTHOR_ID' => 'author_id',
            'TITLE' => 'title',
            //при сохранении заполнять BLOG_ID
            'DETAIL_TEXT' => 'message',
            'sections' => 'sections',
            'like_pos' => 'like_pos',
            'like_neg' => 'like_neg',
            'reposts' => 'reposts',
            'VIEWS' => 'views',
            'NUM_COMMENTS' => 'comments',
            'ENABLE_COMMENTS'=>'disable_comments',
            'DATE_CREATE'=> 'created_at',
            'joined' => 'joined',
            'favorite' => 'favorite',
            'params' => 'params',
            'deleted' => 'deleted',
            'external_id' => 'external_id',
            'external_date' => 'external_date',
            'updated_at' => 'updated_at'
        );
    }

    private function _getCreateTableQuery(){
        return "CREATE TABLE bitrix.b_blog_post (
          ID int(11) NOT NULL AUTO_INCREMENT,
          TITLE varchar(255) NOT NULL,
          BLOG_ID int(11) NOT NULL,
          AUTHOR_ID int(11) NOT NULL,
          PREVIEW_TEXT text DEFAULT NULL,
          PREVIEW_TEXT_TYPE char(4) NOT NULL DEFAULT 'text',
          DETAIL_TEXT text NOT NULL,
          DETAIL_TEXT_TYPE char(4) NOT NULL DEFAULT 'text',
          DATE_CREATE datetime NOT NULL,
          DATE_PUBLISH datetime NOT NULL,
          KEYWORDS varchar(255) DEFAULT NULL,
          PUBLISH_STATUS char(1) NOT NULL DEFAULT 'P',
          CATEGORY_ID char(100) DEFAULT NULL,
          ATRIBUTE varchar(255) DEFAULT NULL,
          ENABLE_TRACKBACK char(1) NOT NULL DEFAULT 'Y',
          ENABLE_COMMENTS char(1) NOT NULL DEFAULT 'Y',
          ATTACH_IMG int(11) DEFAULT NULL,
          NUM_COMMENTS int(11) NOT NULL DEFAULT 0,
          NUM_TRACKBACKS int(11) NOT NULL DEFAULT 0,
          VIEWS int(11) DEFAULT NULL,
          FAVORITE_SORT int(11) DEFAULT NULL,
          PATH varchar(255) DEFAULT NULL,
          CODE varchar(255) DEFAULT NULL,
          MICRO char(1) NOT NULL DEFAULT 'N',
          HAS_IMAGES varchar(1) DEFAULT NULL,
          HAS_PROPS varchar(1) DEFAULT NULL,
          HAS_TAGS varchar(1) DEFAULT NULL,
          HAS_COMMENT_IMAGES varchar(1) DEFAULT NULL,
          HAS_SOCNET_ALL varchar(1) DEFAULT NULL,
          SEO_TITLE varchar(255) DEFAULT NULL,
          SEO_TAGS varchar(255) DEFAULT NULL,
          SEO_DESCRIPTION text DEFAULT NULL,
          post_type_id int(3) DEFAULT 0,
          sections varchar(255) DEFAULT NULL,
          like_pos int(6) DEFAULT NULL,
          like_neg int(6) DEFAULT NULL,
          reposts int(6) DEFAULT NULL,
          joined int(6) DEFAULT NULL,
          favorite int(6) DEFAULT NULL,
          params tinytext DEFAULT NULL,
          deleted int(1) DEFAULT NULL,
          external_id int(18) DEFAULT NULL,
          external_date date DEFAULT NULL,
          updated_at datetime DEFAULT NULL,
          PRIMARY KEY (ID),
          INDEX IX_BLOG_POST_1 (BLOG_ID, PUBLISH_STATUS, DATE_PUBLISH),
          INDEX IX_BLOG_POST_2 (BLOG_ID, DATE_PUBLISH, PUBLISH_STATUS),
          INDEX IX_BLOG_POST_3 (BLOG_ID, CATEGORY_ID),
          INDEX IX_BLOG_POST_4 (PUBLISH_STATUS, DATE_PUBLISH),
          INDEX IX_BLOG_POST_5 (DATE_PUBLISH, AUTHOR_ID),
          INDEX IX_BLOG_POST_CODE (BLOG_ID, CODE)
        )
        ENGINE = INNODB
        CHARACTER SET utf8
        COLLATE utf8_unicode_ci;";



        //UNIQUE INDEX UK_author__post_type__extarnal_id (author_id, post_type_id, external_id),
        //UNIQUE INDEX UK_author__post_type__extarnal_date (author_id, post_type_id, external_date)
    }

    public function truncate(){
        $all = PostsBitrixV1MySqlRepository::find();
        $all->delete();
    }



    private function metaData()
    {
        return array(
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => array(
                'ID', 'post_type_id', 'AUTHOR_ID', 'TITLE', 'DETAIL_TEXT', 'sections', 'like_pos', 'ENABLE_COMMENTS', 'like_neg', 'joined', 'reposts', 'VIEWS', 'favorite', 'external_id', 'external_date', 'NUM_COMMENTS',  'params' ,'DATE_CREATE', 'updated_at', 'deleted'
            ),

            // Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => array(
                'ID'
            ),

            // Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => array(
                'post_type_id', 'AUTHOR_ID', 'TITLE', 'DETAIL_TEXT', 'sections', 'like_pos', 'ENABLE_COMMENTS', 'like_neg', 'joined', 'reposts', 'VIEWS', 'favorite', 'external_id', 'external_date', 'NUM_COMMENTS', 'params' ,'DATE_CREATE', 'updated_at', 'deleted'
            ),

            // Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => array(
                'ID', 'post_type_id', 'AUTHOR_ID'
            ),

            // Every column and their data types
            MetaData::MODELS_DATA_TYPES => array(
                'ID'   => Column::TYPE_INTEGER,
                'post_type_id'=> Column::TYPE_INTEGER,
                'AUTHOR_ID' => Column::TYPE_INTEGER,
                'TITLE' => Column::TYPE_VARCHAR,
                'DETAIL_TEXT' => Column::TYPE_TEXT,
                'sections' => Column::TYPE_VARCHAR,
                'like_pos' => Column::TYPE_INTEGER,
                'like_neg'=> Column::TYPE_INTEGER,
                'ENABLE_COMMENTS'=> Column::TYPE_VARCHAR,
                'params' => Column::TYPE_TEXT,
                'joined' => Column::TYPE_INTEGER,
                'reposts' => Column::TYPE_INTEGER,
                'VIEWS' => Column::TYPE_INTEGER,
                'favorite' => Column::TYPE_INTEGER,
                'external_id' => Column::TYPE_INTEGER,
                'external_date' => Column::TYPE_DATE,
                'NUM_COMMENTS' => Column::TYPE_INTEGER,
                'DATE_CREATE' => Column::TYPE_DATETIME,
                'updated_at'=> Column::TYPE_DATETIME,
                'deleted' => Column::TYPE_INTEGER
            ),

            // The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => array(
                'ID'   => true,
                'post_type_id' => true,
                'AUTHOR_ID' => true,
                'like_pos' => true,
                'like_neg' => true,
                'joined'=>true,
                'reposts'=>true,
                'VIEWS'=>true,
                'favorite'=>true,
                'external_id'=>true,
                'NUM_COMMENTS'=>true,
                'deleted'=>true
            ),

            // The identity column, use boolean false if the model doesn't have
            // an identity column
            MetaData::MODELS_IDENTITY_COLUMN => 'ID',

            // How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => array(

                'ID'   => Column::BIND_PARAM_INT,
                'post_type_id'=> Column::BIND_PARAM_INT,
                'AUTHOR_ID' => Column::BIND_PARAM_INT,
                'TITLE' => Column::BIND_PARAM_STR,
                'DETAIL_TEXT' => Column::BIND_PARAM_STR,
                'sections' => Column::BIND_PARAM_STR,
                'like_pos' => Column::BIND_PARAM_INT,
                'like_neg'=> Column::BIND_PARAM_INT,
                'ENABLE_COMMENTS'=> Column::BIND_PARAM_STR,
                'joined'=> Column::BIND_PARAM_INT,
                'params' => Column::BIND_PARAM_STR,
                'reposts'=> Column::BIND_PARAM_INT,
                'VIEWS'=> Column::BIND_PARAM_INT,
                'favorite'=> Column::BIND_PARAM_INT,
                'external_id'=> Column::BIND_PARAM_INT,
                'external_date'=> Column::BIND_PARAM_STR,
                'NUM_COMMENTS'=> Column::BIND_PARAM_INT,
                'DATE_CREATE' => Column::BIND_PARAM_STR,
                'updated_at'=> Column::BIND_PARAM_STR,
                'deleted'=> Column::BIND_PARAM_INT
            ),

            // Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => array(

            ),

            // Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => array(

            ),

            // Default values for columns
            MetaData::MODELS_DEFAULT_VALUES => array(


                'TITLE' => null,
                'DETAIL_TEXT' => '',
                'sections' => null,
                'like_pos' => null,
                'like_neg'=> null,
                'disable_comments'=> 'Y',
                'joined'=> null,
                'params'=> null,
                'reposts'=> null,
                'VIEWS'=> null,
                'favorite'=> null,
                'external_id'=> null,
                'external_date'=> null,
                'NUM_COMMENTS'=> 0,
                'DATE_CREATE' => 0,
                'updated_at'=> null,
                'deleted'=> null,
            ),

            // Fields that allow empty strings
            MetaData::MODELS_EMPTY_STRING_VALUES => array()
        );
    }
}
