<?php

namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Apps\PostsApp;

use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsCacheTarantool;
use Tvoydenvnik\Posts\Models\Posts\PostsMySqlRepository;
use Tvoydenvnik\Posts\Models\PostsCommonFeedService;
use Tvoydenvnik\Posts\Models\PostsSectionsFeedService;


class PostsAppTest2 extends \PHPUnit_Framework_TestCase{




    public function testForTest(){
        $this->assertEquals(true, true);
        $app = $this->_getPostsApp(true);
    }


    private static $_postId1 = 0;

    public function testAddPost(){
        
        $app = $this->_getPostsApp();


        for($i=0;$i<300000;$i++){
            $appAnswer = $app->addPost(
                1,
                PostTypes::$cPOST_TYPE_FOOD_DIARY,
                "Заговловок",
                "Сообщение",
                array(12,45,12),
                null,
                array("/fff/1.gif"),
                100,
                array("params"=>true)
            );
        }
        $appAnswer = $app->addPost(
            1,
            PostTypes::$cPOST_TYPE_FOOD_DIARY,
            "Заговловок",
            "Сообщение",
            array(12,45,12),
            array("userId"=>1),
            array("/fff/1.gif"),
            100,
            array("params"=>true)
        );

        $this->assertEquals(false, $appAnswer->error);
        $this->assertGreaterThan(2000000, $appAnswer->getResult()["id"]);
        self::$_postId1 = $appAnswer->getResult()["id"];
        
        
        //повторно не даст добавить, т.к. такой ключ уже есть
        $appAnswer = $app->addPost(
            1,
            PostTypes::$cPOST_TYPE_FOOD_DIARY,
            "Заговловок",
            "Сообщение",
            array(12,45,12),
            array("userId"=>1),
            array("/fff/1.gif"),
            100,
            array("params"=>true)
        );

        $this->assertEquals(true, $appAnswer->error);

    }

    

    /**
     * @param $truncate
     * @return PostsApp
     */
    private function _getPostsApp($truncate = false){

        /**
         * @var $app PostsApp
         */
        $app = FactoryDefault::getDefault()->get('getPostsApp');

        if($truncate === true){
            $app->truncate();
        }

        return $app;
        
    }

}
