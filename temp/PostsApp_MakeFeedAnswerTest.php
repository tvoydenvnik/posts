<?php

namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Apps\PostsApp;
use Tvoydenvnik\Posts\Models\PostsBitrixReadMySqlRepository;
use Tvoydenvnik\Posts\Models\PostsCacheTarantool;


class PostsAppTestX extends \PHPUnit_Framework_TestCase{



    private function _getPostsApp($truncate=false){

        $app = new PostsApp();

        $db = new PostsBitrixReadMySqlRepository();
        $db->setConnection(FactoryDefault::getDefault()->get('dbBitrix'));
        $app->setPostsDBService($db);

        $cache = new PostsCacheTarantool();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        if($truncate){
            $cache->truncate();
        }
        $app->setPostsCacheService($cache);


        return $app;
    }


    public function test1Test(){
        $this->assertEquals(true, true);
    }

    public function testMakeFeedAnswer(){


        $app = $this->_getPostsApp(true);
        $feed = $app->makeFeedAnswer(array(14139, 1792, 36836, 4586, "fdks232sz"));

        $this->assertEquals(4, count($feed["postsData"]));


        $this->assertEquals("Рецепт Салат с адыгейским сыром ", $feed["postsData"][14139]->getTitle());
        $this->assertEquals("Полезные Свойства Фрукта Свити", $feed["postsData"][1792]->getTitle());
        $this->assertEquals("Рецепт бурито", $feed["postsData"][36836]->getTitle());
        $this->assertEquals("Дневник питания и тренировок за 24.05.2012", $feed["postsData"][4586]->getTitle());


        $this->assertEquals(true, is_array($feed["postsData"][4586]->getParams()));

        $d = $feed["postsData"][4586]->getParams();
        $this->assertEquals(array(40042,42176,54105,80947), $feed["usersId"]);


        /*
         * Повторный вызов, все должно ыть из кэша
         */




        $feed2 = $app->makeFeedAnswer(array(14139, 1792, 36836, 4586, "fdks232sz"));
        $this->assertGreaterThan(0, $feed2["postsData"][14139]->data["__expired"]);
        $this->assertGreaterThan(0, $feed2["postsData"][1792]->data["__expired"]);
        $this->assertGreaterThan(0, $feed2["postsData"][36836]->data["__expired"]);
        $this->assertGreaterThan(0, $feed2["postsData"][4586]->data["__expired"]);




        $this->assertEquals("Рецепт Салат с адыгейским сыром ", $feed["postsData"][14139]->getTitle());
        $this->assertEquals("Полезные Свойства Фрукта Свити", $feed["postsData"][1792]->getTitle());
        $this->assertEquals("Рецепт бурито", $feed["postsData"][36836]->getTitle());
        $this->assertEquals("Дневник питания и тренировок за 24.05.2012", $feed["postsData"][4586]->getTitle());


    }


    public function testMakeFeedAnswerPerformance(){
        $app = $this->_getPostsApp(true);
        $this->assertTime(1.7, function() use($app){
            for($i=0;$i<1000; $i++){
                $feed2 = $app->makeFeedAnswer(array(14139, 1792, 36836, 4586));
            }
        });
        
//        for($i=0;$i<110000; $i++){
//            $feed2 = $app->makeFeedAnswer(array(14139, 1792, 36836, 4586));
//        }
    }


    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }



}
