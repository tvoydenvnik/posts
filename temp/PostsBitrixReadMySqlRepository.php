<?php

namespace Tvoydenvnik\Posts\Models;



use Phalcon\Db;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Tvoydenvnik\Common\Lib\ArrayUtils;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsDBService;

class PostsBitrixReadMySqlRepository implements IPostsDBService{


    /**
     * @var Mysql
     */
    private $connection = null;

    /**
     * Установка соединения к mysql
     * @param $connection
     */
    public function setConnection($connection){
        $this->connection = $connection;
    }

    /**
     * Получить массив сообщений из БД
     * @param array $arPostsId - array(1, 2, ...)
     * @return array
     */
    public function getPosts(array $arPostsId){

       $arPostsIdNew = ArrayUtils::getNumericUniqueArray($arPostsId);


        $query = "SELECT
          b_blog_post.ID as id,
          b_blog_post.TITLE as title,
          b_blog_post.AUTHOR_ID as author_id,
          b_blog_post.DETAIL_TEXT as message,
          b_blog_post.DATE_CREATE as created_at,
          b_blog_post.DATE_PUBLISH as updated_at,
          b_blog_post.KEYWORDS,
          b_blog_post.PUBLISH_STATUS,
          b_blog_post.NUM_COMMENTS as comments,
          b_blog_post.VIEWS as views,
          b_blog_post.HAS_IMAGES,
          b_rating_voting.TOTAL_POSITIVE_VOTES as like_pos,
          b_rating_voting.TOTAL_NEGATIVE_VOTES as like_neg,
          hd_extra_post.TYPE_POST as post_type_id,
          hd_extra_post.ID_EXTRA,
          hd_extra_post.SECTION,
          hd_extra_post.RATING,
          hd_extra_post.PARAMS as params,
          hd_extra_post.DATE as external_date
        FROM b_blog_post
          LEFT OUTER JOIN hd_extra_post
            ON b_blog_post.ID = hd_extra_post.ID_POST
          LEFT OUTER JOIN b_rating_voting
            ON  b_rating_voting.ENTITY_TYPE_ID = 'BLOG_POST' AND
                b_blog_post.ID = b_rating_voting.ENTITY_ID
        WHERE b_blog_post.ID IN (".implode(",", $arPostsIdNew).")";


        // Send a SQL statement to the database system
        $result = $this->connection->query($query);


        $arResult = array();
        $result->setFetchMode(Db::FETCH_ASSOC);
        while ($oPost = $result->fetch()) {

            $entity = EntityPost::create(array(
                "id"=>intval($oPost["id"]),
                "post_type_id"=>intval($oPost["post_type_id"]),
                "author_id"=>intval($oPost["author_id"]),
                "title"=> $oPost["title"],
                "message"=>$oPost["message"],
                //todo "sections"=>$oPost["sections"],
                "params"=>$oPost["params"],
                "like_pos"=>intval($oPost["like_pos"]),
                "like_neg"=>intval($oPost["like_neg"]),
                "reposts"=>0,
                "views"=>intval($oPost["views"]),
                "favorite"=>0,
                "external_id"=>1,
                "external_date"=>$oPost["external_date"],
                "comments"=>intval($oPost["comments"]),
                "disable_comments"=>false,
                "joined"=>0,
                "created_at"=>$oPost["created_at"],
                "updated_at"=>$oPost["updated_at"],
                "has_attached_images"=>($oPost["HAS_IMAGES"]=="Y"?1:0)

            ));

            array_push($arResult, $entity);
        }

        return $arResult;
    }


    public function addPost(EntityPost $oEntityPost){

    }

    public function updatePost(EntityPost $oEntityPost, $bIsAdmin){

    }

    public function markDeleted($nPostId, $nAuthorId, $bIsAdmin){

    }


    public function truncate(){

    }
}