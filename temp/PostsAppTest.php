<?php

namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Apps\PostsApp;
use Tvoydenvnik\Posts\Models\PostsBitrixV1MySqlRepository;
use Tvoydenvnik\Posts\Models\PostsCacheTarantool;
use Tvoydenvnik\Posts\Models\PostsCommonFeedService;
use Tvoydenvnik\Posts\Models\PostsSectionsFeedService;


class PostsAppTest extends \PHPUnit_Framework_TestCase{



    private function _getPostsApp($truncate){

        $app = new PostsApp();

        //new PostsMySqlRepository()
        $app->setPostsDBService(new PostsBitrixV1MySqlRepository());

        $feed = new PostsCommonFeedService();
        $feed->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $app->setPostsCommonFeed($feed);


        $feedSections = new PostsSectionsFeedService();
        $feedSections->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $app->setPostsSectionsFeedService($feedSections);


        $cache = new PostsCacheTarantool();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $app->setPostsCacheService($cache);



        if($truncate === true){
           $app->truncate();
        }

        return $app;
    }


    public function test1Test(){
        $this->assertEquals(true, true);
    }


    public function testAddDeleteUpdatePosts(){

        $app = $this->_getPostsApp(true);
        $arListOfPosts = array();

        $nUserCount = 10;



        $timestamp = time();

        for($nUserId=1; $nUserId<=$nUserCount; $nUserId++){

            //кол-во сообщений у пользователя
            $userPosts = rand(20, 100);




            $nPostTypeId = 1;
            for($nUserPost = 1; $nUserPost<=$userPosts; $nUserPost++){


                $arSections = (array(rand(85, 100), rand(80, 100), rand(80, 100)));

                $timestamp = $timestamp + 1;
                $lAddResult = $app->addPost($nUserId, $nPostTypeId, 'title ' . $nUserPost, 'text ' . $nUserPost, $arSections,  array('mentions'), array("createdAtTimestamp"=>$timestamp/*microtime(true)*10000*/));

                array_unshift($arListOfPosts, array(
                    "id"=> $lAddResult['id'],
                    "author"=>$nUserId,
                    "postType"=>$nPostTypeId,
                    "sections"=>$arSections,
                    "title"=>'title ' . $nUserPost,
                    "message"=> 'text ' . $nUserPost
                ));




                //сбросим тип сообщения
                $nPostTypeId++;
                if($nPostTypeId===5){
                    $nPostTypeId = 1;
                }

                usleep(1);

            }

        }

        ///////////////////////////////////////////
        //сделаем модификации deletePost and updatePost
        ///////////////////////////////////////////


        // удалим все сообщения пользователя с id=1
        $arUserPostsForDelete = $this->_filter($arListOfPosts, array("author"=>1));
        foreach($arUserPostsForDelete as $key=>$value){
            $app->deletePost($value["id"], $value["author"]);
        }

        $arListOfPosts = array_filter($arListOfPosts, function($val){
            return !($val["author"] === 1);
        });

        //для второго пользователя обновим все сообщения
        foreach($arListOfPosts as $key=>$value){
            if( $value["author"] === 2){

                $arListOfPosts[$key]["sections"] = (array(rand(85, 100), rand(80, 100), rand(80, 100), rand(1, 50)));
                $arListOfPosts[$key]["title"] = $value["title"] . ' - update';
                $arListOfPosts[$key]["message"] = $value["message"] . ' - update';
                $app->updatePost($value["id"],$value["author"], $arListOfPosts[$key]["title"], $arListOfPosts[$key]["message"], $arListOfPosts[$key]["sections"]);
            }
        }



        //usleep(1000);

        ///////////////////////////////////////////
        //Тестируем блог пользователя
        //////////////////////////////////////////
        for($nAuthorId=1; $nAuthorId<=$nUserCount; $nAuthorId++){


            $lCountOfPosts = $app->_postsCommonFeedService->getCountOfPostsInFeed($nAuthorId);

            $arUserPosts = $this->_filter($arListOfPosts, array("author"=>$nAuthorId));
            //$arUserPostsTypes = $this->_getPostTypes($arUserPosts);
            $arUserPostsIds = $this->_getIds($arUserPosts);

            /*
             * Общее количество сообщений в блоге
             */
            $this->assertEquals(count($arUserPosts), $lCountOfPosts, 'Общее кол-во сообщений в блоге пользователя');

//            /*
//             * Общее количество сообщений: фильтр postType
//             */
//            array_walk($arUserPostsTypes, function($postType) use ($app, $nAuthorId, $arUserPosts){
//
//                $lCountOfPostsByType = $app->_postsCommonFeedService->getCountOfPostsInFeed($nAuthorId);
//                $this->assertEquals(count($this->_filter($arUserPosts, array())) , $lCountOfPostsByType, 'Общее кол-во сообщений в блоге пользователя: фильтр по типу соообщения');
//
//            });

            /*
             * Сформируем корзины для блога пользователя и проверим их
             */

            $arBaskets = $this->_makeBasket($arUserPostsIds);
            array_walk($arBaskets, function($arIdsOfBasket, $indexOfBasket) use ($app, $nAuthorId){

                $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, $indexOfBasket+1, 10);
                $this->assertEquals($arBasketCheck["postIds"], $arIdsOfBasket, 'Проверим корзины для блога пользователя');

            });

            /*
             * Получим последнюю корзину
             */
            $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, null, 10);
            $this->assertEquals($arBasketCheck["postIds"], $arBaskets[count($arBaskets)-1], 'Проверим последнюю корзину для блога пользователя');

//            /*
//             * Сформируем корзины для блога пользователя с учетом типа сообщения и проверим их
//             */
//            array_walk($arUserPostsTypes, function($postType) use ($app, $nAuthorId, $arUserPosts){
//
//                $arBaskets = $this->_makeBasket($this->_getIds($this->_filter($arUserPosts, array("postType"=>$postType))));
//
//                foreach($arBaskets as $indexBasket=>$idsBasket){
//                    $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, $postType, $indexBasket+1, 10);
//                    $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Корзины по типу сообщения: фильтр по типу сообщения');
//                }
//
//            });

            ///////////////////////////////////////////
            //Тестируем по разделам: По блогу автора
            //////////////////////////////////////////

            $arSections = $this->_getSections($arUserPosts);
            foreach($arSections as $i=>$section){

                $postInSection = $this->_filter($arUserPosts, array("section"=>$section));
                $lCount = $app->_postsSectionsFeedService->getCountOfPostsInFeedPerSection($section, $nAuthorId);

                $this->assertEquals(count($postInSection), $lCount, 'Блог пользователя: Количество: Фильтр по разделам');

                $arBaskets = $this->_makeBasket($this->_getIds($postInSection));

                foreach($arBaskets as $indexBasket=>$idsBasket){
                    $arBasketCheck  = $app->_postsSectionsFeedService->getFeedPerSection($section, $nAuthorId, $indexBasket+1, 10);
                    $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Блог пользователя: Корзины по типу сообщения: фильтр по разделу');
                }

            }

        }


        ///////////////////////////////////////////
        //Тестируем общую ленту
        //////////////////////////////////////////


        /*
         * Общее количество сообщений
         */
        $lCountOfPosts = $app->_postsCommonFeedService->getCountOfPostsInFeed(null);
        $this->assertEquals(count($arListOfPosts), $lCountOfPosts, 'Общее кол-во сообщений в общей ленте');


        /*
        * Сформируем корзины и проверим по общей ленте
        */
        $arBaskets = $this->_makeBasket($this->_getIds($arListOfPosts));
        array_walk($arBaskets, function($arIdsOfBasket, $indexOfBasket) use ($app){

            $arBasketCheck  = $app->_postsCommonFeedService->getFeed(null, $indexOfBasket+1, 10);
            $this->assertEquals($arBasketCheck["postIds"], $arIdsOfBasket, 'Общая лента: Проверим корзины');

        });


//        /*
//         * Сформируем корзины и проверим по общей ленте: фильтр по типу сообщения
//         */
//        $arPostsTypes = $this->_getPostTypes($arListOfPosts);
//        array_walk($arUserPostsTypes, function($postType) use ($app, $arListOfPosts){
//
//            $arBaskets = $this->_makeBasket($this->_getIds($this->_filter($arListOfPosts, array("postType"=>$postType))));
//
//            foreach($arBaskets as $indexBasket=>$idsBasket){
//                $arBasketCheck  = $app->_postsCommonFeedService->getFeed(null, $postType, $indexBasket+1, 10);
//                $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Общая лента: Корзины по типу сообщения: фильтр по типу сообщения');
//            }
//
//        });

        ///////////////////////////////////////////
        //Тестируем по разделам: Общая лента
        //////////////////////////////////////////

        $arSections = $this->_getSections($arListOfPosts);
        foreach($arSections as $i=>$section){

            $postInSection = $this->_filter($arListOfPosts, array("section"=>$section));
            $lCount = $app->_postsSectionsFeedService->getCountOfPostsInFeedPerSection($section, null);

            $this->assertEquals(count($postInSection), $lCount, 'Общая лента: Количество: Фильтр по разделам');


            $arBaskets = $this->_makeBasket($this->_getIds($postInSection));

            foreach($arBaskets as $indexBasket=>$idsBasket){
                $arBasketCheck  = $app->_postsSectionsFeedService->getFeedPerSection($section, null, $indexBasket+1, 10);
                $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Общая лента: Корзины по типу сообщения: фильтр по разделу');
            }

        }



        $this->assertEquals(false,  $app->_postsSectionsFeedService->getFeedPerSection(12423232, null, null, 10));
        $this->assertEquals(false,  $app->_postsCommonFeedService->getFeed(12423232, null, 10));


        /*=================================================================
         * Проверим getPosts из кэша и из БД
         *=================================================================*/

        $arResultFromCache = $app->_postsCacheService->getPostsFromCache($this->_getIds($arListOfPosts));

        $arResultFromDB = $app->_postsDBService->getPosts($this->_getIds($arListOfPosts));


        $this->assertEquals(count($arResultFromCache['posts']), count($arListOfPosts), "1");
        $this->assertEquals(count($arResultFromDB), count($arListOfPosts),"2");

        $this->assertEquals($arResultFromCache['posts'], $arResultFromDB);




        /*=================================================================
        * ПОЛУЧИМ БЛОГ АВТОРА todo tests
        *=================================================================*/
        $nAuthor1= $arListOfPosts[0]["author"];

        $app->_postsCacheService->truncate();
        $userBlogFirstPage = $app->getAuthorPosts($nAuthor1);
        $userBlogFirstPage2 = $app->getAuthorPosts($nAuthor1, 1);


//        $app->updatePost(122222222222,111, 'ss', 'sss');
//        $commonPosts = $app->getCommonPosts();


        $test = 1;

    }

//    private function _getPostTypes($arPosts){
//        $arResult = array();
//
//        foreach($arPosts as $key=>$value){
//            array_push($arResult, $value["postType"]);
//        }
//
//        return array_unique($arResult);
//    }

    private function _getSections($arPosts){
        $arResult = array();

        foreach($arPosts as $key=>$value){

            foreach($value["sections"] as $i=>$val){
                array_push($arResult, $val);
            }

        }

        return array_unique($arResult);
    }


    private function _filter($arPosts, $arFilter){

        $arResult = array();

        foreach($arPosts as $key=>$value){

            $bFilter = null;
            if(isset($arFilter["author"])){
                $bFilter = ($arFilter["author"] === $value["author"]);
            }

            if(($bFilter === true || $bFilter === null) && isset($arFilter["postType"])){
                $bFilter = ($arFilter["postType"] === $value["postType"]);
            }

            if(($bFilter === true || $bFilter === null) && isset($arFilter["section"])){

                foreach($value["sections"] as $i=>$sect){
                    if($sect === $arFilter["section"]){
                        $bFilter = true;
                        break;
                    }
                }

            }

            if($bFilter === true){
                array_push($arResult, $value);
            }
        }

        return $arResult;

    }

    private function _getIds($arPosts){
        $arResult = array();

        foreach($arPosts as $key=>$value){
            array_push($arResult, $value["id"]);
        }

        return $arResult;
    }

    private function _makeBasket($arIds){

        $arBasket = array();

        foreach($arIds as $key=>$value){

            //сформируем корзины
            if(count($arBasket)===0){
                array_push($arBasket, array($value));
            }else{
                $lCountAll =count($arBasket);
                $lFirst = $arBasket[0];
                if(count($lFirst)===10){
                    array_unshift($arBasket, array($value));
                }else{
                    array_push($arBasket[0], $value);
                }

            }


        }

        return $arBasket;

    }

}
