<?php
namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Models\PostsBitrixReadMySqlRepository;

class PostsBitrixReadMySqlRepositoryTest extends \PHPUnit_Framework_TestCase{


    /**
     * @var PostsBitrixReadMySqlRepository
     */
    private $repository = null;

    public function setUp()
    {

        $repository = new PostsBitrixReadMySqlRepository();
        $repository->setConnection(FactoryDefault::getDefault()->get('dbBitrix'));

        $this->repository =  $repository;

    }

    public function tearDown()
    {
        $this->repository = null;
    }


    public function testGetPosts(){

        $arResult = $this->repository->getPosts(array(14139, 1792, 36836, 4586, "fdks232sz"));

        $this->assertEquals(4, count($arResult));

        $this->assertEquals("Полезные Свойства Фрукта Свити", $arResult[0]->getTitle());
        $this->assertEquals("Дневник питания и тренировок за 24.05.2012", $arResult[1]->getTitle());
        $this->assertEquals("Рецепт Салат с адыгейским сыром ", $arResult[2]->getTitle());
        $this->assertEquals("Рецепт бурито", $arResult[3]->getTitle());
        $this->assertEquals(json_decode('{"__error":"\u21161231929у129","food":{"id":72112,"name":"Рецепт бурито","name_group":"бутерброд","units":[],"nutrients":{"68":1.3,"74":0.472,"11":160.4,"12":65,"13":10.7,"14":6.6,"15":14.6,"18":1,"19":0.5,"84":4.1747,"85":1110.4,"86":0.54,"23":0.33,"24":0.435,"25":0.209,"26":35.639,"27":12.43,"29":0.218,"22":0.119,"31":2.454,"32":300.12,"33":175.33,"35":30.34,"36":380.09,"37":83.04,"38":186.8,"39":160.51,"41":25.3,"42":1.42,"43":2.73,"44":3.969,"45":0.1106,"46":91.24,"47":5.896,"50":6.3,"52":2.97,"53":1.659,"55":21.3,"56":1.646,"57":25.93,"58":6.375,"62":0.132},"recipe_description":"","id_base_unit":0,"recipe":{"weight_cooking":1791,"weight_raw":1945,"food":[{"cc":0,"fn":"Лаваш армянский тонкий (пшеничная мука 1 сорта)","fs":{"id_s":0,"id":21272},"w":300},{"cc":0,"fn":"фасоль с грибами","fs":{"id_s":1,"id":68457},"w":332},{"cc":7,"fn":"Помидоры (томаты) (Пассерование)","fs":{"id_s":0,"id":397},"w":100},{"cc":0,"fn":"Бобы (фасоль фава), зрелые семена, консервированные","fs":{"id_s":0,"id":18391},"w":50},{"cc":7,"fn":"Лук репчатый (Пассерование)","fs":{"id_s":0,"id":186},"w":100},{"cc":7,"fn":"Лук порей (Пассерование)","fs":{"id_s":0,"id":188},"w":100},{"cc":7,"fn":"Чеснок луковица (Пассерование)","fs":{"id_s":0,"id":436},"w":10},{"cc":0,"fn":"Перец черный молотый","fs":{"id_s":0,"id":21224},"w":1},{"cc":0,"fn":"Перец молотый красный","fs":{"id_s":0,"id":21227},"w":1},{"cc":0,"fn":"Сыр голландский, брусковый","fs":{"id_s":0,"id":1091},"w":275},{"cc":0,"fn":"Шампиньоны","fs":{"id_s":0,"id":440},"w":154},{"cc":0,"fn":"Сладкая молочная кукуруза, желтая, консервированная, без добавления соли","fs":{"id_s":0,"id":17180},"w":125},{"cc":4,"fn":"Печень куриная (Тушение)","fs":{"id_s":0,"id":275},"w":150},{"cc":0,"fn":"Куриный фарш, рубленный, приготовленный на пару","fs":{"id_s":0,"id":14831},"w":151},{"cc":0,"fn":"Огурец соленый","fs":{"id_s":0,"id":244},"w":87},{"cc":0,"fn":"Масло оливковое","fs":{"id_s":0,"id":249},"w":9}]},"type_of_food":1,"id_nature":0,"id_account":80947,"deleted":0,"translit":"burito","post_id":36836,"url_of_post":"\/people\/user\/80947\/blog\/36836\/"},"HAS_IMAGES":"N"}', true), $arResult[3]->getParams());

    }


    public function testGetPostsPerformance(){

        $this->assertTime(1.7, function(){
            for($i=0;$i<10000; $i++){
                $arResult = $this->repository->getPosts(array(14139));

            }
        });


    }


    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }

}