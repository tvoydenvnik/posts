<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 30.04.2016
 * Time: 14:50
 */

namespace Tvoydenvnik\Posts\Utils;


use Tvoydenvnik\Posts\Entity\EntityPost;

class EntityPostToRecipeSearchData
{

    private static function getElement(EntityPost $entity, $nNutrientId){
        $params = $entity->getParams();
        if(is_array($params)){

            if(isset($params['food']) &&  isset($params['food']['nutrients'])  &&  isset($params['food']['nutrients'][$nNutrientId])){
                return round(floatval($params['food']['nutrients'][$nNutrientId]),0);
            }

        }

        return 0;
    }

    private static function getTimeCooking(EntityPost $entity){
        $params = $entity->getParams();
        if(is_array($params)){

            if(isset($params['food']) &&  isset($params['food']['recipe'])  &&  isset($params['food']['recipe']['time'])){
                return intval($params['food']['recipe']['time']);
            }

        }

        return 0;
    }

    private static function getIngredients(EntityPost $entity){

        $params = $entity->getParams();
        if(is_array($params)){

            $arResult = array();
            if(isset($params['food']) &&  isset($params['food']['recipe']) &&  isset($params['food']['recipe']['food'])){

                $foods = $params['food']['recipe']['food'];
                if(is_array($foods)){

                    for($i=0,$length=count($foods);$i<$length;$i++){
                        $item = $foods[$i];
                        if(isset($item['fn'])){
                            array_push($arResult, trim(htmlspecialchars($item['fn'])));
                        }
                    }
                }
            }


            return $arResult;
        }

        return array();

    }

    public static function getData(EntityPost $entity){
        if($entity->isPostRecipe()!==true){
            return false;
        }

        $arResult =  array(

            /*
             * id сообщения
             */
            "local_id"         => $entity->getId(),
            /*
             * Id источника рецепта (по сути id таблицы, где хранится данные рецепт)
             * Используется для фильтрации.
             */
            "source_id"        => 1,//todo
            //***********************************************

            /*
             * Наименование рецепта, по которому будет осуществляться поиск с учетом морфологии.
             */
            "name"             => $entity->getTitle(),

            /*
             * Ингредиенты рецепта, по которым будет осуществляться поиск с учетом морфологии.
             */
            "ingredients"      => self::getIngredients($entity),

            /*
             * id автора. Используется для фильтрации
             */
            "author_id"        => $entity->getAuthorId(),

            /*
             * Дата создания рецепта. Может использоваться для сортировки.
             */
            "date"             => $entity->getCreatedAtAsDateString() ,// "2015-01-02",

            /*
             * Код типа блюда (type of dish). Только один.  Фильтрация, фасеты.
             */
            //todo "type_of_dish"     => 12,

            /*
             * Горячее или холодное (Hot or cold)
             */
            //todo "hot_or_cold"      => 1, /*1 - hot, 2 - cold*/

            /*
             * Способ приготовления (cooking method)  Только один.  Фильтрация, фасеты.
             */
            //todo "cooking_method" => 12,

            /*
             * Приём пищи (food intake) Можно несколько.  Фильтрация, фасеты.
             */
            //todo "food_intake"      => array(1, 2, 3),

            /*
             * Кухни мира (cuisine ) Только один.  Фильтрация, фасеты.
             */
            //todo "cuisine"          => 442,

            /*
             * Назначение (purpose) Можно несколько.  Фильтрация, фасеты.
             */
            //todo "purpose"          => array(14, 23, 3677),

            /*
             * Основной ингредиент (basic ingredient) Можно несколько.  Фильтрация, фасеты.
             */
            //todo "basic_ingredient" => array(78, 5454, 4545),

            /*
             * Калорийность рецепта. Фильтрация, фасеты, сортировка
             */
            "calorie"          => self::getElement($entity, 11),

            /*
             * Белки рецепта. Фильтрация
             */
            "protein"          => self::getElement($entity, 13),

            /*
             * Углеводы рецепта. Фильтрация
             */
            "carb"             => self::getElement($entity, 14),

            /*
             * Жиры рецепта. Фильтрация
             */
            "fat"              => self::getElement($entity, 15),


            /*
             * Данные по которым будет считаться рейтинг, используемый для сортировки
             * Формула рейтинга:  (comments*коэф1 + likes*коэф2 + has_photo*коэф3 + in_favorites*коэф4)
             */

            "comments"         => $entity->getComments(),
            "likes"            => max($entity->getLikePos() - $entity->getLikeNeg(),0),
            "has_photo"        => count($entity->getAttachedImages())>0,
            "in_favorites"     => $entity->getFavorite(),

            /*
             * Время приготовления.
             */
            "time"             => self::getTimeCooking($entity),
        );

        return $arResult;
    }

}