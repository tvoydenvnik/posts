<?php

namespace Tvoydenvnik\Posts\Utils;

use Phalcon\Db;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\PostsCommonFeedService;

class LoadPostsToCommonFeedService {

    /**
     * @var Mysql
     */
    private $connection = null;

    /**
     * @var PostsCommonFeedService
     */
    private $feed = null;

    public function setConnection($connection){
        $this->connection = $connection;
    }

    public function setFeed($connection){
        $this->feed = $connection;
    }

    public function load(){


        $this->feed->truncate();

        $query = "SELECT
          b_blog_post.ID as id,
          b_blog_post.AUTHOR_ID as author_id,
          b_blog_post.DATE_CREATE as created_at,
          b_blog_post.DATE_PUBLISH as updated_at,
          b_blog_post.KEYWORDS,
          b_blog_post.PUBLISH_STATUS,
          hd_extra_post.TYPE_POST as post_type_id,
          hd_extra_post.SECTION,
          hd_extra_post.DATE as external_date
        FROM b_blog_post
          LEFT OUTER JOIN hd_extra_post
            ON b_blog_post.ID = hd_extra_post.ID_POST
            WHERE PUBLISH_STATUS='P' and author_id<>0

       ";

        // Send a SQL statement to the database system
        $result = $this->connection->query($query);


        $arResult = array();
        $result->setFetchMode(Db::FETCH_ASSOC);
        while ($oPost = $result->fetch()) {

            $this->feed->addPost(EntityPost::create(
                array("id"=>intval($oPost["id"]), "author_id"=>$oPost["AUTHOR_ID"], 'created_at'=>$oPost["created_at"])
            ));

        }
    }


}