<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 28.05.2016
 * Time: 13:56
 */

namespace Tvoydenvnik\Posts\Utils;


class Common
{
    
    
    
    public static function emptyStringToNull($value){
        return ($value==''?null:$value);
    }
    
    public static function zeroToNull($value){
        return ($value==0?null:$value);
    }

    public static function jsonToArray($sValue){
        try{

            if(is_array($sValue)) {
                return $sValue;
            }else if(is_string($sValue)){
                $arResult =  json_decode($sValue, true);
                if(is_array($arResult)){
                    return $arResult;
                }
                return array();
            }

        }catch (\Exception $e){
            return array();
        }

        return array();
    }


    public static function  getOffset($nNumberOfBasket, $nCountOfPosts, $nBasketSize){

        $nCountOfBasket = ceil( $nCountOfPosts / $nBasketSize);
        if($nCountOfBasket === 0){
            $nCountOfBasket = 1;
        }
        
        $offset = null;
        if($nNumberOfBasket === null || $nNumberOfBasket > $nCountOfBasket || $nCountOfBasket === 0){
            $offset = null;
            $nNumberOfBasket = $nCountOfBasket;
        }else{
            $offset = ($nCountOfBasket - $nNumberOfBasket) * $nBasketSize;
        }

        return array(
            "limit"=>$nBasketSize,
            "offset"=>$offset,
            "countOfBasket"=> $nCountOfBasket,
            "numberOfBasket"=>$nNumberOfBasket
        );
    }


    public static function getOffsetLua($sFuncName = "__getOffset"){


        $eval = <<<EOT
        local function $sFuncName(nNumberOfBasket, nCountOfPosts, nBasketSize)

            local nCountOfBasket = math.ceil( nCountOfPosts / nBasketSize);
            if nCountOfBasket == 0 then
                nCountOfBasket = 1;
            end
        
            local offset = null;
            if nNumberOfBasket == null or nNumberOfBasket > nCountOfBasket or nCountOfBasket == 0 then
             offset = null;
             nNumberOfBasket = nCountOfBasket;
            else
             offset = (nCountOfBasket - nNumberOfBasket) * nBasketSize;
            end
        
            return {
                limit=nBasketSize,
                offset=offset,
                countOfBasket=nCountOfBasket,
                numberOfBasket=nNumberOfBasket
            }

end
EOT;

        return $eval;
    }

}