<?php

namespace Tvoydenvnik\Posts\Utils;


use Phalcon\Di\FactoryDefault;

class RunLua
{

    public static function evaluateFile($sPath, $arDara = null){

        /**
         * @var $connection \Tarantool
         */
        $connection = FactoryDefault::getDefault()->get("tarantool");

        $arResult = $connection->evaluate(file_get_contents($sPath), $arDara);
        if(is_array($arResult) && count($arResult)==1){
            return $arResult[0];
        }

        return $arResult;
    }

}