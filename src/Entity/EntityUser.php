<?php

namespace Tvoydenvnik\Users\Entity;


class EntityUser {


    /**
     * Данные об пользователе
     * @var $data array
     */
    public $data = array();


    public function getId(){
        return intval($this->_get('id', 0));
    }

    public function setId($nId){
        $this->data['id'] = intval($nId);
    }

    public function isSetId(){
        return array_key_exists('id', $this->data);
    }


    public function getName(){
        return $this->_get('name',  "");
    }

    public function setName($name){
        //todo обрезать более 50  Пустую строку в null
        $this->data['name'] = trim($name);
        return $this;
    }

    public function isSetName(){
        return array_key_exists('name', $this->data);
    }


    public function getLastName(){
        return $this->_get('last_name',  "");
    }

    public function setLastName($name){
        //todo обрезать более 50  
        $this->data['last_name'] = trim($name);
        return $this;
    }

    public function isSetLastName(){
        return array_key_exists('last_name', $this->data);
    }



    public function getCreatedAt(){
        return $this->_get('created_at', '');
    }

    public function setCreatedAt($created_at){
        $this->data['created_at'] = $created_at;
    }

    public function isSetCreatedAt(){
        return array_key_exists('created_at', $this->data);
    }


    public function getAvatar(){
        return $this->_get('avatar', '');
    }

    public function setAvatar($created_at){
        $this->data['avatar'] = $created_at;
    }

    public function isSetAvatar(){
        return array_key_exists('avatar', $this->data);
    }


    public function getLogin(){
        return $this->_get('login', '');
    }

    public function setLogin($created_at){
        $this->data['login'] = $created_at;
    }

    public function isSetLogin(){
        return array_key_exists('login', $this->data);
    }


    public function getActive(){
        return $this->_get('active', '');
    }

    public function setActive($created_at){
        $this->data['active'] = $created_at;
    }

    public function isSetActive(){
        return array_key_exists('active', $this->data);
    }


    public function getSex(){
        return $this->_get('sex', '');
    }

    public function setSex($created_at){
        $this->data['sex'] = $created_at;
    }

    public function isSetSex(){
        return array_key_exists('sex', $this->data);
    }


    public function getCity(){
        return $this->_get('city', '');
    }

    public function setCity($created_at){
        $this->data['city'] = $created_at;
    }

    public function isSetCity(){
        return array_key_exists('city', $this->data);
    }


    public function getBirthday(){
        return $this->_get('birthday', '');
    }

    public function setBirthday($created_at){
        $this->data['birthday'] = $created_at;
    }

    public function isSetBirthday(){
        return array_key_exists('birthday', $this->data);
    }

    public function getPhotoId(){
        return $this->_get('photo_id', '');
    }

    public function setPhotoId($created_at){
        $this->data['photo_id'] = $created_at;
    }

    public function isSetPhotoId(){
        return array_key_exists('photo_id', $this->data);
    }


    public function getUpdatedAt(){
        return $this->_get('updated_at', '');
    }

    public function isSetUpdatedAt(){
        return array_key_exists('updated_at', $this->data);
    }



    /**
     * Возвращает свойство
     * @param $sProp
     * @param null $pDefault
     * @return null
     */
    private function _get($sProp, $pDefault = null){

        if(is_array($this->data) === true && isset($this->data[$sProp]) === true){
            return $this->data[$sProp];
        }

        return $pDefault;

    }


    public static function create($data){
        $post = new EntityUser();
        $post->data = $data;
        return $post;
    }


    /**
     *
     * Сообщение в массив.
     * Используется при сохранении в кэш, при выводе
     * @return array
     */
    public function userToArray(){

        $arResult = array();
        if($this->isSetId()){
            $arResult["id"] = $this->getId();
        }


        if($this->isSetName()){
            $arResult["name"] = $this->getName();
        }

        if($this->isSetLastName()){
            $arResult["last_name"] = $this->getLastName();
        }

        if($this->isSetLogin()){
            $arResult["login"] = $this->getLogin();
        }

        if($this->isSetActive()){
            $arResult["active"] = $this->getActive();
        }

        if($this->isSetSex()){
            $arResult["sex"] = $this->getSex();
        }

        if($this->isSetCity()){
            $arResult["city"] = $this->getCity();
        }

        if($this->isSetBirthday()){
            $arResult["birthday"] = $this->getBirthday();
        }

        if($this->isSetPhotoId()){
            $arResult["photo_id"] = $this->getPhotoId();
        }

        if($this->isSetAvatar()){
            $arResult["avatar"] = $this->getAvatar();
        }

        if($this->isSetCreatedAt()){
            $arResult["created_at"] = $this->getCreatedAt();
        }

        if($this->isSetUpdatedAt()){
            $arResult["updated_at"] = $this->getUpdatedAt();
        }

        return $arResult;
    }

}