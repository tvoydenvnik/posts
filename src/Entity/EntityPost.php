<?php

namespace Tvoydenvnik\Posts\Entity;

use Tvoydenvnik\Common\Lib\ArrayUtils;
use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Constants\Sections;
use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\Common\Lib\JSON;

use Tvoydenvnik\Posts\Utils\EntityPostToRecipeSearchData;
use Tvoydenvnik\TextParser\TextParser;

class EntityPost {

    /**
     * Данные сообщения
     * @var $data array
     */
    public $data = array();


    public function getId(){
        return intval($this->_get('id', 0));
    }

    public function setId($nId){
        $this->data['id'] = intval($nId);
    }

    public function isSetId(){
        return $this->_isset('id', $this->data);
    }

    public function getAuthorId(){
        return intval($this->_get('author_id', 0));
    }

    public function isAuthorIdInHDru(){

        if($this->getAuthorId()>=19000000){
            return false;
        }
        return true;
    }

    public function setAuthorId($authorId){
        $this->data['author_id'] = intval($authorId);
    }

    public function isSetAuthorId(){
        return $this->_isset('author_id', $this->data);
    }


    public function getPostTypeId(){
        return intval($this->_get('post_type_id', 0));
    }

    public function setPostTypeId($postTypeId){
        $this->data['post_type_id'] = intval($postTypeId);
    }

    public function isSetPostTypeId(){
        return $this->_isset('post_type_id', $this->data);
    }
    
    public function isPostTypeComment(){
        return (PostTypes::$cPOST_TYPE_COMMENT === $this->getPostTypeId());
    }

    public function isPostTypePhotoAlbumItem(){
        return (PostTypes::$cPOST_TYPE_PHOTO_ALBUM_ITEM === $this->getPostTypeId());
    }

    public function getParentId(){
        return intval($this->_get('parent_id', 0));
    }

    public function setParentId($nParentId){
        $this->data['parent_id'] = intval($nParentId);
    }

    public function isSetParentId(){
        return $this->_isset('parent_id', $this->data);
    }


    public function getAttachedImages(){

        $arImages = $this->_get('attached_images', array());
        if(is_array($arImages) == false){
            $arImages = Common::jsonToArray($arImages);
            $this->data['attached_images'] = $arImages;
        }
        return $arImages;

    }

    public function getAttachedImagesForSaveInRepository(){
        return self::_getValueForSaveInRepositoryForArray($this->_get('attached_images',null));
    }


    public function setAttachedImages($arImages){
        $this->data['attached_images'] =  Common::jsonToArray($arImages);
    }

    public function isSetAttachedImages(){
        return $this->_isset('attached_images', $this->data);
    }

    /**
     * Является ли сообщение рецептом
     * @return bool
     */
    public function isPostRecipe(){
        return (PostTypes::$cPOST_TYPE_RECIPE == $this->getPostTypeId());
    }


    public function getTitle(){
        return $this->_get('title',  "");
    }

    public function setTitle($title){
        //todo обрезать более 150  Пустую строку в null
        $this->data['title'] = trim($title);
        return $this;
    }

    public function isSetTitle(){
        return $this->_isset('title', $this->data);
    }

    /**
     * Текст сообщения. Может быть в формате bbcode.
     * Когда рецепты/дневники будут публиковаться из приложения, то может быть пустой, т.к. весь текст будет в params
     * @return String
     */
    public function getMessage(){

        $arMessage = $this->_get('message', array());
        if(is_array($arMessage) == false){
            $arMessage = Common::jsonToArray($arMessage);
            $this->data['message'] = $arMessage;
        }
        return $arMessage;

    }

    public function setMessage($message){
        //todo обрезать более 10000
        $this->data['message'] = Common::jsonToArray($message);
        return $this;
    }

    public function getMessageForSaveInRepository(){
        return self::_getValueForSaveInRepositoryForArray($this->_get('message',null));
    }


    public function isSetMessage(){
        return $this->_isset('message', $this->data);
    }


//    public function getViews(){
//        return intval($this->_get('views', 0));
//    }

//    public function setViews($views){
//        $this->data['views'] = intval($views);
//        return $this;
//    }
//
//    public function isSetViews(){
//        return $this->_isset('views', $this->data);
//    }


    public function getReposts(){
        return intval($this->_get('reposts', 0));
    }

    public function setReposts($reposts){
        $this->data['reposts'] = intval($reposts);
        return $this;
    }

    public function isSetReposts(){
        return $this->_isset('reposts', $this->data);
    }

    public function getFavorite(){
        return intval($this->_get('favorite', 0));
    }

    public function setFavorite($favorite){
        $this->data['favorite'] = intval($favorite);
        return $this;
    }

    public function isSetFavorite(){
        return $this->_isset('favorite', $this->data);
    }

    public function getExternal(){
        $arExternal = $this->_get('external', array());
        if(is_array($arExternal) == false){
            $arExternal = Common::jsonToArray($arExternal);
            $this->data['external'] = $arExternal;
        }
        return $arExternal;
    }

    public function setExternal($externalDate){
        $this->data['external'] = $externalDate;
        return $this;
    }

    public function externalToSha1(){
       return sha1(json_encode($this->getAttachedImagesForSaveInRepository()));
    }

    public function getUrl(){

        return "/people/user/"+$this->getAuthorId()+"/blog/"+$this->getId()+"/";
    }
    
    public function isSetExternal(){
        return $this->_isset('external', $this->data);
    }

    public function getExternalForSaveInRepository(){
        return self::_getValueForSaveInRepositoryForArray($this->_get('external',null));
    }

    public function getComments(){
        return intval($this->_get('comments', 0));
    }

    public function setComments($comments){
        $this->data['comments'] = intval($comments);
        return $this;
    }

    public function isSetComments(){
        return $this->_isset('comments', $this->data);
    }

    public function getJoined(){
        return intval($this->_get('joined', 0));
    }

    public function setJoined($joined){
        $this->data['joined'] = intval($joined);
        return $this;
    }

    public function isSetJoined(){
        return $this->_isset('joined', $this->data);
    }
    

    public function getLikePos(){
        return intval($this->_get('like_pos', 0));
    }

    public function setLikePos($likePos){
        $this->data['like_pos'] = intval($likePos);
        return $this;
    }

    public function isSetLikePos(){
        return $this->_isset('like_pos', $this->data);
    }

    public function getLikeNeg(){
        return intval($this->_get('like_neg', 0));
    }

    public function setLikeNeg($likeNeg){
        $this->data['like_neg'] = intval($likeNeg);
        return $this;
    }


    public function isSetLikeNeg(){
        return $this->_isset('like_neg', $this->data);
    }


    public function prepareMessageForSaveInDB(){
        $this->setMessage($this->getTextParser()->prepareForSaveInDB($this->getMessage()));
        return $this;
    }

//    public function messageParse($arImageSrc = array(), $arSmileConfig = array()){
//        $this->setMessage($this->getTextParser()->toHtml($this->getMessage(), $arImageSrc, $arSmileConfig));
//        return $this;
//    }


    public function prepareTitleForSaveInDB(){

        $this->setTitle($this->getTextParser()->toSimpleText($this->getTitle(),150));
        return $this;
    }


    /**
     * Если непроверенные данные, то применить getNumericUniqueArray
     * @return array|null
     */
    public function getSections(){

        $arSections = $this->_get('sections', array());
        if(is_array($arSections) == false){
            $arSections = Common::jsonToArray($arSections);
            $this->data['sections'] = $arSections;
        }
        return $arSections;
    }


    private static function _getValueForSaveInRepositoryForArray($val){
        try{

            if(is_array($val)) {

                if (count($val) === 0) {
                    return null;
                } else {
                    return JSON::json_encode($val);
                }
            }elseif(is_null($val) || $val == ""  || $val == "[]" || $val == "{}") {
                return null;
            }
            return strval($val);


        }catch (\Exception $e){
            return null;
        }
    }


    public function getSectionsForSaveInRepository(){
        return self::_getValueForSaveInRepositoryForArray($this->_get('sections',null));
    }

    /**
     * @param $arSections
     * @param int $maxSections - максимальное кол-во разделов
     */
    public function setSections($arSections, $maxSections = 5){
        /*
        * Обработаем секции
        */
        $arSectionsNew = Common::jsonToArray($arSections);

        $arSectionsNew = ArrayUtils::getNumericUniqueArray($arSectionsNew);
        if(count($arSectionsNew)>$maxSections){
            $arSectionsNew = array_slice($arSectionsNew, 0, $maxSections); 
        }
        
        $this->data['sections'] = $arSectionsNew;
    }
    
    public function getSectionsToIndexInTarantool(){
        
        $arSections = $this->getSections();
        $arIndex = array();
        foreach ($arSections as $value){
            if(in_array($value, Sections::getSectionsToIndexInTarantool())){
                array_push($arIndex, $value);
            }
        }
        
        return $arIndex;
        
    }

    public function isSetSections(){
        return $this->_isset('sections', $this->data);
    }


//    public function getParamsAsArray(){
//        return $this->_get('params', array());
//    }

//    public function getParamsAsJSON(){
//        return json_encode($this->_get('params', array()));
//    }

    public function getParamsForSaveInRepository(){
        return self::_getValueForSaveInRepositoryForArray($this->_get('params',null));
    }

    /**
     * Содержит дополнительную информацию.
     * Например:
     *  Для дневника питания - общие КБЖУ и т.д.
     *  Для рецепта: содержание рецепта
     * @return null
     */
    public function getParams(){

        $arParams = $this->_get('params', array());
        if(is_array($arParams) == false){
            $arParams = Common::jsonToArray($arParams);
            $this->data['params'] = $arParams;
        }
        return $arParams;


    }

    public function setParams($params){
        if(isset($params)===false){
            return;
        }
        $this->data['params'] = Common::jsonToArray($params);
    }


    public function updateParams($params){
        if(isset($params)===false){
            return;
        }
        $this->data['params'] = array_merge($this->getParams(), Common::jsonToArray($params));
    }

    public function isSetParams(){
        return $this->_isset('params', $this->data);
    }

    
    private function _isset($prop, $data){
        if(array_key_exists($prop, $data)){
            if(is_null($data[$prop])){
                return false;
            }
            return true;
        }
        return false;
    }



    public function getDeleted(){
        return intval($this->_get('deleted', 0));
    }

    public function setDeleted($deleted){
        $this->data['deleted'] = $deleted;
    }

    public function isSetDeleted(){
        return $this->_isset('deleted', $this->data);
    }


    public function getCreatedAt(){
        return $this->_get('created_at', '');
    }

    public function setCreatedAt($created_at){
        $this->data['created_at'] = $created_at;
    }

    public function isSetCreatedAt(){
        return $this->_isset('created_at', $this->data);
    }


    public function getUpdatedAt(){
        return $this->_get('updated_at', '');
    }

    public function getUpdateAtAsTimestamp(){
        $updated = $this->getUpdatedAt();
        if($updated == ''){
            $updated = $this->getCreatedAt();
        }
        $timestamp = strtotime ($updated);
        if($timestamp == false){
            $timestamp = time();
        }

        return $timestamp;
    }

    public function isSetUpdatedAt(){
        return $this->_isset('updated_at', $this->data);
    }

    public function getCreatedAtAsTimestamp(){
        $created = $this->getCreatedAt();
        $timestamp = strtotime ($created);
        if($timestamp == false){
            $timestamp = time();
        }

        return $timestamp;
    }


    public function getCreatedAtAsDateString(){
        return  date("Y-m-d", $this->getCreatedAtAsTimestamp());
    }


//    public function getHasAttachedImages(){
//        return $this->_get('has_attached_images', '');
//    }
//
//    public function setHasAttachedImages($created_at){
//        $this->data['has_attached_images'] = $created_at;
//    }
//
//    public function isSetHasAttachedImages(){
//        return $this->_isset('has_attached_images', $this->data);
//    }

    public function getLastComments(){

        $arComments = $this->_get('last_comments', array());
        if(is_array($arComments) == false){
            $arComments = Common::jsonToArray($arComments);
            $this->data['last_comments'] = $arComments;
        }
        return $arComments;
    }

    public function setLastComments($created_at){
        $this->data['last_comments'] = $created_at;
    }

    public function isSetLastComments(){
        return $this->_isset('last_comments', $this->data);
    }

    public function getLastPhotos(){

        $arPhotos = $this->_get('last_photos', array());
        if(is_array($arPhotos) == false){
            $arPhotos = Common::jsonToArray($arPhotos);
            $this->data['last_photos'] = $arPhotos;
        }
        return $arPhotos;
    }

    public function setLastPhotos($created_at){
        $this->data['last_photos'] = $created_at;
    }

    public function isSetLastPhotos(){
        return $this->_isset('last_photos', $this->data);
    }



    public function canSave(){

        if($this->getMessage()=='' || $this->isSetAttachedImages()===false){
            return false;
        }
        return true;
    }

    /**
     * Возвращает свойство
     * @param $sProp
     * @param null $pDefault
     * @return null
     */
    private function _get($sProp, $pDefault = null){

        if(is_array($this->data) === true && isset($this->data[$sProp]) === true){
            return $this->data[$sProp];
        }

        return $pDefault;

    }


    public static function create($data){
        $post = new EntityPost();
        $post->data = $data;
        return $post;
    }


    /**
     *
     * Сообщение в массив.
     * Используется при сохранении в кэш, при выводе
     * @return array
     */
    public function postToArray(){

        $arResult = array();
        if($this->isSetId()){
            $arResult["id"] = $this->getId();
        }
        
        $arResult["updated_at_time"] = $this->getUpdateAtAsTimestamp();
        
        if($this->isSetPostTypeId()){
            $arResult["post_type_id"] = $this->getPostTypeId();
        }

        if($this->isSetAuthorId()){
            $arResult["author_id"] = $this->getAuthorId();
        }

        if($this->isSetTitle()){
            $arResult["title"] = $this->getTitle();
        }

        if($this->isSetMessage()){
            $arResult["message"] = $this->getMessage();
        }

        if($this->isSetSections()){
            $arResult["sections"] = $this->getSectionsForSaveInRepository();
        }

        if($this->isSetParams()){
            $arResult["params"] = $this->getParamsForSaveInRepository();
        }

        if($this->isSetLikePos()){
            $arResult["like_pos"] = $this->getLikePos();
        }

        if($this->isSetLikeNeg()){
            $arResult["like_neg"] = $this->getLikeNeg();
        }

        if($this->isSetReposts()){
            $arResult["reposts"] = $this->getReposts();
        }

//        if($this->isSetViews()){
//            $arResult["views"] = $this->getViews();
//        }


        if($this->isSetFavorite()){
            $arResult["favorite"] = $this->getFavorite();
        }

        if($this->isSetExternal()){
            $arResult["external"] = $this->getExternal();
        }

        if($this->isSetComments()){
            $arResult["comments"] = $this->getComments();
        }

        if($this->isSetJoined()){
            $arResult["joined"] = $this->getJoined();
        }

        if($this->isSetCreatedAt()){
            $arResult["created_at"] = $this->getCreatedAt();
        }

        if($this->isSetParentId()){
            $arResult["parent_id"] = $this->getParentId();
        }

        if($this->isSetAttachedImages()){
            $arResult["attached_images"] = $this->getAttachedImagesForSaveInRepository();
        }

        if($this->isSetUpdatedAt()){
            $arResult["updated_at"] = $this->getUpdatedAt();
        }
        
        //
        
        

        return $arResult;
    }


    public function getDataForAddRecipeToSearch(){

        return EntityPostToRecipeSearchData::getData($this);
        
    }

    /**
     * @var $textParser TextParser
     */
    private $textParser = null;
    private function getTextParser(){
        if($this->textParser == null){
            $this->textParser = new TextParser();
        }
        return $this->textParser;
    }

}