<?php

namespace Tvoydenvnik\Posts\Apps;

use Tvoydenvnik\Common\AppAnswer;
use Tvoydenvnik\Common\EntityTypes;
use Tvoydenvnik\Common\Lib\ArrayUtils;
use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsCacheService;
use Tvoydenvnik\Posts\Interfaces\IPostsCommonFeedService;
use Tvoydenvnik\Posts\Interfaces\IPostsDBService;
use Tvoydenvnik\Posts\Interfaces\IPostsSectionsFeedService;
use Tvoydenvnik\Posts\Models\Posts\ExternalIndexMySqlModel;
use Tvoydenvnik\Posts\Models\Posts\PostsAuthorFeed;
use Tvoydenvnik\Posts\Models\Posts\PostsParentsIndexTarantool;
use Tvoydenvnik\Users\Apps\UserApp;

/*
 * Про комментарии.
 * У нас два уровня.
 * В БД храним супер парент id. А в параметрах запоним на какой ответ было.
 * В индексах все сортируем в порядке дополнения.
 * Но вывод используя время и если парент id совпадает, то рисуем иерархию.
 */

class PostsApp {


    /**
     * @param $nAuthorId
     * @param $nPostTypeId
     * @param $sTitle
     * @param $sMessage
     * @param null $arSections
     * @param null $external
     * @param null $arAttachedImages
     * @param null $nParentId
     * @param null $arParams
     * @param null $arMentions
     * @param array $arOptions
     * @return AppAnswer
     */

    public function addPost(
        $nAuthorId, //id пользователя
        $nPostTypeId,//id типа сообщения
        $sTitle, //Заголовок сообщения (может быть не задан)
        $sMessage, //текст сообщения
        $arSections = null,//id разделов, в которые входит сообщение. array или json-строка
        $external = null,//array или json-строка
        $arAttachedImages = null,//array или json-строка
        $nParentId = null,
        $arParams = null,//array или json-строка
        $arMentions = null,
        $arOptions = array()
    ){
       
        $entityPost = new EntityPost();
        $entityPost->setAuthorId($nAuthorId);
        $entityPost->setPostTypeId($nPostTypeId);
        //Подготовим текст для записи в БД
        $entityPost->setTitle($sTitle)->prepareTitleForSaveInDB();
        $entityPost->setMessage($sMessage)->prepareMessageForSaveInDB();
        $entityPost->setParams($arParams);
        $entityPost->setExternal($external);
        $entityPost->setParentId($nParentId);
        $entityPost->setAttachedImages($arAttachedImages);
        $entityPost->setSections($arSections);


        if($entityPost->canSave()!==true){
            return AppAnswer::create()->setError("Сообщение пустое.", $entityPost->postToArray())->setPublicErrorDesc("Сообщение пустое.");
        }
        
        if($entityPost->isSetExternal() === true){
            //todo проверка extarnal ключа
            $checkPostId = ExternalIndexMySqlModel::byIdExternal($entityPost->externalToSha1());
            if($checkPostId!== false){
                $entityPost->setId($checkPostId);
                return AppAnswer::create()->setResult(array("id"=>$entityPost->getId(), "url"=>$entityPost->getUrl()))->setError("Ошибка сохранения сообщения: он ранее был опубликован.", $entityPost->postToArray())->setPublicErrorDesc("Ошибка сохранения сообщения: он ранее был опубликован.");
            }
        }

        $parentEntity = $this->getSuperParentPost($entityPost);

        if($parentEntity!== false){
            if($parentEntity->getId()!== $entityPost->getParentId()){
                /*
                 * В БД храним толко основного родителя.
                 * Но ответ может прийти, на любой комментарий. Сохраним информацию об этом в параметрах.
                 */
                $entityPost->updateParams(array("level"=>2));
                $entityPost->updateParams(array("parent"=>$entityPost->getParentId()));
                //но в итого в базах, будет сохранет id главного сообщения.
                $entityPost->setParentId($parentEntity->getId());
            }
        }else{
            $entityPost->setParentId(0);
        }


        /**
         * @var $entityPost EntityPost
         * Сохраним в БД
         */
        $entityPost = $this->_postsDBService->addPost($entityPost);

        if($entityPost === false){

            return AppAnswer::create()->setError("Ошибка сохранения сообщения", $entityPost->postToArray())->setPublicErrorDesc("Ошибка сохранения сообщения");

        }else{


            //Сохраним в кэше
            if(isset($arOptions['doNotCache'])===false || $arOptions['doNotCache']!==true){
                // -- подготовим сообщение
                $this->_postsCacheService->addPost($entityPost);
            }

            if($parentEntity!== false){
                //увеличим кол-во комментариев в БЛ
                $this->_postsDBService->incrementPostComments($parentEntity);
            }

            if($entityPost->isSetExternal()===true){
                //Добавим в индекс публикаций дневника, чтобы обеспечить защиту от повторной публикации.
                ExternalIndexMySqlModel::addPost($entityPost);    
            }





            if($entityPost->isPostTypeComment()===false){

                //todo ограничить попадания сообщения в живую ленту: например, слишком мало текста, автор новичок. в update тоже самое
                //Добавим в общую ленту и в ленту HDru и tvoydnevnik
                $this->_postsCommonFeedService->addPost($entityPost);
                if($entityPost->isAuthorIdInHDru()){
                    $this->_postsCommonFeedServiceHD->addPost($entityPost);
                }else{
                    $this->_postsCommonFeedServiceTvoydnevnik->addPost($entityPost);
                }

                //Добавим в ленту разделов. Индексируем не все разделы.
                $this->_postsSectionsFeedService->addPost($entityPost);

            }


            //Добавить в ленту автора: добавляем как сообщения, todo??? так и комментарии. 
            //todo добавлять комментарии имеющие x символов, т.е. смайлы не добавлять.
            $this->_postsAuthorFeed->addPost($entityPost);
            

            
            
            /*
            *-- Добавим упоменания для всех, кто указан в $arMentions или если заполнен парент и автор парент другой человек.
             * Упоменения при чатах.
            */

            /*
            *-- Отправим сообщение на индексацию в поиск
            */

            /*
            *-- Карта сайта
            */

            /*
             *-- Если это чат, то созать канал
             */

        }

        return AppAnswer::create()->setResult(array(
            "id"=>$entityPost->getId(),
            "url"=>$entityPost->getUrl()
        ));


    }

    /**
     * @param EntityPost $entityPost
     * @return bool|EntityPost
     */
    private function getSuperParentPost(EntityPost $entityPost){
        if(/*$entityPost->isPostTypeComment() &&*/ $entityPost->getParentId()>0) {
            //получим родителя.
            $parentEntity = $this->postById($entityPost->getParentId());
            if($parentEntity !== false){
               return $parentEntity;
            }
        }

        return false;
    }


    public function addComment(
        $nAuthorId, //id пользователя
        $sMessage, //текст сообщения
        $arAttachedImages = null,//array или json-строка
        $nParentId = null
    ){
        $this->addPost($nAuthorId, PostTypes::$cPOST_TYPE_COMMENT, null, $sMessage, null,null,$arAttachedImages, $nParentId, null,array());
    }


    public function addPhotoAlbum(){

    }
    
    
    public function addPhotoAlbumItem(){

    }


    /**
     * @param $nPostId
     * @param $nAuthorId
     * @param $sTitle
     * @param $sMessage
     * @param null $arSections
     * @param null $arAttachedImages
     * @param null $arParams
     * @param bool $isAdmin
     * @return bool|AppAnswer
     */
    public function updatePost(
        $nPostId, //id поста
        $nAuthorId,
        $sTitle, //Заголовок сообщения (может быть не задан)
        $sMessage, //текст сообщения, не может быть пустым
        $arSections = null,//array или json-строка; id разделов, в котроые входит сообщение. (Не обязательно)
        $arAttachedImages = null,//array или json-строка
        $arParams = null,//array или json-строка
        $isAdmin = false
    ){

        /*
        * -- Подготовим текст для записи в БД
        */
        
        $entityPost = new EntityPost();
        $entityPost->setAuthorId($nAuthorId);
        $entityPost->setId($nPostId);
        $entityPost->setTitle($sTitle)->prepareTitleForSaveInDB();
        $entityPost->setMessage($sMessage)->prepareMessageForSaveInDB();
        $entityPost->setSections($arSections);
        $entityPost->setAttachedImages($arAttachedImages);
        $entityPost->setParams($arParams);

        if($entityPost->canSave()!==true){
            return AppAnswer::create()->setError("Сообщение пустое.", $entityPost->postToArray())->setPublicErrorDesc("Сообщение пустое.");
        }

        $answerUpdate = $this->_postsDBService->updatePost($entityPost, $isAdmin);

        if($answerUpdate->hasError() === false){

            $this->_postsCacheService->addPost($entityPost);
            
            $arDiff = array_diff($entityPost->getSections(), $answerUpdate["entity"]->getSections());
            if(count($arDiff)>0){
                $this->_postsSectionsFeedService->updatePost($entityPost);
            }


            
            return true;
            
        }else{
            
            return $answerUpdate;
        }


    }


    /**
     * Помечаем сообщение как удаленное
     * @param $nPostId
     * @param $nUserId
     * @param array $arOptions
     */
    public function deletePost($nPostId, $nUserId, $arOptions = array()){

        $isAdmin = false;
        if(isset($arOptions['isAdmin'])){
            $isAdmin = $arOptions['isAdmin'];
        }

        $isDeleted = $this->_postsDBService->markDeleted($nPostId, $nUserId, $isAdmin);

        if($isDeleted!==false){

            $this->_postsCommonFeedService->deletePost($nPostId);
            $this->_postsSectionsFeedService->deletePost($nPostId);
            $this->_postsCacheService->delete($nPostId);

        }

    }

    /**
     * Физически удаляем сообщение
     */
    public function removePost(){
        
    }

    /**
     * Установим лайки.
     */
    public function setLikes(){

        /*
         * Есть следующие намерения:
         * 1. Поставить лайк: положительный
         * 2. Поставить лайк: отрицательный
         * 3. Забрать лайк (ранее установленный + или - лайк)
         */
    }


    /**
     * Установить кол-во репостов
     */
    public function setReposts(){

    }


    /**
     * Установить кол-во проссмотров
     */
    public function setViews(){

    }

    /**
     * Установить кол-во избранное
     */
    public function setFavorite(){

    }

    /**
     * Установить кол-во joined
     */
    public function setJoined(){

    }


    /**
     * @param $nPostId
     * @return bool|EntityPost
     */
    private function postById($nPostId){
        $postsFromCache = $this->_postsCacheService->byId($nPostId);
        if($postsFromCache === false){
            $arPostsFromDB = $this->_postsDBService->getPosts(array($nPostId));
            if(count($arPostsFromDB)>0){
                $this->_postsCacheService->addPost($arPostsFromDB[0]);
                return $arPostsFromDB[0];
            }
        }else{
            return $postsFromCache;
        }

        return false;
    }

    /**
     *
     * Основная функция, которая возращает массив сообщений.
     * Получены они либо из кэша, либо из БД.
     * 
     * Она будет включать:
     *      - id сообщений
     *      - сами сообщения
     *      - Данные об пользователях
     *      - Последние 3 комментария
     *
     * @param $arPostsIds - Массив id сообщений, которые нужно вернуть.
     * @param array $arOptions
     *                  - withoutPostCache = true (Не использовать кэш)
     * @return array
     */
    public function makeFeedAnswer($arPostsIds, $arOptions = array()){


        $arPostsIdNew = ArrayUtils::getNumericUniqueArray($arPostsIds);

        /*
        * -- Получим данные  об о всех сообщениях
        */

        $arPosts = array(
            /*
             * "id_post"=>EntityPost
             *  ...
             */
        );

        if(isset($arOptions["withoutPostCache"])){
            $arPostsFromCache = array("posts"=>array(), 'notInCache'=>$arPostsIdNew);
        }else{
            $arPostsFromCache = $this->_postsCacheService->getPostsFromCache($arPostsIdNew);
        }


        /*
         * В результате получим сообщения из кэша, а также id сообщений, которых нет в кэше.
         * $arPostsFromCache = array(
            'posts'=> array(
                "id_post"=>EntityPost
            ),
            'notInCache'=>$arNotInCache
         )*/
        $arPosts = $arPostsFromCache['posts'];


        /*
         * -- Получим все сообщения из БД, которых не в кэше и поместим их туда же
         */
        if(count($arPostsFromCache['notInCache']) > 0){


            $arPostsFromDB = $this->_postsDBService->getPosts($arPostsFromCache['notInCache']);
            $arImageSrc = array();
            /**
             * @var $postEntity EntityPost
             */
            foreach($arPostsFromDB as $key=>$postEntity){

                /*
                 * добавим их в кэш. Там произодет конвертация сообщения в html. Т.е. в кэше храним html.
                 */
                $this->_postsCacheService->addPost($postEntity);

                $arPosts[$postEntity->getId()] = $postEntity;
            }

        }

        /*
         * -- Продемся по всем дневникам и сформируем:
         * 1) Список пользователей.
         * 2) Список каналов
         */

        $arUsers = array();
        $arCommentsChannels = array();

        /**
         * @var $postEntity EntityPost
         */
        foreach($arPosts as $key=>$postEntity){

            array_push($arUsers, $postEntity->getAuthorId());

            array_push($arCommentsChannels, array('entity_type'=>EntityTypes::$cENTITY_TYPE_POST, 'entity_id'=>$postEntity->getId()));

        }

        $arUsers = ArrayUtils::getNumericUniqueArray($arUsers);
        
        /*
         * 
         * -- Получим комментарии к сообщениям.
         * -- Дополним список пользователей
         * 
         */


        /*
        * 
        * -- Получим данные об пользователях
        * 
        */
        $arUsersData = array();
        if(is_null($this->_userApp)===false){
            $arUsersData = $this->_userApp->getUsers($arUsers);
        }

        

        return array(
            "postsId"=>$arPostsIdNew,
            "postsData"=>$arPosts,
            "usersId"=>$arUsers,
            "usersData"=>$arUsersData,
            "commentsChannels"=>$arCommentsChannels
        );

    }


    /**
     * Получить сообщения пользователя.
     *
     * @param $nAuthorId
     * @param null $nFilterSection - получить сообщения только по данному разделу.
     * @param null $nFilterNumberOsBasket - номер корзины
     * @param int $nBasketSize - количество сообщений в корзине
     * @return array|bool
     */
    public function getAuthorPosts($nAuthorId, $nFilterSection = null, $nFilterNumberOsBasket = null, $nBasketSize = 10){

        /*
         * -- Получим id сообщений с учетом фильтров
         */
        if(is_null($nFilterSection)){
            $arFeedResult = $this->_postsCommonFeedService->getFeed($nAuthorId, $nFilterNumberOsBasket, $nBasketSize);
        }else{
            $arFeedResult = $this->_postsSectionsFeedService->getFeedPerSection($nFilterSection, $nAuthorId, $nFilterNumberOsBasket, $nBasketSize);
        }


        if($arFeedResult === false){
            return false;
        }else{

            $arResult = $this->makeFeedAnswer($arFeedResult["postIds"]);


            $arResult["pagination"] = array(
                "numberOfBasket"=>$arFeedResult["numberOfBasket"],
                "countOfBasket"=> $arFeedResult["countOfBasket"],
                "countOfPosts"=>$arFeedResult["countOfPosts"],
                "basketSize"=> $nBasketSize,
            );

           return $arResult;
        }

    }

    /**
     *
     * Получить сообщения.
     *
     * @param null $nFilterSection - получить сообщения только по данному разделу.
     * @param null $nFilterNumberOsBasket - номер корзины
     * @param int $nBasketSize - количество сообщений в корзине
     * @return array|bool
     */
    public function getCommonPosts($nFilterSection = null, $nFilterNumberOsBasket = null, $nBasketSize = 10){

        /*
         * -- Получим id сообщений с учетом фильтров
         */
        if(is_null($nFilterSection)){
            $arFeedResult = $this->_postsCommonFeedService->getFeed(null, $nFilterNumberOsBasket, $nBasketSize);
        }else{
            $arFeedResult = $this->_postsSectionsFeedService->getFeedPerSection($nFilterSection, null, $nFilterNumberOsBasket, $nBasketSize);
        }


        if($arFeedResult === false){
            return false;
        }else{
            $arResult = $this->makeFeedAnswer($arFeedResult["postIds"]);

            $arResult["pagination"] = array(
                "numberOfBasket"=>$arFeedResult["numberOfBasket"],
                "countOfBasket"=> $arFeedResult["countOfBasket"],
                "countOfPosts"=>$arFeedResult["countOfPosts"],
                "basketSize"=> $nBasketSize,
            );

            return $arResult;
        }

    }


    /**
     * Очистим все. Только для ТЕСТОВ!!!
     */
    public function truncate(){
        $this->_postsCacheService->truncate();
        $this->_postsCommonFeedService->truncate();
        $this->_postsSectionsFeedService->truncate();
        $this->_postsDBService->truncate();
        $this->_postsParentsIndexTarantool->truncate();
        ExternalIndexMySqlModel::truncate();
    }


    /**
     * @var $_postsDBService IPostsDBService
     */
    public $_postsDBService;
    public function setPostsDBService(IPostsDBService $service){
        $this->_postsDBService = $service;
    }


    /**
     * @var $_postsCommonFeedService IPostsCommonFeedService
     */
    public $_postsCommonFeedService;
    public function setPostsCommonFeed(IPostsCommonFeedService $service){
        $this->_postsCommonFeedService = $service;
    }

    /**
     * @var $_postsCommonFeedServiceHD IPostsCommonFeedService
     */
    public $_postsCommonFeedServiceHD;
    public function setPostsCommonFeedHD(IPostsCommonFeedService $service){
        $this->_postsCommonFeedServiceHD = $service;
    }

    /**
     * @var $_postsCommonFeedServiceTvoydnevnik IPostsCommonFeedService
     */
    public $_postsCommonFeedServiceTvoydnevnik;
    public function setPostsCommonFeedTvoydnevnik(IPostsCommonFeedService $service){
        $this->_postsCommonFeedServiceTvoydnevnik = $service;
    }

    /**
     * @var $_postsSectionsFeedService IPostsSectionsFeedService
     */
    public $_postsSectionsFeedService;
    public function setPostsSectionsFeedService(IPostsSectionsFeedService $service){
        $this->_postsSectionsFeedService = $service;
    }


    /**
     * @var $_postsCacheService IPostsCacheService
     */
    public $_postsCacheService;
    public function setPostsCacheService(IPostsCacheService $service){
        $this->_postsCacheService = $service;
    }



    /**
     * @var $_postsParentsIndexTarantool PostsParentsIndexTarantool
     */
    public $_postsParentsIndexTarantool;
    public function setPostsParentsIndexTarantool(PostsParentsIndexTarantool $service){
        $this->_postsParentsIndexTarantool = $service;
    }

    /**
     * @var $_postsAuthorFeed PostsAuthorFeed
     */
    public $_postsAuthorFeed;
    public function setPostsAuthorFeed(PostsAuthorFeed $service){
        $this->_postsAuthorFeed = $service;
    }


    /**
     * @var $_userApp UserApp
     */
    public $_userApp = null;
    public function setUserApp(UserApp $service){
        $this->_userApp = $service;
    }

}