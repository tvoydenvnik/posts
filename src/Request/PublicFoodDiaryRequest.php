<?php


namespace Tvoydenvnik\Posts\Request;

use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Common\AppAnswer;
use Tvoydenvnik\Posts\Apps\PostsApp;
use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Constants\Sections;
use Validation\Validator;

class PublicFoodDiaryRequest {

    public static function create(){
        return new PublicFoodDiaryRequest();
    }

    /**
     * @param $arPost
     * @param $nAuthorId
     * @param $isAdmin
     * @return bool|AppAnswer
     */
    public function publicRequest($arPost, $nAuthorId, $isAdmin){
        
        $validator = new Validator($arPost, array('title', 'message',  'external'));
        $validator->rule('required', array('title', 'message', 'external'));

        $validator->rule('lengthMax', 'title', 150);
        $validator->rule('array', 'message')->toArray('message');
        $validator->rule('array', 'external')->toArray('external');

        if($validator->validate() === false){

            return AppAnswer::create()->setError('Ошибка инициализации параметров.', array(
                'class'=>__CLASS__,
                'file'=>__FILE__,
                'method'=>__METHOD__,
                'line'=>__LINE__,
                'data'=>$arPost
            )) ->setPublicErrorDesc('Ошибка инициализации параметров.');
            

        }else{


            /**
             * @var $appPost PostsApp
             */
            $appPost = FactoryDefault::getDefault()->get("getPostsApp");
            

            $sTitle = $validator->getValidData('title');
            $sMessage = $validator->getValidData('message');
            $arExternal = $validator->getValidData('external');

            /*
             * Допоним author_id - чтобы всегда был ключ уникальный
             */
            $arExternal["author_id"] = $nAuthorId;


            $lResult = $appPost->addPost($nAuthorId,  PostTypes::$cPOST_TYPE_FOOD_DIARY, $sTitle,  $sMessage,  array(Sections::getSectionOfFoodDiary()), $arExternal);
            
            if($lResult->hasError() && isset($lResult->getResult()["url"])){

                $res = $lResult->getResult();
                
                //todo доделать.
                return $appPost->updatePost(
                    $res["id"], //id поста
                    $nAuthorId,
                    $sTitle, //Заголовок сообщения (может быть не задан)
                    $sMessage, //текст сообщения, не может быть пустым
                    $arSections = null,//array или json-строка; id разделов, в котроые входит сообщение. (Не обязательно)
                    null,//array или json-строка
                    null,//array или json-строка
                    $isAdmin
                );

            }

            return $lResult;


        }

    }
}