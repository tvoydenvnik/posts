<?php

namespace Tvoydenvnik\Posts\Models;



use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

/*
 * Живая лента в разрезе пользователей
 *

 * 2 - user_id Чья живая лента. Id пользователя
 * 3 - post_id (если это упоминание, то добавляем @, чтобы не обновить при добавлении комментариев)
 * 4 - is_owner  Фильтр: Мои, Подписка
 * 5 - type Тип: рипост, упоминание
 * 6 - date Дата добавления. Или дата последнего добавленного комментария.
 * 7 - params Данные в JSON. Комментарий к репосту. Текст упоминание и др.
 * 8 - comments Массив id последних комментариев. Не более 10.
 *
 * Индексы:
 *
 * Добавление сообщения (primary)
 *  user_id, post_id
 *
 * Для ленты пользователя. + фильтр: мое|подписка
 *  user_with_filter - user_id, is_owner, date
 *  user - user_id, date
 *
 * Для обновления date, при добавлении комменатриев
 *  post - post_id
 *
 * Удаление старых сообщений. Например, старше 30 дней.
 * date - date
 *
 */
class UserLiveFeedTarantool{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'user_live_feed';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'NUM'), true);
    }


    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}