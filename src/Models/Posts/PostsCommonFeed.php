<?php

namespace Tvoydenvnik\Posts\Models\Posts;


use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsCommonFeedService;
use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class PostsCommonFeed implements IPostsCommonFeedService{


    /**
     * @var $_connection \Tarantool
     */
    public $_connection;

    /* *******************************
  * Общая лента.  posts_common
  *   Цель:
  *      Хранение списка сообщений в разрезе всего сайта.
  *
  *   Структура:
  *      - postId (1) - id сообщения
  *      - createdAt (2)- дата создания
  *
  *  Использование:
  *
  *      - Общий блог - получить все сообщения в порядке создания для всего сайта.
  *
  *
  *
  */

    public static function getFeedForHDru(){

        $feed = new PostsCommonFeed();
        $feed->space_name = $feed->space_name . "_101";
        return $feed;

    }

    public static function getFeedForTvoydnevnik(){

        $feed = new PostsCommonFeed();
        $feed->space_name = $feed->space_name . "_100";
        return $feed;

    }


    public $space_name = 'posts_common_feed1';


    public function initSchema($sUserName = 'app'){

        TarantoolHelper::createSpace($this->_connection, $this->space_name, array('user'=>$sUserName, 'if_not_exists'=>true));

        //postId
        TarantoolHelper::createIndex($this->_connection, $this->space_name , 'primary', 'hash', true, array(1, 'NUM'), true);

        //createdAt
        TarantoolHelper::createIndex($this->_connection, $this->space_name , 'created_at', 'tree', false, array(2, 'NUM'), true);

    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }



    public function spaceLength(){
        $result =  $this->_connection->evaluate("return box.space.".$this->space_name.":len()");
        if(is_array($result) && count($result)>0){
            return $result[0];
        }
        return 0;
    }

    /**
     * Удалить все данные из спейсов
     */
    public function truncate(){
        try{

            TarantoolHelper::truncate($this->_connection,  $this->space_name);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, $this->space_name)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  $this->space_name);
        }

    }


    public function drop(){
        try{

            TarantoolHelper::drop($this->_connection,  $this->space_name);
            return true;

        }catch (\Exception $e){
            return false;
        }

    }



    public function addPost(EntityPost $entityPost){

        $nPostId = $entityPost->getId();
        $nPostCreatedAt = $entityPost->getCreatedAtAsTimestamp();
        try{

            $this->_connection->insert($this->space_name, array($nPostId, $nPostCreatedAt));

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, $this->space_name)===false){
                $this->initSchema();
                $this->_connection->insert($this->space_name, array($nPostId, $nPostCreatedAt));
            }
        }

    }

    public function deletePost($nPostId){

        $nPostId = intval($nPostId);
        $this->_connection->delete($this->space_name, array($nPostId) , 'primary');

    }



    public function getCountOfPostsInFeed(){


        $sEval = 'return box.space.'.$this->space_name.'.index.primary:count({})';

        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) === 0){
            return 0;
        }else{
            return $lResult[0];
        }

    }


    public function getFeed($nNumberOfBasket = null, $nBasketSize = 10, $bGetPostsData = true){

        $__getOffset = Common::getOffsetLua();
        $getLuaFuncGetPosts = PostsCacheTarantool::getLuaFuncGetPosts(false);

        $space = $this->space_name;

        $eval = <<<EOT
        
        $__getOffset
        $getLuaFuncGetPosts
       local function __getFeedPostsCommonFeed(nNumberOfBasket, nBasketSize, bWithPosts)

            local nCountOfPosts =  box.space.$space:len();
            
            local postIds = {};
            local posts = {};
            
            if nCountOfPosts == 0 then
                return false;
            end
            
            local getOffsetResult = __getOffset(nNumberOfBasket, nCountOfPosts, nBasketSize);
            local result = box.space.$space.index.created_at:select({}, {
                iterator = 'REQ',
                limit = getOffsetResult["limit"],
                offset = getOffsetResult["offset"]
            })
            
            if result ~= nill  then           
                for key,value in ipairs(result)
                do
                    table.insert(postIds, value[1])
                end
                
                if bWithPosts then
                    posts = __PostsCacheTarantoolGetPostsV1(postIds, true, true);
                end
            end
            
            
            getOffsetResult['postIds'] = postIds;
            getOffsetResult['countOfPosts'] = nCountOfPosts;
            getOffsetResult['basketSize'] = nBasketSize;
            if bWithPosts then
                getOffsetResult['posts'] = posts;
            end
            
            
            return getOffsetResult;
            
        end
        
        return __getFeedPostsCommonFeed(...);
       
EOT;

        $lResult = $this->_connection->evaluate($eval, array($nNumberOfBasket, $nBasketSize, $bGetPostsData));

        if(is_array($lResult) && count($lResult)>0 && is_array($lResult[0])){
            return $lResult[0];
        }

        return false;
//        /*
//         * -- получим кол-во сообщений
//         */
//
//        $lCount = $this->getCountOfPostsInFeed();
//
//
//
//        if($lCount === 0){
//            return false;
//        }else{
//
//            $arOffset = Common::getOffset($nNumberOfBasket, $lCount, $nBasketSize);
//
//            $arResult = $this->_connection->select($this->space_name, array(), "created_at", $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);
//
//            $arPostsIds = array();
//
//            for($i=0, $length=count($arResult); $i<$length; $i++){
//                array_push($arPostsIds, $arResult[$i][0]);
//            }
//
//
//            return array(
//                "countOfBasket"=> $arOffset['countOfBasket'],
//                "countOfPosts" => $lCount,
//                "postIds"=>$arPostsIds,
//                "numberOfBasket"=>$arOffset['numberOfBasket'],
//                "basketSize"=>$nBasketSize
//            );
//
//
      //  }


    }


}