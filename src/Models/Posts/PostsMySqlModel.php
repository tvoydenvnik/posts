<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Phalcon\Db\Column;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\MetaData;


class PostsMySqlModel extends Model
{
    /**
     * id сообщения
     *
     * Posts хранит данные обо всех сообщениях.
     * В ней будет хранится как данные из hd.ru, так и данные из tvoydnevnik.
     *Чтобы не было проблем с id:
     * 1.	Даем запас hd.ru – 2 000 000  сообщений. Будем считать, что за это время я смогу все сделать.
     * 2.	Все новые сообщения будут иметь id начиная с id c 2 000 000
     * 3.	В Posts данные будет переноситься из hd.ru по мере создания сообщений в hd.ru
     *
     * @var $id integer
     */
    private $id;
    public function setId($val){
        $this->id = intval($val);
    }

    public function getId(){
        return intval($this->id);
    }


    /**
     * Тип сообщения - определяет характер записи:
     *      обычное
     *      дневник питания и тренирровок
     *      дневник питания
     *      ...
     * @var $post_type_id integer
     */
    public $post_type_id;

    /**
     * Данное поле будет использоваться в фотоальбомах:
     *   1. Будет одно соообщение - альбом
     *   2. И сообщение на каждую фотографию.
     *   Это позволит делать комментарии/лайки как для альбома, так и для фотографии, используя единные механизмы.
     * @var $parent_id integer
     */
    public $parent_id;

    /**
     * Может быть как одна фотка (фотография альбома), так и несколько
     *
     * @var $attached_images string
     */
    public $attached_images;

    /**
     * Автор сообщения
     * @var $author_id integer
     */
    public $author_id;

    /**
     * Заголовок.
     * @var $title string
     */
    public $title;

    /**
     *
     * Соообщение.
     *
     * Может быть:
     *  1. Текст
     *  2. JSON - данные дневника питания, тренировок, рецепта,  и т.п.
     *      Для hd.ru будут объеденены текст + данные дненвика, рецепта
     *      Формат:
     *          [
     *              todo {???}
     *          ]
     * 
     *
     *
     * @var $message string
     */
    public $message;



    /**
     *
     * id разделов сообщения.  Например: [1,22,34]
     * Разделы предустановленные.
     * @var $attaches string
     */
    public $sections;



    /**
     * @var $like_pos integer
     */
    public $like_pos;

    /**
     * @var $like_neg integer
     */
    public $like_neg;


    /**
     * Для рецептов -  кол-во я готовил.
     * Для события - кол-во я прийду
     * @var $joined integer
     */
    public $joined;

    /**
     * Кол-во рипостов
     * @var $reposts integer
     */
    public $reposts;


    /**
     * 
     * Кол-во проссмотров.
     * Todo: нужно ли
     * @var $views integer
     */
    public $views;


    /**
     * 
     * Колво в кулинарных книгах или в избранном (будет ли избранное).
     * @var $favorite integer
     */
    public $favorite;

    /**
     * external - данные необходимые для связи с сервером МЗР
     * Храним в json формате. По ним создать таблицу с индексом - для проверки дубля публикации.
     * @var $external string
     */
    public $external;

    /**
     * 
     * Кол-во комментариев
     * @var $comments integer
     */
    public $comments;
    
    /**
     * 
     * Параметры сообщения. Например - запрет комментирования.
     * При обновлении параметры - добавляются или обновляются. Старые параметры не затираются.  
     * @var $params
     */
    public $params;


    /**
     * @var $deleted integer
     */
    public $deleted;

    /**
     * @var $created_at integer
     */
    public $created_at;

    /**
     * @var $updated_at integer
     */
    public $updated_at;

    public function initialize()
    {
        $this->useDynamicUpdate(true);
    }
    /**
     *
     * Сообщение в массив.
     * Используется при сохранении в кэш, при выводе
     * @return array
     */
    public function postToArray(){
        return array(
            "id"=>intval($this->id),
            "post_type_id"=>intval($this->post_type_id),
            "author_id"=>intval($this->author_id),
            'parent_id'=> intval($this->parent_id),
            'attached_images'=> $this->attached_images,
            "title"=> htmlspecialchars($this->title),
            "message"=>$this->message,
            "is_raw_message"=>true,
            "sections"=>$this->sections,
            "params"=>$this->params,
            "like_pos"=>intval($this->like_pos),
            "like_neg"=>intval($this->like_neg),
            "reposts"=>intval($this->reposts),
            "favorite"=>intval($this->favorite),
            "external"=>$this->external,
            "comments"=>intval($this->comments),
            "deleted"=>intval($this->deleted),
            "joined"=>intval($this->joined),
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
        );
    }




    private static function _emptyStringToNull($value){
        return ($value==''?null:$value);
    }
    private static function _zeroToNull($value){
        return ($value==0?null:$value);
    }


    /**
     * @param $nPostId
     * @return PostsMySqlModel
     */
    public static function byId($nPostId){
        $result = PostsMySqlModel::find($nPostId);
        if($result->count() === 0){
            return false;
        }else{
            $result->rewind();
            while ($result->valid()) {
                return $result->current();
            }

        }
    }

    private function _getCreateTableQuery(){
        return "CREATE TABLE test.s_posts (
          id int(18) NOT NULL AUTO_INCREMENT,
          parent_id int(18) DEFAULT NULL COMMENT 'id родителя',
          post_type_id int(3) NOT NULL COMMENT 'id типа сообщения',
          author_id int(18) NOT NULL COMMENT 'id автора сообщения',
          title varchar(100) DEFAULT NULL COMMENT 'Заголвоко сообщения',
          message tinytext DEFAULT NULL COMMENT 'Текст сообщения',
          sections varchar(255) DEFAULT NULL COMMENT 'Разделы, в которые включено сообщение',
          like_pos int(6) DEFAULT NULL COMMENT 'Кол-во положительных лайков',
          like_neg int(6) DEFAULT NULL COMMENT 'Кол-во отрицательных лайков',
          attached_images tinytext DEFAULT NULL COMMENT 'Прикрепленные изображения',
          reposts int(6) DEFAULT NULL COMMENT 'Количество репостов',
          comments int(6) DEFAULT NULL COMMENT 'Количество комментариев',
          joined int(6) DEFAULT NULL COMMENT 'Для рецепта: сколько людей готовили, для встречи: сколько пойдут',
          favorite int(6) DEFAULT NULL COMMENT 'Сколько людей поставили себе сообщение в избранное или в кулинарную книгу',
          params tinytext DEFAULT NULL COMMENT 'Дополнительные данные.  Например, для дневника питания хранится КБЖУ',
          deleted int(1) DEFAULT NULL COMMENT 'Сообщение помечено как удаленное',
          external varchar(160) DEFAULT NULL COMMENT 'Связь с сервером приложения. Например, дата опубликованного дневника питания и тренировок',
          created_at datetime DEFAULT NULL,
          updated_at datetime DEFAULT NULL,
          PRIMARY KEY (id)
        )
        ENGINE = INNODB
        AUTO_INCREMENT = 2000000
        CHARACTER SET utf8
        COLLATE utf8_unicode_ci;";

    }

    public static function truncate(){
        $all = PostsMySqlModel::find();
        $all->delete();
    }


    public function commentsIncrement(){
        if($this->update(array('comments'=>$this->comments+1))){
            return $this->comments;
        };

        return false;
    }
    
    public function repostsIncrement(){
        if($this->update(array('reposts'=>$this->reposts+1))){
            return $this->reposts;
        };
        
        return false;
    }
    public function repostsDecrement(){
        if($this->update(array('reposts'=>$this->reposts-1))){
            return $this->reposts;
        };

        return false;
    }


    public function markDeleted(){
        return $this->update(array('deleted'=>1));
    }
    public function unMarkDeleted(){
        return $this->update(array('deleted'=>0));
    }

    public function getSource()
    {
        return "s_posts";
    }

    private function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date('Y-m-d H:i:s');
    }

    private function beforeUpdate()
    {
        // Set the modification date
        $this->updated_at = date('Y-m-d H:i:s');
    }

    private function metaData()
    {
        return array(
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => array(
                'id', 'post_type_id','parent_id', 'attached_images', 'author_id', 'title', 'message', 'sections', 'like_pos', 'like_neg', 'joined', 'reposts', 'favorite', 'external', 'comments',  'params' ,'created_at', 'updated_at', 'deleted'
            ),

            // Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => array(
                'id'
            ),

            // Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => array(
                'post_type_id', 'parent_id', 'attached_images', 'author_id', 'title', 'message', 'sections', 'like_pos', 'like_neg', 'joined', 'reposts', 'favorite', 'external',  'comments', 'params' ,'created_at', 'updated_at', 'deleted'
            ),

            // Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => array(
                'id', 'post_type_id', 'author_id'
            ),

            // Every column and their data types
            MetaData::MODELS_DATA_TYPES => array(
                'id'   => Column::TYPE_INTEGER,
                'post_type_id'=> Column::TYPE_INTEGER,
                'parent_id'=> Column::TYPE_INTEGER,
                'attached_images' => Column::TYPE_TEXT,
                'author_id' => Column::TYPE_INTEGER,
                'title' => Column::TYPE_VARCHAR,
                'message' => Column::TYPE_TEXT,
                'sections' => Column::TYPE_VARCHAR,
                'like_pos' => Column::TYPE_INTEGER,
                'like_neg'=> Column::TYPE_INTEGER,
                'params' => Column::TYPE_TEXT,
                'joined' => Column::TYPE_INTEGER,
                'reposts' => Column::TYPE_INTEGER,
                'favorite' => Column::TYPE_INTEGER,
                'external' => Column::TYPE_TEXT,
                'comments' => Column::TYPE_INTEGER,
                'created_at' => Column::TYPE_DATETIME,
                'updated_at'=> Column::TYPE_DATETIME,
                'deleted' => Column::TYPE_INTEGER
            ),

            // The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => array(
                'id'   => true,
                'post_type_id' => true,
                'parent_id'=> true,
                'author_id' => true,
                'like_pos' => true,
                'like_neg' => true,
                'joined'=>true,
                'reposts'=>true,
                'favorite'=>true,
                'comments'=>true,
                'deleted'=>true
            ),

            // The identity column, use boolean false if the model doesn't have
            // an identity column
            MetaData::MODELS_IDENTITY_COLUMN => 'id',

            // How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => array(

                'id'   => Column::BIND_PARAM_INT,
                'post_type_id'=> Column::BIND_PARAM_INT,
                'parent_id'=> Column::BIND_PARAM_INT,
                'attached_images' => Column::BIND_PARAM_STR,
                'author_id' => Column::BIND_PARAM_INT,
                'title' => Column::BIND_PARAM_STR,
                'message' => Column::BIND_PARAM_STR,
                'sections' => Column::BIND_PARAM_STR,
                'like_pos' => Column::BIND_PARAM_INT,
                'like_neg'=> Column::BIND_PARAM_INT,
                'joined'=> Column::BIND_PARAM_INT,
                'params' => Column::BIND_PARAM_STR,
                'reposts'=> Column::BIND_PARAM_INT,
                'favorite'=> Column::BIND_PARAM_INT,
                'external'=> Column::BIND_PARAM_STR,
                'comments'=> Column::BIND_PARAM_INT,
                'created_at' => Column::BIND_PARAM_STR,
                'updated_at'=> Column::BIND_PARAM_STR,
                'deleted'=> Column::BIND_PARAM_INT
            ),

            // Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => array(

            ),

            // Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => array(

            ),

            // Default values for columns
            MetaData::MODELS_DEFAULT_VALUES => array(

                'parent_id'=> null,
                'attached_images' => null,
                'title' => null,
                'message' => null,
                'sections' => null,
                'like_pos' => null,
                'like_neg'=> null,
                'joined'=> null,
                'params'=> null,
                'reposts'=> null,
                'favorite'=> null,
                'external'=> null,
                'comments'=> null,
                'created_at' => null,
                'updated_at'=> null,
                'deleted'=> null,
            ),

            // Fields that allow empty strings
            MetaData::MODELS_EMPTY_STRING_VALUES => array()
        );
    }
}