<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\TarantoolHelper\TarantoolAutoIncrement;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class PostsAuthorFeed{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

  /********************************
  * Лента сообщений.  posts_author_feed
  *   Цель:
  *      Хранение списка сообщений в разрезе пользователей. Т.е. это блог пользователя с репостами
  *      todo Реализовать возможность хранения комментариев. Т.к. комментарии также являются важной частью общения, то нужно в этой ленте отображать и комментарии пользователя.
  *      Сделать это можно таким образом (без изменения структуры): просто добавлять все комментарии, но имеющие не меньше x сисволов (чтобы простые смайлы не добавлять). isOwner ставим в 3 - комментарии            
  *    
  *
  *   Структура:
  *      - id (1)
  *      - authorId (2) - id пользователя (чья лента)
   *     - postId (3) - id сообщения
  *      - isOwner (4) - это собственное, иначе репост
  *      - createdAt (5)- дата создания (или добавления репоста)
  *      - createdAtAsString (6) '2012-01-01'
  *
  *
  *  Использование:
  *      - Блог пользователя - получить все сообщения в порядке создания (postId) для заданного пользователя.
  *              Возможность получить кол-во сообщений с группировокй по createdAtAsString.
  *
  *
  */
    private static $cSPACE_NAME_COMMON = 'posts_author_feed_9';




    public function initSchema($sUserName = 'app'){

        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME_COMMON, array('user'=>$sUserName, 'if_not_exists'=>true));

        //id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_COMMON , 'primary', 'hash', true, array(1, 'NUM'), true);

        //postId - удаление сообщений
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_COMMON , 'posts', 'tree', false, array(3, 'NUM'), true);

        //authorId isOwner createdAt - фильтр автор + (только мое/репосты) + сортировка
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_COMMON , 'author_owner', 'tree', false, array(2, 'NUM', 4, 'NUM', 5, 'NUM'), true);

        //authorId createdAt - фильтр автор + сортировка
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_COMMON , 'author', 'tree', false, array(2, 'NUM', 5, 'NUM'), true);

    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }



    public function spaceLength(){
        $result =  $this->_connection->evaluate("return box.space.".self::$cSPACE_NAME_COMMON.":len()");
        if(is_array($result) && count($result)>0){
            return $result[0];
        }
        return 0;
    }

    /**
     * Удалить все данные из спейсов
     */
    public function truncate(){
        try{

            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_COMMON);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME_COMMON)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_COMMON);
        }

    }


    public function drop(){
        try{

            TarantoolHelper::drop($this->_connection,  self::$cSPACE_NAME_COMMON);
            return true;

        }catch (\Exception $e){
            return false;
        }

    }



    public function addPost(EntityPost $entityPost, $nUserFeedId = null, $bTimeNow = false){

         /*
         * Приведим переменные в соответсвии с типами спейсов
         */

        $nId = TarantoolAutoIncrement::getNewId($this->_connection, self::$cSPACE_NAME_COMMON);
        $nAuthorId = $entityPost->getAuthorId();
        $nPostId = $entityPost->getId();
        $isOwner = 1;
        if(is_null($nUserFeedId)=== false){
            $isOwner = 0;
            $nAuthorId = $nUserFeedId;
        }

        $createdAtAsTimestamp = $entityPost->getCreatedAtAsTimestamp();
        $createdAtAsDateString = $entityPost->getCreatedAtAsDateString();
        if($bTimeNow === true){
            //Используется, если добавляется репост
            $createdAtAsTimestamp = time();
            $createdAtAsDateString = date("Y-m-d", $createdAtAsTimestamp);
        }

        $arData = array($nId, $nAuthorId, $nPostId, $isOwner, $createdAtAsTimestamp, $createdAtAsDateString);
        try{

            $this->_connection->insert(self::$cSPACE_NAME_COMMON, $arData);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME_COMMON)===false){
                $this->initSchema();
                $this->_connection->insert(self::$cSPACE_NAME_COMMON, $arData);
            }
        }

    }

    public function deletePost($nPostId){

        $nPostId = intval($nPostId);

        $arResult = $this->_connection->select(self::$cSPACE_NAME_COMMON, array($nPostId), "posts");

        for($i=0, $length=count($arResult); $i<$length; $i++){
            $this->_connection->delete(self::$cSPACE_NAME_COMMON, array( $arResult[$i][0]) , 'primary');
        }


    }


    private function _getIndexByOwner($isOwner){
        $index = 'author_owner';
        if($isOwner === null){
            $index = 'author';
        }

        return $index;
    }


    public function getCountOfPostsInFeed($nAuthorId, $isOwner = null){

       $key = $nAuthorId;
       if($isOwner === true){
           $key = $nAuthorId . ', 1';
       }elseif ($isOwner === false){
           $key = $nAuthorId . ', 0';
       }
        
        $sEval = 'return box.space.'.self::$cSPACE_NAME_COMMON.'.index.'.$this->_getIndexByOwner($isOwner).':count({'.$key.'})';
        
        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) === 0){
            return 0;
        }else{
            return $lResult[0];
        }

    }


    /**
     * Возвращает id сообщений
     *
     * @param $nAuthorId
     * @param null $nNumberOfBasket
     * @param int $nBasketSize
     * @param null $isOwner - null - без фильтра, true - только пользователя, false - только репосты
     * @return array|bool
     */

    public function getFeed($nAuthorId, $nNumberOfBasket = null, $nBasketSize = 10, $isOwner = null){

        /*
         * -- получим кол-во сообщений
         */

        $lCount = $this->getCountOfPostsInFeed($nAuthorId, $isOwner);


        $key = array($nAuthorId);
        if($isOwner === true){
            $key = array($nAuthorId, 1);
        }elseif ($isOwner === false){
            $key =array($nAuthorId, 0);
        }

        if($lCount === 0){
            return false;
        }else{

            $arOffset = Common::getOffset($nNumberOfBasket, $lCount, $nBasketSize);
            
            $arResult = $this->_connection->select(self::$cSPACE_NAME_COMMON, $key, $this->_getIndexByOwner($isOwner), $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);
            

            $arPostsIds = array();

            for($i=0, $length=count($arResult); $i<$length; $i++){
                array_push($arPostsIds, $arResult[$i][2]);
            }


            return array(
                "countOfBasket"=> $arOffset['countOfBasket'],
                "countOfPosts" => $lCount,
                "postIds"=>$arPostsIds,
                "numberOfBasket"=>$arOffset['numberOfBasket'],
                "basketSize"=>$nBasketSize,
                'isOwnerFilter'=>$isOwner
            );


        }


    }


    public function getPostsByDateForAuthor($nAuthorId)
    {

        $spaceName = self::$cSPACE_NAME_COMMON;
        $eval = <<<EOT

local function __PostsUserFeedGetPostsByDateForAuthor__(nAuthorId)

   local result = {};


   for _, tuple in
   box.space.$spaceName.index.author_owner:pairs({nAuthorId, 1}, {
       iterator = box.index.EQ}) do

       if result[tuple[6]] == nill then
           result[tuple[6]] = {
               count = 0,
               list = {}
           };
       end

       result[tuple[6]]["count"] = result[tuple[6]]["count"] + 1;
       table.insert(result[tuple[6]]["list"], tuple)

   end

   return result;
end

return __PostsUserFeedGetPostsByDateForAuthor__($nAuthorId)
EOT;

        $lResultEval =  $this->_connection->evaluate($eval);

        if(is_array($lResultEval) && count($lResultEval)>0){

           return $lResultEval[0];
        }
        return array();
    }


}