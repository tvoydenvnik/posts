<?php

namespace Tvoydenvnik\Posts\Models\Posts;


use Tvoydenvnik\Common\AppAnswer;
use Tvoydenvnik\Common\Lib\ArrayUtils;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsCacheService;
use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class PostsCacheTarantool implements IPostsCacheService{


    /**
     * @var $_connection \Tarantool
     */
    public $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'posts_v3';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM'), true);

        //expired - используется для удаления старых сообщений
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'expired', 'tree', false, array(2, 'NUM'), true);
    }

    public static function metaData(){

        return array(
            "id"=>array("num"=>1, "func"=>"getId"),
            "updated_at_time"=>array("num"=>2, "func"=>"getUpdateAtAsTimestamp"),//номер не менять
            "last_comments"=>array("num"=>3),//номер не менять
            "post_type_id"=>array("num"=>4, "func"=>"getPostTypeId"),
            "author_id"=>array("num"=>5, "func"=>"getAuthorId"),//Номер не менять, т.к. используется в PostsCacheTarantoolGetPosts
            "title"=>array("num"=>6, "func"=>"getTitle"),
            "message"=>array("num"=>7, "func"=>"getMessage"),
            "sections"=>array("num"=>8, "func"=>"getSections"),
            "params"=>array("num"=>9, "func"=>"getParams"),
            "like_pos"=>array("num"=>10, "func"=>"getLikePos"),
            "like_neg"=>array("num"=>11, "func"=>"getLikeNeg"),
            "reposts"=>array("num"=>12, "func"=>"getReposts"),
            "favorite"=>array("num"=>13, "func"=>"getFavorite"),
            "external"=>array("num"=>14, "func"=>"getExternal"),
            "comments"=>array("num"=>15, "func"=>"getComments"),
            "joined"=>array("num"=>16, "func"=>"getJoined"),
            "created_at"=>array("num"=>17, "func"=>"getCreatedAt"),
            "parent_id"=>array("num"=>18, "func"=>"getParentId"),
            "attached_images"=>array("num"=>19, "func"=>"getAttachedImages"),
            "updated_at"=>array("num"=>20, "func"=>"getUpdatedAt"),
            "last_photos"=>array("num"=>21),//номер не менять


        );
    }


    public static function entityPostToArray(EntityPost $entityPost){


        $arResult = array();
        $arMetaData = self::metaData();
        foreach($arMetaData as $key=>$value){
            if(isset($value["func"])){
                $val = call_user_func(array($entityPost, $value["func"]));
                if($val === 0 || $val === "" || (is_array($val) && count($val)===0)){
                    $val = null;
                }
                array_push($arResult, $val);
            }else{
                array_push($arResult, null);
            }

        }

        return $arResult;

    }

    public function spaceLength(){
        $result =  $this->_connection->evaluate("return box.space.".self::$cSPACE_NAME.":len()");
        if(is_array($result) && count($result)>0){
            return $result[0];
        }
        return 0;
    }


    private function prepareFromCache($arData){

        $arResult = array();
        $arMetaData = self::metaData();

        foreach($arMetaData as $key=>$value){
            $index = $value["num"]-1;
            if(array_key_exists($index, $arData)){

                if(is_null($arData[$index])==false){
                    $arResult[$key] = $arData[$index];
                }
            }

        }

        return $arResult;

    }

    public function delete($nAccountId){
        $nAccountId = intval($nAccountId);
        $this->_connection->delete(self::$cSPACE_NAME, array($nAccountId) , 'primary');

    }

    /**
     * @param $nPostId
     * @return bool|EntityPost
     */
    public function byId($nPostId){
        $nPostId = intval($nPostId);

        $lResult = $this->getPostsV3(array($nPostId), false, false, false);
        if($lResult === false || isset($lResult["posts"]) == false || isset($lResult["posts"][$nPostId]) == false){
            return false;
        }else{
            return $lResult["posts"][$nPostId];
        }
    }

    public function isExist($nPostId){

        return TarantoolHelper::isTupleExist($this->_connection, self::$cSPACE_NAME, $nPostId, 'primary');

    }

    public function addPost(EntityPost $entityPost){

      
        //todo возможно не стоит обновлять updated_at_time, если это личные сообщения.
        // todo - если не найдено соообщение, то может стоит его из mysql взять.


        $space = self::$cSPACE_NAME;

        $eval = <<<EOT
        
           local function tablelength(T)
              local count = 0
              for _ in pairs(T) do count = count + 1 end
              return count
            end

            local function __PostsCacheTarantoolAddPost(arData, nParentId, bUpdateComments, bUpdateLastPhotos)

                  box.space.$space:replace(arData);
                  
                  if bUpdateComments or bUpdateLastPhotos then
                  
                    local post = box.space.$space.index.primary:select({nParentId});
                    if post ~= nill and post[1] ~= nill then
                        post = post[1];
                                                                   
                        if bUpdateComments then
                            local comments = post[15];
                            if comments == nill then
                                comments = 1;
                            else
                                comments = comments + 1;
                            end
                            
                            local last_comments = post[3];
                            if last_comments == nill then
                                last_comments = {};                      
                            end
                            
                            local length = tablelength(last_comments);
                            if length > 2 then
                                last_comments = { last_comments[length-1], last_comments[length], arData[1]};
                            else
                                table.insert(last_comments, arData[1]);
                            end
                            
                            box.space.$space.index.primary:update(nParentId, {{'=', 15, comments }, {'=', 2, os.time() }, {'=', 3, last_comments }});
                        end
                        if bUpdateLastPhotos then
                                                                        
                            local last_photos = post[21];
                            if last_photos == nill then
                                last_photos = {};                      
                            end
                            
                            local length = tablelength(last_photos);
                            if length > 2 then
                                last_photos = { last_photos[length-1], last_photos[length], arData[1]};
                            else
                                table.insert(last_photos, arData[1]);
                            end
                            
                            box.space.$space.index.primary:update(nParentId, {{'=', 21, last_photos }});
                        end
                        
                    end
                  end
              
            end
            return __PostsCacheTarantoolAddPost(...)

EOT;

        $bUpdateComments = $entityPost->isPostTypeComment();
        $bUpdateLastPhotos = $entityPost->isPostTypePhotoAlbumItem();


        $this->_connection->evaluate($eval, array(self::entityPostToArray($entityPost), $entityPost->getParentId(), $bUpdateComments, $bUpdateLastPhotos));
    }

    /**
     * @param EntityPost $oEntityPost
     * @param $bIsAdmin
     * @return AppAnswer
     */
    public function updatePost(EntityPost $oEntityPost, $bIsAdmin){


        $entityInCache = $this->byId($oEntityPost->getId());
        if($entityInCache === false){
            return AppAnswer::create()->setError("Сообщение не найдено", $oEntityPost->postToArray())->setPublicErrorDesc("Сообщение не найдено");
        }

        if(intval($entityInCache->getAuthorId()) !== $oEntityPost->getAuthorId() && $bIsAdmin === false){
            return AppAnswer::create()->setError("Нет прав на изменение сообщения.", $oEntityPost->postToArray())->setPublicErrorDesc("У вас нет прав на изменение сообщения.");
        }


        if($oEntityPost->isSetTitle()){
            $entityInCache->setTitle( Common::emptyStringToNull($oEntityPost->getTitle()));
        }

        if($oEntityPost->isSetMessage()){
            $entityInCache->setMessage($oEntityPost->getMessage());
        }

        if($oEntityPost->isSetSections()){
            $entityInCache->setSections($oEntityPost->getSections());
        }

        if($oEntityPost->isSetAttachedImages()){
            $entityInCache->setAttachedImages($oEntityPost->getAttachedImages());
        }


        if($oEntityPost->isSetParams()){
            $newParams = $oEntityPost->getParams();
            $oEntityPost->setParams($entityInCache->getParams());
            $oEntityPost->updateParams($newParams);
            $entityInCache->setParams($oEntityPost->getParams());
        }



        $space = self::$cSPACE_NAME;
        $eval = <<<EOT
          local function __PostsCacheTarantoolUpdatePost(nPostId, sTitle, sMessage, arSections, arAttachedImages, arParams)
    
            local post = box.space.$space.index.primary:select({nPostId});
        
            if post ~= nill and post[1] ~= nill then
                post = post[1];
        
                box.space.$space.index.primary:update(nPostId,  {{'=', 6, sTitle }, {'=', 7, sMessage }, {'=', 8, arSections }, {'=', 19, arAttachedImages }, {'=', 9, arParams }});
                
            else
                return false;
            end
            
        end

            
        return __PostsCacheTarantoolUpdatePost(...);

EOT;

        $this->_connection->evaluate($eval, array($oEntityPost->getId(),
            $oEntityPost->getTitle(),
            $oEntityPost->getMessage(),
            $oEntityPost->getSections(),
            $oEntityPost->getAttachedImages(),
            $oEntityPost->getParams()));

        return AppAnswer::create()->setResult(array("entity"=>$oEntityPost));

    }

    

    public static function getLuaFuncGetPosts($bWithCall = true){

        $space = self::$cSPACE_NAME;
        $eval = <<<EOT
            local function __PostsCacheTarantoolGetPostsV1(arData, bWithAccount, bWithLastComments, bWithLastPhotos)

    local result = {};

    local lastComments = {};
    local lastPhotos = {};
    local needAccounts = {};
    local accounts = {};
    local notInCache = {};
    

    for key,value in ipairs(arData)
    do

        local post = box.space.$space.index.primary:select({value});
        if post ~= nill and post[1] ~= nill then
            post = post[1];
           
            if post[4] ~= nill then
                needAccounts[post[4]]=post[4];
            end
            
               
              
            if bWithLastComments == true and post[3] ~= null and type(post[3]) == 'table' then
                for keyLC,valueLC in ipairs(post[3])
                do
                    local postComments = box.space.$space.index.primary:select({valueLC});
                    if postComments ~= nill and postComments[1] ~= nill then
                        postComments = postComments[1];
                        lastComments[valueLC] = postComments;
                        
                        if postComments[4] ~= nill then
                            needAccounts[postComments[4]]=postComments[4];
                        end
                        
                    end
                end
            end
            
            if bWithLastPhotos == true and post[21] ~= null and type(post[21]) == 'table' then
                for keyLC,valueLC in ipairs(post[21])
                do
                    local postPhotos = box.space.$space.index.primary:select({valueLC});
                    if postPhotos ~= nill and postPhotos[1] ~= nill then
                        postPhotos = postPhotos[1];
                        lastPhotos[valueLC] = postPhotos;
                                             
                    end
                end
            end
            
            

            result[value] = post;
            
        else
            table.insert(notInCache, value)
        end

    end
    
    
    if bWithAccount and box.space.accounts then
        for key,value in ipairs(arData)
        do
            local account = box.space.accounts.index.primary:select({value});
            if account ~= nill and account[1]  ~= nill then
                account = account[1];
                accounts[value] = account;
            end
        end
    end
   
    return {
        posts = result,
        lastComments = lastComments,
        needAccounts = needAccounts,
        accounts = accounts,
        notInCache = notInCache,
        lastPhotos = lastPhotos
    };
end

EOT;

        if($bWithCall === true){
            $eval = $eval . "\n" . "return __PostsCacheTarantoolGetPostsV1(...)";
        }
        return $eval;
    }

    private function getPostsV3(array $arPostsId, $bWithAccount = true, $bWithLastComments = true, $bWithLastPhotos = true)
    {
        
        $eval = self::getLuaFuncGetPosts();

        $lResultEval =  $this->_connection->evaluate($eval, array($arPostsId, $bWithAccount, $bWithLastComments, $bWithLastPhotos));

        if(is_array($lResultEval) && count($lResultEval)>0){

            $lPosts = array();
            $lLastComments = array();
            $lLastPhotos = array();

            foreach($lResultEval[0]['posts'] as $postId=>$arrayOfPosts){
                if(count($arrayOfPosts)>0){
                    $lPosts[$postId] = EntityPost::create($this->prepareFromCache($arrayOfPosts));
                }
            }

            foreach($lResultEval[0]['lastComments'] as $postId=>$arrayOfPosts){
                if(count($arrayOfPosts)>0){
                    $lLastComments[$postId] = EntityPost::create($this->prepareFromCache($arrayOfPosts));
                }
            }

            foreach($lResultEval[0]['lastPhotos'] as $postId=>$arrayOfPosts){
                if(count($arrayOfPosts)>0){
                    $lLastPhotos[$postId] = EntityPost::create($this->prepareFromCache($arrayOfPosts));
                }
            }


            $lResult = array();
            $lResult["posts"]=$lPosts;
            $lResult["lastComments"]=$lLastComments;
            $lResult["needAccounts"]= $lResultEval[0]['needAccounts'];
            $lResult["notInCache"]= $lResultEval[0]['notInCache'];
            $lResult["lastPhotos"]= $lLastPhotos;
            $lResult["accounts"]= $lResultEval[0]['accounts'];//todo в entity

            
            return $lResult;
        }
        return false;
    }


    /**
     *
     * Получим все сообщения, которые есть в кэше, а также массив id сообщений, которых нет.
     * Возвращает объект:
     *      posts - ассоциативный массив EntityPost - найденные сообщения в кэше
     *      notInCache - массив id сообщений, которые не присутсвуют в кэше.
     * @param array $arPostsId
     * @return array
     */
    public function getPostsFromCache(array $arPostsId){

        $arPostsIdNew = ArrayUtils::getNumericUniqueArray($arPostsId);
        
        return $this->getPostsV3($arPostsIdNew);

    }


    /*
     * Удалить n записей, к которым было самое меньшее число обращений. Т.е. это не нужные данные
     */
    public function deleteItemsByExpired($nLimit = 100){


        $sEval = "return box.space.".self::$cSPACE_NAME.".index.expired:select({nill}, { limit=".$nLimit." })";

        $lResultEval = $this->_connection->evaluate($sEval);

        if(is_array($lResultEval) && count($lResultEval)>0){
            $lResult = array();

            foreach($lResultEval[0] as $index=>$data){
                $this->_connection->delete(self::$cSPACE_NAME,array($data[0]));
            }

            return count($lResultEval[0]);
        }

        return 0;

    }


    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}