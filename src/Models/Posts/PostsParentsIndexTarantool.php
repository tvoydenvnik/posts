<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

/*
 * Индекс по parent_id.
 *      Основная цель:
 *              возможность получить подчиненные элементы с limit + offcet
 *              возможность получить последние 3 комментария или последние 3 фото для альбома.
 *              Комментарии здесь хранятся без без иерархии. Т.е. только суппер парент
 *
 * 1 - parent_post_id
 * 2 - post_id
 * 3 - created_at
 */
class PostsParentsIndexTarantool{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'posts_parents_index';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //parent_post_id  post_id  - удаление, добавление. tree - чтобы была возможность удалить все.
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'NUM'), true);
        //parent_post_id created_at - цель получить  комментарии с limit + offcet
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'order', 'tree', true, array(1, 'NUM',3, 'NUM'), true);
    }



    public function addPost(EntityPost $parentEntity, EntityPost $entityPost){

        //Добавим новое сообщение
        $this->_connection->insert(self::$cSPACE_NAME, array($parentEntity->getId(), $entityPost->getId(), $entityPost->getCreatedAtAsTimestamp()));
        
    }



    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}