<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsSectionsFeedService;
use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

class PostsSectionsFeedService implements IPostsSectionsFeedService{


    /**
     * @var $_connection \Tarantool
     */
    public $_connection;


    /*
     * Сообщения в разрезе разделов
     *
     * У нас может быть множество разделов. Но индексировать будем не все, а только то,
     *      - что касается приложения (дневники питания, тренировок, рецепты, планы питания, программы тренировок),
     *      - истории успеха
     *
     * Остальное, если понадобиться, то можно индексировать с помощью сфинкса + кэш.
     *
     * Т.е. сяда попадает, то что действительно важно отделить. Возможно лучше это все перенести и в сфинкс, но пока оставим.
     *
     * Чтобы получить кол-во сообщений по разделам, просто перебераем их, т.к. их не такуж и много
     * Структура
     *  - postId (1)
     *  - authorId (2)
     *  - sectionId (3)
     *  - created_at (4)
     */

    public static $cSPACE_NAME_SECTIONS = 'posts_sections';

    public static $cSPACE_NAME_SECTIONS_COUNTS = 'posts_sections_counts';
    

    public function initSchema($sUserName = 'app'){

        //разделы

        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME_SECTIONS, array('user'=>$sUserName, 'if_not_exists'=>true));

        //sectionId postId - в разрезе всех сообщений
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_SECTIONS , 'primary', 'tree', true, array(3, 'NUM', 1, 'NUM'), true);

        //sectionId created_at - в разрезе всех сообщений
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_SECTIONS , 'common', 'tree', false, array(3, 'NUM', 4, 'NUM'), true);

        //sectionId authorId created_at - в разрезе автора
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_SECTIONS , 'author', 'tree', false, array(3, 'NUM', 2, 'NUM', 4, 'NUM'), true);

        //postId - необходимо при удалении/изменении сообщения
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_SECTIONS , 'post_id', 'tree', false, array(1, 'NUM'), true);


        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME_SECTIONS_COUNTS, array('user'=>$sUserName, 'if_not_exists'=>true));

        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME_SECTIONS_COUNTS , 'primary', 'hash', true, array(1, 'STR'), true);

    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }


    /**
     * Удалить все данные из спейсов
     */
    public function truncate(){
        try{

            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_SECTIONS);
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_SECTIONS_COUNTS);

        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME_SECTIONS)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_SECTIONS);
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME_SECTIONS_COUNTS);
        }

    }


    public function drop(){
        try{

            TarantoolHelper::drop($this->_connection,  self::$cSPACE_NAME_SECTIONS);
            return true;

        }catch (\Exception $e){
            return false;
        }

    }


    private function getIncDecEval($key, $bInc){
        $do = "inc";
        if($bInc === false){
            $do = "dec";
        }
        return "box.space.".self::$cSPACE_NAME_SECTIONS_COUNTS.":$do{'".$key."'}";
    }

    public function addPost(EntityPost $entityPost, $bIndexAll = false){

        /*
         * Приведим переменные в соответсвии с типами спейса
         */
        $nAuthorId = $entityPost->getAuthorId();
        $nPostId = $entityPost->getId();


        if($bIndexAll === true){
            $arSections = $entityPost->getSections();
        }else{
            //индексируем не все.
            $arSections = $entityPost->getSectionsToIndexInTarantool();    
        }
        
        
        if(is_array($arSections) && count($arSections)>0){

            for($i=0, $length = count($arSections); $i< $length; $i++){

                $this->_connection->insert(self::$cSPACE_NAME_SECTIONS,array($nPostId, $nAuthorId, intval($arSections[$i]), $entityPost->getCreatedAtAsTimestamp()));

                $key = $nAuthorId . '-' . intval($arSections[$i]);
                $this->_connection->evaluate($this->getIncDecEval($key, true));

                $keyCommon = "all" . '-' . intval($arSections[$i]);
                $this->_connection->evaluate($this->getIncDecEval($keyCommon, true));


            }
        }



    }

    public function deletePost($nPostId){

        $arResult = $this->_connection->select(self::$cSPACE_NAME_SECTIONS, array($nPostId), "post_id");
        foreach($arResult as $key=>$value){
            $this->_connection->delete(self::$cSPACE_NAME_SECTIONS, array( intval($value[2]),intval($value[0])),'primary' );


            $key = intval($value[1]) . '-' . intval($value[2]);
            $this->_connection->evaluate($this->getIncDecEval($key, false));

            $keyCommon = "all" . '-' . intval($value[2]);
            $this->_connection->evaluate($this->getIncDecEval($keyCommon, false));

        }
    }

    public function updatePost(EntityPost $entityPost){


        $nPostId = intval($entityPost->getId());
        $nAuthorId = intval($entityPost->getAuthorId());
        $arNewSections = $entityPost->getSectionsToIndexInTarantool();
        $arHasSections = array();
        $arResult = $this->_connection->select(self::$cSPACE_NAME_SECTIONS, array($nPostId), "post_id");
        
        foreach($arResult as $key=>$value){

            if(in_array($value[2], $arNewSections) === false){
                $this->_connection->delete(self::$cSPACE_NAME_SECTIONS, array(intval($value[2]),intval($value[0])),'primary' );

                $key = intval($value[1]) . '-' . intval($value[2]);
                $this->_connection->evaluate($this->getIncDecEval($key, false));

                $keyCommon = "all" . '-' . intval($value[2]);
                $this->_connection->evaluate($this->getIncDecEval($keyCommon, false));

            }else{
                array_push($arHasSections, $value[2]);
            }

        }
        
        $arNeedAddSections = array_values(array_diff($arNewSections, $arHasSections));

        for($i=0, $length = count($arNeedAddSections); $i< $length; $i++){

            try{
                $this->_connection->insert(self::$cSPACE_NAME_SECTIONS,array($nPostId, $nAuthorId, intval($arNeedAddSections[$i]), $entityPost->getCreatedAtAsTimestamp()));

                $key = $nAuthorId . '-' . intval($arNeedAddSections[$i]);
                $this->_connection->evaluate($this->getIncDecEval($key, true));

                $keyCommon = "all" . '-' . intval($arNeedAddSections[$i]);
                $this->_connection->evaluate($this->getIncDecEval($keyCommon, true));


            }catch (\Exception $e){
                $test= 1;//todo
            }

        }

    }


    public function getCountOfPostsInFeedPerSection($nSection, $nAuthorId = null){

        $sEval = '';
        if($nAuthorId === null){
            $keyCommon = "all" . '-' . intval($nSection);
            $sEval = 'return box.space.'.self::$cSPACE_NAME_SECTIONS_COUNTS.'.index.primary:select({\''.$keyCommon.'\'})';
        }else{
            $key = "".intval($nAuthorId) . '-' . intval($nSection);
            $sEval = 'return box.space.'.self::$cSPACE_NAME_SECTIONS_COUNTS.'.index.primary:select({\''.$key.'\'})';
        }

        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) > 0 && count($lResult[0]) > 0 && count($lResult[0][0]) > 0){
            return $lResult[0][0][1];
        }else{
            return 0;
        }

    }



    public function getFeedPerSection($nSection, $nAuthorId = null, $nNumberOfBasket = null, $nBasketSize = 10, $bGetPostsData = true)
    {
        //todo реализовать кэширование кол-ва, 12 против 3

        $__getOffset = Common::getOffsetLua();
        $getLuaFuncGetPosts = PostsCacheTarantool::getLuaFuncGetPosts(false);

        $space = self::$cSPACE_NAME_SECTIONS;
        $spaceCount = self::$cSPACE_NAME_SECTIONS_COUNTS;

        $eval = <<<EOT
        
        $__getOffset
        $getLuaFuncGetPosts
        
       local function __getFeedPostsSectionsFeed(nSection, nAuthorId, nNumberOfBasket, nBasketSize, bWithPosts)

            local nCountOfPosts =  0;
         
            if nAuthorId == nill then
               --nCountOfPosts = box.space.$space.index.primary:count({nSection});              
               local nCountOfPostsR = box.space.$spaceCount.index.primary:select({'all-'.. nSection})
               if nCountOfPostsR[1] then
                    nCountOfPosts = nCountOfPostsR[1][2]
               end
            else
                --nCountOfPosts = box.space.$space.index.author:count({nSection, nAuthorId});               
                nCountOfPostsR = box.space.$spaceCount.index.primary:select({nAuthorId .. '-'.. nSection})
                if nCountOfPostsR[1] then
                    nCountOfPosts = nCountOfPostsR[1][2]
               end
            end
            

            local postIds = {};
            local posts = {};
            
            if nCountOfPosts == 0 then
                return false;
            end
            
            local getOffsetResult = __getOffset(nNumberOfBasket, nCountOfPosts, nBasketSize);
            
            local result = nill;
            
            if nAuthorId == nill then
                result = box.space.$space.index.common:select({nSection}, {
                    iterator = 'REQ',
                    limit = getOffsetResult["limit"],
                    offset = getOffsetResult["offset"]
                })
              
            else
                result = box.space.$space.index.author:select({nSection, nAuthorId}, {
                    iterator = 'REQ',
                    limit = getOffsetResult["limit"],
                    offset = getOffsetResult["offset"]
                })          
            end
            
            
            if result ~= nill  then           
                for key,value in ipairs(result)
                do
                    table.insert(postIds, value[1])
                end
                
                if bWithPosts then
                    posts = __PostsCacheTarantoolGetPostsV1(postIds, true, true);
                end
            end
            
            
            getOffsetResult['postIds'] = postIds;
            getOffsetResult['countOfPosts'] = nCountOfPosts;
            getOffsetResult['basketSize'] = nBasketSize;
            if bWithPosts then
                getOffsetResult['posts'] = posts;
            end
            
            
            return getOffsetResult;
            
        end
        
        return __getFeedPostsSectionsFeed(...);
       
EOT;

        $lResult = $this->_connection->evaluate($eval, array($nSection, $nAuthorId, $nNumberOfBasket, $nBasketSize, $bGetPostsData));

        if(is_array($lResult) && count($lResult)>0 && is_array($lResult[0])){
            return $lResult[0];
        }

        return false;

        
    }


    public function spaceLength(){
        $result =  $this->_connection->evaluate("return box.space.".self::$cSPACE_NAME_SECTIONS.":len()");
        if(is_array($result) && count($result)>0){
            return $result[0];
        }
        return 0;
    }
}

