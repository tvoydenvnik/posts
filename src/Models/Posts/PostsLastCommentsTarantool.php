<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

/*
 * В данной индексе храниться последние 3 комментария (id)
 * Храним, только для сообщения
 *
 * 1 - parent_post_id
 * 2 - post_ids - массив
 */
class PostsLastCommentsTarantool{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'posts_last_comments';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //parent_post_id
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM'), true);
    }



    public function addPost(EntityPost $entityPost){

        //todo оптимизация в один запрос

        //Добавим новое сообщение
        $this->_connection->insert(self::$cSPACE_NAME, array($entityPost->getParentId(), $entityPost->getId(), $entityPost->getCreatedAtAsTimestamp()));
        
    }



    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}