<?php

namespace Tvoydenvnik\Posts\Models\Posts;

use Phalcon\Db\Column;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\MetaData;
use Tvoydenvnik\Posts\Entity\EntityPost;


class ExternalIndexMySqlModel extends Model
{

    private $id;

    public $post_id = "";
    public $external_sha1 = "";

    /**
     * @var $created_at integer
     */
    public $created_at;

    public function initialize()
    {
        $this->useDynamicUpdate(true);
    }


//    /**
//     * @param $nPostId
//     * @return ExternalIndexMySqlModel
//     */
//    private static function byId($nPostId){
//        $result = ExternalIndexMySqlModel::find($nPostId);
//        if($result->count() === 0){
//            return false;
//        }else{
//            $result->rewind();
//            while ($result->valid()) {
//                return $result->current();
//            }
//
//        }
//    }


    /**
     * @param $ExternalSha1
     * @return ExternalIndexMySqlModel|bool
     */
    public static function byIdExternal($ExternalSha1){
        $result = ExternalIndexMySqlModel::find(array("external_sha1"=>$ExternalSha1));
        if($result->count() === 0){
            return false;
        }else{
            $result->rewind();
            while ($result->valid()) {
                
                $current = $result->current();
                
                return intval($current->post_id);
            }

        }
    }
    
    public static function addPost(EntityPost $entity){
        if($entity->isSetExternal()===false){
            return false;
        }
        $data = new ExternalIndexMySqlModel();
        $data->post_id = $entity->getId();
        $data->external_sha1 = $entity->externalToSha1();
        return $data->save();
    }

    private function _getCreateTableQuery(){
        return "CREATE TABLE test.s_post_external_index (
              id int(18) NOT NULL AUTO_INCREMENT,
              post_id int(18) DEFAULT NULL COMMENT 'id сообщения',
              external_sha1 char(41) NOT NULL COMMENT 'sha1 от ключа',
              created_at datetime DEFAULT NULL,
              PRIMARY KEY (id),
              UNIQUE INDEX UK_s_post_external_index_external_sha1 (external_sha1),
              UNIQUE INDEX UK_s_post_external_index_post_id (post_id)
            )
            ENGINE = INNODB
            CHARACTER SET utf8
            COLLATE utf8_unicode_ci;";

    }

    public static function truncate(){
        $all = ExternalIndexMySqlModel::find();
        $all->delete();
    }


    public function getSource()
    {
        return "s_post_external_index";
    }

    private function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date('Y-m-d H:i:s');
    }

    private function metaData()
    {
        return array(
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => array(
                'id', 'external_sha1','post_id' ,'created_at'
            ),

            // Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => array(
                'id'
            ),

            // Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => array(
                'external_sha1','post_id' ,'created_at'
            ),

            // Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => array(
                'id', 'external_sha1','post_id'
            ),

            // Every column and their data types
            MetaData::MODELS_DATA_TYPES => array(
                'id'   => Column::TYPE_INTEGER,
                'external_sha1'=> Column::TYPE_TEXT,
                'post_id'=> Column::TYPE_INTEGER,
                'created_at' => Column::TYPE_DATETIME
            ),

            // The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => array(
                'id'   => true,
                'post_id' => true
            ),

            // The identity column, use boolean false if the model doesn't have
            // an identity column
            MetaData::MODELS_IDENTITY_COLUMN => 'id',

            // How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => array(
                'id'   => Column::BIND_PARAM_INT,
                'external_sha1'=> Column::BIND_PARAM_STR,
                'post_id'=> Column::BIND_PARAM_INT,
                'created_at' => Column::BIND_PARAM_STR,
            ),

            // Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => array(

            ),

            // Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => array(

            ),

            // Default values for columns
            MetaData::MODELS_DEFAULT_VALUES => array(
                'created_at' => null
            ),

            // Fields that allow empty strings
            MetaData::MODELS_EMPTY_STRING_VALUES => array()
        );
    }
}