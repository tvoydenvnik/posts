<?php

namespace Tvoydenvnik\Posts\Models\Posts;


use Tvoydenvnik\Common\AppAnswer;
use Tvoydenvnik\Common\Lib\JSON;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Interfaces\IPostsDBService;
use Tvoydenvnik\Posts\Utils\Common;

class PostsMySqlRepository  implements IPostsDBService{


    /**
     * @param EntityPost $oEntityPost
     * @return bool|EntityPost
     */
    public function addPost(EntityPost $oEntityPost){

        $newPost = new PostsMySqlModel();

        $newPost->author_id = $oEntityPost->getAuthorId();
        $newPost->post_type_id = $oEntityPost->getPostTypeId();
        $newPost->title = Common::emptyStringToNull($oEntityPost->getTitle());
        $newPost->message = $oEntityPost->getMessageForSaveInRepository();
        $newPost->sections = $oEntityPost->getSectionsForSaveInRepository();
        $newPost->params = $oEntityPost->getParamsForSaveInRepository();
        //$newPost->views = Common::zeroToNull($oEntityPost->getViews());
        $newPost->reposts = Common::zeroToNull($oEntityPost->getReposts());
        $newPost->favorite = Common::zeroToNull($oEntityPost->getFavorite());
        $newPost->comments = Common::zeroToNull($oEntityPost->getComments());
        $newPost->like_neg = Common::zeroToNull($oEntityPost->getLikeNeg());
        $newPost->like_pos = Common::zeroToNull($oEntityPost->getLikePos());
        $newPost->parent_id = Common::zeroToNull($oEntityPost->getParentId());
        $newPost->external = $oEntityPost->getExternalForSaveInRepository();
        $newPost->attached_images = $oEntityPost->getAttachedImagesForSaveInRepository();


        $bSave = $newPost->save();
        if($bSave == false){
            return false;
        }else{
            $oEntityPost->setId($newPost->getId());
            $oEntityPost->setCreatedAt($newPost->created_at);
            return $oEntityPost;
        }

    }

    /**
     * Обновим сообщение: пользователь отредактировал его.
     *          Следующие поля заменяются (если они заданны):
     *              - title
     *              - message
     *              - sections
     *              - attached_images
     *              - post_type_id
     *          Следующие поля (объединяются)
     *              - params
     *
     * @param EntityPost $oEntityPost
     * @param $bIsAdmin
     * @return AppAnswer
     */
    public function updatePost(EntityPost $oEntityPost, $bIsAdmin = false){


        $oPost = PostsMySqlModel::byId($oEntityPost->getId());

        if($oPost===false){
            return AppAnswer::create()->setError("Сообщение не найдено", $oEntityPost->postToArray())->setPublicErrorDesc("Сообщение не найдено");
        }

        if(intval($oPost->author_id) !== $oEntityPost->getAuthorId() && $bIsAdmin === false){
            return AppAnswer::create()->setError("Нет прав на изменение сообщения.", $oEntityPost->postToArray())->setPublicErrorDesc("У вас нет прав на изменение сообщения.");
        }

        
        if($oEntityPost->isSetTitle()){
            $oPost->title = Common::emptyStringToNull($oEntityPost->getTitle());    
        }

        if($oEntityPost->isSetMessage()){
            $oPost->message = $oEntityPost->getMessageForSaveInRepository();
        }

        if($oEntityPost->isSetSections()){
            $oPost->sections = $oEntityPost->getSectionsForSaveInRepository();
        }

        if($oEntityPost->isSetAttachedImages()){
            $oPost->attached_images = $oEntityPost->getAttachedImagesForSaveInRepository();
        }


        if($oEntityPost->isSetParams()){
            $newParams = $oEntityPost->getParams();
            $oEntityPost->setParams($oPost->params);
            $oEntityPost->updateParams($newParams);
            $oPost->params = $oEntityPost->getParamsForSaveInRepository();
        }
        

//        if($oEntityPost->isSetPostTypeId() && $oEntityPost->getPostTypeId()!=intval($oPost->post_type_id)){
//            $oPost->post_type_id = $oEntityPost->getPostTypeId();
//        }

//        if(count($oPost->getChangedFields()) == 0){
//            return true;
//        }



        $bResult = $oPost->update();
        //$oPost->getWriteConnection()->getSQLStatement();
        if($bResult!== false){
            $entity =  EntityPost::create($oPost->postToArray());
            return AppAnswer::create()->setResult(array("entity"=>$entity));
        }else{
            return AppAnswer::create()->setError("Ошибка на сервере при изменении сообщения", $oEntityPost->postToArray())->setPublicErrorDesc("Ошибка на сервере при изменении сообщения");
        }

    }

    public function markDeleted($nPostId, $nAuthorId, $bIsAdmin = false){

        $oPost = PostsMySqlModel::byId($nPostId);

        if($oPost!==false && ( intval($oPost->author_id) === intval($nAuthorId) || $bIsAdmin === true) ){
            return $oPost->markDeleted();
        }

        return false;

    }

    public function unMarkDeleted($nPostId, $nAuthorId, $bIsAdmin = false){

        $oPost = PostsMySqlModel::byId($nPostId);

        if($oPost!==false && ( intval($oPost->author_id) === intval($nAuthorId) || $bIsAdmin === true) ){
            return $oPost->unMarkDeleted();
        }

        return false;

    }

    public function incrementPostComments(EntityPost $oEntityPost){
        $oPost = PostsMySqlModel::byId($oEntityPost->getId());
        if($oPost!==false){
            return $oPost->commentsIncrement();
        }
        return false; 
    }


   
    
//    public function updateViews($nPostId, $nViews){
//
//        $oPost = PostsMySqlModel::byId($nPostId);
//
//        if($oPost!==false){
//            return $oPost->updateViews($nViews);
//        }
//        return false;
//    }




    /**
     * @param $nPostId
     * @return bool
     */
    public function deleteById($nPostId){
        $result = PostsMySqlModel::byId($nPostId);
        if($result === false){
            return false;
        }else{
            return $result->delete();
        }
    }


    /**
     * @param array $arPostsId
     * @param bool $bAsAssoc - true (возвращаем как ассоциативный массив EntityPost, где ключ id сообщения), иначе - массив
     * @return array
     */
    public function getPosts(array $arPostsId, $bAsAssoc = true){

        $result = PostsMySqlModel::find(
            array(
                'id IN ({ids:array})',
                'bind' => array(
                    'ids' => array_values($arPostsId)
                )
            )
        );

        $arResult = array();

        $result->rewind();
        while ($result->valid()) {

            /**
             * @var $currentPost PostsMySqlModel
             */
            $currentPost = $result->current();

            $arPostArray = $currentPost->postToArray();

            $entity =  EntityPost::create($arPostArray);

            if($bAsAssoc == true){
                $arResult[$arPostArray['id']] =  $entity;
            }else{
                array_push($arResult,$entity);
            }

            $result->next();
        }

        return $arResult;

    }


    public function truncate(){
        PostsMySqlModel::truncate();
    }


    public function repostsIncrement($nPostId){

        $oPost = PostsMySqlModel::byId($nPostId);
        if($oPost!==false){
            return $oPost->repostsIncrement();
        }
        return false;

    }

    public function repostsDecrement($nPostId){

        $oPost = PostsMySqlModel::byId($nPostId);
        if($oPost!==false){
            return $oPost->repostsDecrement();
        }
        return false;

    }

}
