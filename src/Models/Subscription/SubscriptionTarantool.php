<?php

namespace Tvoydenvnik\Posts\Models\Subscription;



use Tvoydenvnik\Posts\Utils\Common;
use Tvoydenvnik\TarantoolHelper\TarantoolHelper;

/*
 * Подписка пользователей друг на друга
 *
 * 1 - Кто
 * 2 - На кого
 */
class SubscriptionTarantool{


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    public function setConnection(\Tarantool $con){
        $this->_connection = $con;
    }


    private static $cSPACE_NAME = 'subscription5';


    public function initSchema($sUserName = 'app'){
        TarantoolHelper::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>$sUserName, 'if_not_exists'=>true));
        //Кто  На_кого
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'NUM'), true);
        //На_кого Кто
        TarantoolHelper::createIndex($this->_connection, self::$cSPACE_NAME , 'onwhom', 'tree', true, array(2, 'NUM',1, 'NUM'), true);
    }


    /*
     * Подписан ли $who на $onWhom
     */
    public function isSubscribeOn($who, $onWhom){
        $who = intval($who);
        $onWhom = intval($onWhom);
        $arResult = $this->_connection->select(self::$cSPACE_NAME, array($who, $onWhom), "primary");

        return (count($arResult)>0?true:false);

    }

    /*
     * Подписаны ли два пользователя друг на друга
     */
    public function isSubscribeUponEachOther($who1, $who2){

        $one = $this->isSubscribeOn($who1, $who2);
        if($one == true){
            return $this->isSubscribeOn($who2, $who1);
        }
        return false;
    }


    /*
     * $who подписывается на $onWhom
     */
    public function subscribeOn($who, $onWhom){
        $who = intval($who);
        $onWhom = intval($onWhom);
        if($this->isSubscribeOn($who, $onWhom) == false){
            $this->_connection->insert(self::$cSPACE_NAME, array($who, $onWhom));
        }
    }

    /*
     * $who отписывается от $onWhom
     */
    public function subscribeOff($who, $onWhom){
        $who = intval($who);
        $onWhom = intval($onWhom);
        $this->_connection->delete(self::$cSPACE_NAME, array($who, $onWhom) , 'primary');
    }

    /**
     * Кто подписался на человека
     * @param $onWhom
     * @param null $nNumberOfBasket
     * @param int $nBasketSize
     * @return array|bool
     */
    public function whoSubscribedOn($onWhom, $nNumberOfBasket = null, $nBasketSize = 10){

        $lCount = $this->getCountSubscribersOnWhom($onWhom);

        if($lCount === 0){
            return false;
        }else{

            $arOffset = Common::getOffset($nNumberOfBasket, $lCount, $nBasketSize);

            $arResult = $this->_connection->select(self::$cSPACE_NAME, array($onWhom), "onwhom", $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);

            $arUserIds = array();

            for($i=0, $length=count($arResult); $i<$length; $i++){
                array_push($arUserIds, $arResult[$i][0]);
            }


            return array(
                "countOfBasket"=> $arOffset['countOfBasket'],
                "countOfUsers" => $lCount,
                "userIds"=>$arUserIds,
                "numberOfBasket"=>$arOffset['numberOfBasket'],
                "basketSize"=>$nBasketSize
            );


        }

    }

    /**
     * На кого подписан человек
     * @param $who
     * @param null $nNumberOfBasket
     * @param int $nBasketSize
     * @return array|bool
     */
    public function onWhomSubscribed($who, $nNumberOfBasket = null, $nBasketSize = 10){
        $lCount = $this->getCountSubscribesWho($who);

        if($lCount === 0){
            return false;
        }else{

            $arOffset = Common::getOffset($nNumberOfBasket, $lCount, $nBasketSize);

            $arResult = $this->_connection->select(self::$cSPACE_NAME, array($who), "primary", $arOffset['limit'], $arOffset['offset'], TARANTOOL_ITER_REQ);

            $arUserIds = array();

            for($i=0, $length=count($arResult); $i<$length; $i++){
                array_push($arUserIds, $arResult[$i][1]);
            }


            return array(
                "countOfBasket"=> $arOffset['countOfBasket'],
                "countOfUsers" => $lCount,
                "userIds"=>$arUserIds,
                "numberOfBasket"=>$arOffset['numberOfBasket'],
                "basketSize"=>$nBasketSize
            );


        }
    }

    /*
     * Получить колво подписавшихся на $onWhom
     */
    public function getCountSubscribersOnWhom($onWhom){

        $sEval = 'return box.space.'.self::$cSPACE_NAME.'.index.onwhom:count({'.$onWhom.'})';
        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) === 0){
            return 0;
        }else{
            return $lResult[0];
        }
    }

    /*
     * Получить колво подписок $who
     */
    public function getCountSubscribesWho($who){

        $sEval = 'return box.space.'.self::$cSPACE_NAME.'.index.primary:count({'.$who.'})';
        $lResult = $this->_connection->evaluate($sEval);
        if(count($lResult) === 0){
            return 0;
        }else{
            return $lResult[0];
        }
    }

    public function truncate(){
        try{
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }catch (\Exception $e){
            if(TarantoolHelper::isSpaceExist($this->_connection, self::$cSPACE_NAME)===false){
                $this->initSchema();
            }
            TarantoolHelper::truncate($this->_connection,  self::$cSPACE_NAME);
        }
    }
}