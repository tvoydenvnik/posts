<?php
namespace Tvoydenvnik\Posts\Constants;


/*
Тип сообщения.

Определяет сущность записи.
*/
class PostTypes {

    
    public static $cPOST_TYPE_RECIPE = 1;
    public static $cPOST_TYPE_FOOD_SPORT_DIARY = 2;//дневник питания и тренировок
    public static $cPOST_TYPE_MESSAGE = 3;//Обычное сообщение
    public static $cPOST_TYPE_FOOD_DIARY = 4;// дневник питания
    public static $cPOST_TYPE_SPORT_DIARY = 5;//дневник тренировок
    public static $cPOST_TYPE_PHOTO_ALBUM = 6;//альбом фотогалереи
    public static $cPOST_TYPE_PHOTO_ALBUM_ITEM = 7;//фотография альбома
    public static $cPOST_TYPE_COMMENT = 8;//клмментарий
    public static $cPOST_TYPE_CHAT = 9;//чат двух пользователей. Общение уже комментариями.

    //todo
}