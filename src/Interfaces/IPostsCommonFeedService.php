<?php

namespace Tvoydenvnik\Posts\Interfaces;

use Tvoydenvnik\Posts\Entity\EntityPost;

interface IPostsCommonFeedService {


    public function addPost(EntityPost $entityPost);

    public function deletePost($nPostId);

    public function getFeed($nNumberOfBasket, $nBasketSize);

    public function getCountOfPostsInFeed();

    public function truncate();
}