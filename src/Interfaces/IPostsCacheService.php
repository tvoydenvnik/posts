<?php

namespace Tvoydenvnik\Posts\Interfaces;

use Tvoydenvnik\Posts\Entity\EntityPost;

interface IPostsCacheService {


    public function addPost(EntityPost $entityPost);

    public function updatePost(EntityPost $oEntityPost, $bIsAdmin);

    public function byId($nPostId);
    
    public function delete($nAccountId);

    public function getPostsFromCache(array $arPostsId);

    public function truncate();
}