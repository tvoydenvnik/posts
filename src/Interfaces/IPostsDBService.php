<?php

namespace Tvoydenvnik\Posts\Interfaces;


use Tvoydenvnik\Common\AppAnswer;
use Tvoydenvnik\Posts\Entity\EntityPost;

interface IPostsDBService {

    public function addPost(EntityPost $oEntityPost);

    /**
     * @param EntityPost $oEntityPost
     * @param $bIsAdmin
     * @return AppAnswer
     */
    public function updatePost(EntityPost $oEntityPost, $bIsAdmin);

    public function incrementPostComments(EntityPost $oEntityPost);

    public function markDeleted($nPostId, $nAuthorId, $bIsAdmin);

    public function getPosts(array $arPostsId);

    public function truncate();


}