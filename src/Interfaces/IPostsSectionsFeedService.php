<?php

namespace Tvoydenvnik\Posts\Interfaces;

use Tvoydenvnik\Posts\Entity\EntityPost;

interface IPostsSectionsFeedService {


    public function addPost(EntityPost $entityPost);

    public function deletePost($nPostId);

    public function updatePost(EntityPost $entityPost);

    public function getFeedPerSection($nSection, $nAuthorId, $nNumberOfBasket, $nBasketSize);

    public function getCountOfPostsInFeedPerSection($nSection, $nAuthorId);

    public function truncate();
}