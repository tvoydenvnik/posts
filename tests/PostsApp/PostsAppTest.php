<?php

namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Apps\PostsApp;

use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsCacheTarantool;
use Tvoydenvnik\Posts\Models\Posts\PostsMySqlRepository;
use Tvoydenvnik\Posts\Models\PostsCommonFeedService;
use Tvoydenvnik\Posts\Models\PostsSectionsFeedService;


class PostsAppTest extends \PHPUnit_Framework_TestCase{



    public function testForTest(){
        $this->assertEquals(true, true);
        $app = $this->_getPostsApp(true);
    }


    private static $_postId1 = 0;

    public function testAddPost(){
        
        $app = $this->_getPostsApp();
        
        $appAnswer = $app->addPost(
            1,
            PostTypes::$cPOST_TYPE_FOOD_DIARY,
            "Заговловок",
            array("Сообщение"),
            array(12,45,12),
            array("userId"=>1),
            array("/fff/1.gif"),
            100,
            array("params"=>true)
        );

        $this->assertEquals(false, $appAnswer->error);
        //$this->assertGreaterThan(2000000, $appAnswer->getResult()["id"]);
        self::$_postId1 = $appAnswer->getResult()["id"];
        
        
        //повторно не даст добавить, т.к. такой ключ уже есть
        $appAnswer = $app->addPost(
            1,
            PostTypes::$cPOST_TYPE_FOOD_DIARY,
            "Заговловок",
            array("Сообщение"),
            array(12,45,12),
            array("userId"=>1),
            array("/fff/1.gif"),
            100,
            array("params"=>true)
        );

        $this->assertEquals(true, $appAnswer->error);

    }

    public function testGetEntityPost(){


        $app = $this->_getPostsApp();

        $postFromDB = $app->_postsDBService->getPosts([self::$_postId1]);
        $this->assertEquals(1, count($postFromDB), "Сообщение присутствует в БД");

        $postFromCache = $app->_postsCacheService->getPostsFromCache([self::$_postId1]);
        $this->assertEquals(1, count($postFromCache["posts"]), "Сообщение присутствует в кэше");

        $this->assertNotEquals(null, $postFromDB[self::$_postId1]);
        $this->assertEquals($postFromDB[self::$_postId1]->getId(), $postFromCache["posts"][self::$_postId1]->getId());
        $this->assertEquals($postFromDB[self::$_postId1]->getSections(), $postFromCache["posts"][self::$_postId1]->getSections());
        $this->assertEquals($postFromDB[self::$_postId1]->getAuthorId(), $postFromCache["posts"][self::$_postId1]->getAuthorId());
        $this->assertEquals($postFromDB[self::$_postId1]->getAttachedImages(), $postFromCache["posts"][self::$_postId1]->getAttachedImages());
        $this->assertEquals($postFromDB[self::$_postId1]->getMessage(), $postFromCache["posts"][self::$_postId1]->getMessage());
        $this->assertEquals($postFromDB[self::$_postId1]->getParams(), $postFromCache["posts"][self::$_postId1]->getParams());
        $this->assertEquals($postFromDB[self::$_postId1]->getExternal(), $postFromCache["posts"][self::$_postId1]->getExternal());
        $this->assertEquals($postFromDB[self::$_postId1]->getPostTypeId(), $postFromCache["posts"][self::$_postId1]->getPostTypeId());
        $this->assertEquals($postFromDB[self::$_postId1]->getTitle(), $postFromCache["posts"][self::$_postId1]->getTitle());


        $this->assertEquals(self::$_postId1, $postFromCache["posts"][self::$_postId1]->getId());
        $this->assertEquals(array(12,45), $postFromCache["posts"][self::$_postId1]->getSections());
        $this->assertEquals(1, $postFromCache["posts"][self::$_postId1]->getAuthorId());
        $this->assertEquals( array("/fff/1.gif"), $postFromCache["posts"][self::$_postId1]->getAttachedImages());
        $this->assertEquals(array("Сообщение"), $postFromCache["posts"][self::$_postId1]->getMessage());
        $this->assertEquals(array("params"=>true), $postFromCache["posts"][self::$_postId1]->getParams());
        $this->assertEquals(array("userId"=>1), $postFromCache["posts"][self::$_postId1]->getExternal());
        $this->assertEquals(PostTypes::$cPOST_TYPE_FOOD_DIARY, $postFromCache["posts"][self::$_postId1]->getPostTypeId());
        $this->assertEquals("Заговловок", $postFromCache["posts"][self::$_postId1]->getTitle());


    }

    public function testUpdatePost(){
        $app = $this->_getPostsApp();

        $result = $app->updatePost(5689,1,"","");
        $this->assertNotEquals(true, $result, "Сообщение не существует.");



        $result = $app->updatePost(self::$_postId1,122221,"","4");
        $this->assertNotEquals(true, $result, "Нарушение прав.");


        $result = $app->updatePost(self::$_postId1,122221,"","4", null, null, null, true);
        $this->assertEquals(true, $result, "Админу можно сохранять.");


        $result = $app->updatePost(self::$_postId1,1,"Заговловок Новый",array("Сообщение Новое"), array(56, 12), array("/fff/2.gif"), null, true);
        $this->assertEquals(true, $result, "Автору можно сохранять.");

        $result = $app->updatePost(self::$_postId1,1,"Заговловок Новый",array("Сообщение Новое"), array(56, 12), array("/fff/2.gif"), array("params2"=>1), true);
        $this->assertEquals(true, $result, "Автору можно сохранять.");


        //$result = $app->updatePost(self::$_postId1,1,"Заговловок","Сообщение", array(12,45,12), array("/fff/1.gif"), array("params2"=>1), true);

    }

    public function testGetEntityPostAfterUpdate(){


        $app = $this->_getPostsApp();

        $postFromDB = $app->_postsDBService->getPosts([self::$_postId1]);
        $this->assertEquals(1, count($postFromDB), "Сообщение присутствует в БД");

        $postFromCache = $app->_postsCacheService->getPostsFromCache([self::$_postId1]);
        $this->assertEquals(1, count($postFromCache["posts"]), "Сообщение присутствует в кэше");


        /**
         * @var $postFromDB EntityPost
         * @var $postFromCache EntityPost
         */
        $postFromDB = $postFromDB[self::$_postId1];
        $postFromCache = $postFromCache["posts"][self::$_postId1];
        $this->assertEquals(self::$_postId1, $postFromCache->getId());
        $this->assertEquals(array(56,12), $postFromCache->getSections());
        $this->assertEquals(1, $postFromCache->getAuthorId());
        $this->assertEquals( array("/fff/2.gif"), $postFromCache->getAttachedImages());
        $this->assertEquals(array("Сообщение Новое"), $postFromCache->getMessage());
        $this->assertEquals(array("params"=>true, 'params2' => 1), $postFromCache->getParams());
        $this->assertEquals(array("userId"=>1), $postFromCache->getExternal());
        $this->assertEquals(PostTypes::$cPOST_TYPE_FOOD_DIARY, $postFromCache->getPostTypeId());
        $this->assertEquals("Заговловок Новый", $postFromCache->getTitle());

        $this->assertNotEquals(null, $postFromDB);
        $this->assertEquals($postFromDB->getId(), $postFromCache->getId());
        $this->assertEquals($postFromDB->getSections(), $postFromCache->getSections());
        $this->assertEquals($postFromDB->getAuthorId(), $postFromCache->getAuthorId());
        $this->assertEquals($postFromDB->getAttachedImages(), $postFromCache->getAttachedImages());
        $this->assertEquals($postFromDB->getMessage(), $postFromCache->getMessage());
        $this->assertEquals($postFromDB->getParams(), $postFromCache->getParams());
        $this->assertEquals($postFromDB->getExternal(), $postFromCache->getExternal());
        $this->assertEquals($postFromDB->getPostTypeId(), $postFromCache->getPostTypeId());
        

    }



    public function te__stAddDeleteUpdatePosts(){

        $app = $this->_getPostsApp(true);
        $arListOfPosts = array();

        $nUserCount = 10;



        $timestamp = time();

        for($nUserId=1; $nUserId<=$nUserCount; $nUserId++){

            //кол-во сообщений у пользователя
            $userPosts = rand(20, 100);

            
            $nPostTypeId = 1;
            for($nUserPost = 1; $nUserPost<=$userPosts; $nUserPost++){


                $arSections = (array(rand(85, 100), rand(80, 100), rand(80, 100)));

                $timestamp = $timestamp + 1;
                $lAddResult = $app->addPost($nUserId, $nPostTypeId, 'title ' . $nUserPost, 'text ' . $nUserPost, $arSections,  array('mentions'), array("createdAtTimestamp"=>$timestamp/*microtime(true)*10000*/));

                array_unshift($arListOfPosts, array(
                    "id"=> $lAddResult['id'],
                    "author"=>$nUserId,
                    "postType"=>$nPostTypeId,
                    "sections"=>$arSections,
                    "title"=>'title ' . $nUserPost,
                    "message"=> 'text ' . $nUserPost
                ));




                //сбросим тип сообщения
                $nPostTypeId++;
                if($nPostTypeId===5){
                    $nPostTypeId = 1;
                }

                usleep(1);

            }

        }

        ///////////////////////////////////////////
        //сделаем модификации deletePost and updatePost
        ///////////////////////////////////////////


        // удалим все сообщения пользователя с id=1
        $arUserPostsForDelete = $this->_filter($arListOfPosts, array("author"=>1));
        foreach($arUserPostsForDelete as $key=>$value){
            $app->deletePost($value["id"], $value["author"]);
        }

        $arListOfPosts = array_filter($arListOfPosts, function($val){
            return !($val["author"] === 1);
        });

        //для второго пользователя обновим все сообщения
        foreach($arListOfPosts as $key=>$value){
            if( $value["author"] === 2){

                $arListOfPosts[$key]["sections"] = (array(rand(85, 100), rand(80, 100), rand(80, 100), rand(1, 50)));
                $arListOfPosts[$key]["title"] = $value["title"] . ' - update';
                $arListOfPosts[$key]["message"] = $value["message"] . ' - update';
                $app->updatePost($value["id"],$value["author"], $arListOfPosts[$key]["title"], $arListOfPosts[$key]["message"], $arListOfPosts[$key]["sections"]);
            }
        }


        return;

        //usleep(1000);

        ///////////////////////////////////////////
        //Тестируем блог пользователя
        //////////////////////////////////////////
        for($nAuthorId=1; $nAuthorId<=$nUserCount; $nAuthorId++){


            $lCountOfPosts = $app->_postsCommonFeedService->getCountOfPostsInFeed($nAuthorId);

            $arUserPosts = $this->_filter($arListOfPosts, array("author"=>$nAuthorId));
            //$arUserPostsTypes = $this->_getPostTypes($arUserPosts);
            $arUserPostsIds = $this->_getIds($arUserPosts);

            /*
             * Общее количество сообщений в блоге
             */
            $this->assertEquals(count($arUserPosts), $lCountOfPosts, 'Общее кол-во сообщений в блоге пользователя');

//            /*
//             * Общее количество сообщений: фильтр postType
//             */
//            array_walk($arUserPostsTypes, function($postType) use ($app, $nAuthorId, $arUserPosts){
//
//                $lCountOfPostsByType = $app->_postsCommonFeedService->getCountOfPostsInFeed($nAuthorId);
//                $this->assertEquals(count($this->_filter($arUserPosts, array())) , $lCountOfPostsByType, 'Общее кол-во сообщений в блоге пользователя: фильтр по типу соообщения');
//
//            });

            /*
             * Сформируем корзины для блога пользователя и проверим их
             */

            $arBaskets = $this->_makeBasket($arUserPostsIds);
            array_walk($arBaskets, function($arIdsOfBasket, $indexOfBasket) use ($app, $nAuthorId){

                $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, $indexOfBasket+1, 10);
                $this->assertEquals($arBasketCheck["postIds"], $arIdsOfBasket, 'Проверим корзины для блога пользователя');

            });

            /*
             * Получим последнюю корзину
             */
            $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, null, 10);
            $this->assertEquals($arBasketCheck["postIds"], $arBaskets[count($arBaskets)-1], 'Проверим последнюю корзину для блога пользователя');

//            /*
//             * Сформируем корзины для блога пользователя с учетом типа сообщения и проверим их
//             */
//            array_walk($arUserPostsTypes, function($postType) use ($app, $nAuthorId, $arUserPosts){
//
//                $arBaskets = $this->_makeBasket($this->_getIds($this->_filter($arUserPosts, array("postType"=>$postType))));
//
//                foreach($arBaskets as $indexBasket=>$idsBasket){
//                    $arBasketCheck  = $app->_postsCommonFeedService->getFeed($nAuthorId, $postType, $indexBasket+1, 10);
//                    $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Корзины по типу сообщения: фильтр по типу сообщения');
//                }
//
//            });

            ///////////////////////////////////////////
            //Тестируем по разделам: По блогу автора
            //////////////////////////////////////////

            $arSections = $this->_getSections($arUserPosts);
            foreach($arSections as $i=>$section){

                $postInSection = $this->_filter($arUserPosts, array("section"=>$section));
                $lCount = $app->_postsSectionsFeedService->getCountOfPostsInFeedPerSection($section, $nAuthorId);

                $this->assertEquals(count($postInSection), $lCount, 'Блог пользователя: Количество: Фильтр по разделам');

                $arBaskets = $this->_makeBasket($this->_getIds($postInSection));

                foreach($arBaskets as $indexBasket=>$idsBasket){
                    $arBasketCheck  = $app->_postsSectionsFeedService->getFeedPerSection($section, $nAuthorId, $indexBasket+1, 10);
                    $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Блог пользователя: Корзины по типу сообщения: фильтр по разделу');
                }

            }

        }


        ///////////////////////////////////////////
        //Тестируем общую ленту
        //////////////////////////////////////////


        /*
         * Общее количество сообщений
         */
        $lCountOfPosts = $app->_postsCommonFeedService->getCountOfPostsInFeed(null);
        $this->assertEquals(count($arListOfPosts), $lCountOfPosts, 'Общее кол-во сообщений в общей ленте');


        /*
        * Сформируем корзины и проверим по общей ленте
        */
        $arBaskets = $this->_makeBasket($this->_getIds($arListOfPosts));
        array_walk($arBaskets, function($arIdsOfBasket, $indexOfBasket) use ($app){

            $arBasketCheck  = $app->_postsCommonFeedService->getFeed(null, $indexOfBasket+1, 10);
            $this->assertEquals($arBasketCheck["postIds"], $arIdsOfBasket, 'Общая лента: Проверим корзины');

        });


//        /*
//         * Сформируем корзины и проверим по общей ленте: фильтр по типу сообщения
//         */
//        $arPostsTypes = $this->_getPostTypes($arListOfPosts);
//        array_walk($arUserPostsTypes, function($postType) use ($app, $arListOfPosts){
//
//            $arBaskets = $this->_makeBasket($this->_getIds($this->_filter($arListOfPosts, array("postType"=>$postType))));
//
//            foreach($arBaskets as $indexBasket=>$idsBasket){
//                $arBasketCheck  = $app->_postsCommonFeedService->getFeed(null, $postType, $indexBasket+1, 10);
//                $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Общая лента: Корзины по типу сообщения: фильтр по типу сообщения');
//            }
//
//        });

        ///////////////////////////////////////////
        //Тестируем по разделам: Общая лента
        //////////////////////////////////////////

        $arSections = $this->_getSections($arListOfPosts);
        foreach($arSections as $i=>$section){

            $postInSection = $this->_filter($arListOfPosts, array("section"=>$section));
            $lCount = $app->_postsSectionsFeedService->getCountOfPostsInFeedPerSection($section, null);

            $this->assertEquals(count($postInSection), $lCount, 'Общая лента: Количество: Фильтр по разделам');


            $arBaskets = $this->_makeBasket($this->_getIds($postInSection));

            foreach($arBaskets as $indexBasket=>$idsBasket){
                $arBasketCheck  = $app->_postsSectionsFeedService->getFeedPerSection($section, null, $indexBasket+1, 10);
                $this->assertEquals($arBasketCheck["postIds"], $idsBasket, 'Общая лента: Корзины по типу сообщения: фильтр по разделу');
            }

        }



        $this->assertEquals(false,  $app->_postsSectionsFeedService->getFeedPerSection(12423232, null, null, 10));
        $this->assertEquals(false,  $app->_postsCommonFeedService->getFeed(12423232, null, 10));


        /*=================================================================
         * Проверим getPosts из кэша и из БД
         *=================================================================*/

        $arResultFromCache = $app->_postsCacheService->getPostsFromCache($this->_getIds($arListOfPosts));

        $arResultFromDB = $app->_postsDBService->getPosts($this->_getIds($arListOfPosts));


        $this->assertEquals(count($arResultFromCache['posts']), count($arListOfPosts), "1");
        $this->assertEquals(count($arResultFromDB), count($arListOfPosts),"2");

        $this->assertEquals($arResultFromCache['posts'], $arResultFromDB);




        /*=================================================================
        * ПОЛУЧИМ БЛОГ АВТОРА todo tests
        *=================================================================*/
        $nAuthor1= $arListOfPosts[0]["author"];

        $app->_postsCacheService->truncate();
        $userBlogFirstPage = $app->getAuthorPosts($nAuthor1);
        $userBlogFirstPage2 = $app->getAuthorPosts($nAuthor1, 1);


//        $app->updatePost(122222222222,111, 'ss', 'sss');
//        $commonPosts = $app->getCommonPosts();


        $test = 1;

    }

//    private function _getPostTypes($arPosts){
//        $arResult = array();
//
//        foreach($arPosts as $key=>$value){
//            array_push($arResult, $value["postType"]);
//        }
//
//        return array_unique($arResult);
//    }

    private function _getSections($arPosts){
        $arResult = array();

        foreach($arPosts as $key=>$value){

            foreach($value["sections"] as $i=>$val){
                array_push($arResult, $val);
            }

        }

        return array_unique($arResult);
    }


    private function _filter($arPosts, $arFilter){

        $arResult = array();

        foreach($arPosts as $key=>$value){

            $bFilter = null;
            if(isset($arFilter["author"])){
                $bFilter = ($arFilter["author"] === $value["author"]);
            }

            if(($bFilter === true || $bFilter === null) && isset($arFilter["postType"])){
                $bFilter = ($arFilter["postType"] === $value["postType"]);
            }

            if(($bFilter === true || $bFilter === null) && isset($arFilter["section"])){

                foreach($value["sections"] as $i=>$sect){
                    if($sect === $arFilter["section"]){
                        $bFilter = true;
                        break;
                    }
                }

            }

            if($bFilter === true){
                array_push($arResult, $value);
            }
        }

        return $arResult;

    }

    private function _getIds($arPosts){
        $arResult = array();

        foreach($arPosts as $key=>$value){
            array_push($arResult, $value["id"]);
        }

        return $arResult;
    }

    private function _makeBasket($arIds){

        $arBasket = array();

        foreach($arIds as $key=>$value){

            //сформируем корзины
            if(count($arBasket)===0){
                array_push($arBasket, array($value));
            }else{
                $lCountAll =count($arBasket);
                $lFirst = $arBasket[0];
                if(count($lFirst)===10){
                    array_unshift($arBasket, array($value));
                }else{
                    array_push($arBasket[0], $value);
                }

            }


        }

        return $arBasket;

    }

    /**
     * @param $truncate
     * @return PostsApp
     */
    private function _getPostsApp($truncate = false){

        /**
         * @var $app PostsApp
         */
        $app = FactoryDefault::getDefault()->get('getPostsApp');

        if($truncate === true){
            $app->truncate();
        }

        return $app;
        
    }

}
