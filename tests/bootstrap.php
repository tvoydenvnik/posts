<?php


require_once "/home/bitrix/www/__php-unit/posts/vendor/autoload.php";


$di = new Phalcon\Di\FactoryDefault();

$di->setDefault($di);

$di->set('db', function () {

    $eventsManager = new Phalcon\Events\Manager();


    unlink("debug.log");
    $logger = new Phalcon\Logger\Adapter\File("debug.log");

    // Listen all the database events
    $eventsManager->attach('db', function ($event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $logger->log($connection->getSQLStatement(), Phalcon\Logger::INFO);
        }
    });

    $connection =  new Phalcon\Db\Adapter\Pdo\Mysql(
        array(
            "host"     => "192.168.81.134",
            "username" => "remote2342",
            "password" => "remote234",
            "dbname"   => "test"
        )
    );

    // Assign the eventsManager to the db adapter instance
    $connection->setEventsManager($eventsManager);

    return $connection;
}, true);


$di->set('dbBitrix', function () {
    return new Phalcon\Db\Adapter\Pdo\Mysql(
        array(
            "host"     => "192.168.81.134",
            "username" => "remote2342",
            "password" => "remote234",
            "dbname"   => "bitrix"
        )
    );
}, true);


$di->set('tarantool', function(){

    $_connection = new \Tarantool('localhost', 3301);//95.213.251.7
    $_connection->authenticate('app', 'secret');

    return $_connection;
}, true);


$di->set('memcache', function(){

    $l_MC =  new \Memcache();

    if($l_MC->connect('192.168.81.134', '11211')  === false){
        return false;
    }else{
        return $l_MC;
    }
}, true);


$di->set('getPostsApp', function () use ($di){
    $app = new \Tvoydenvnik\Posts\Apps\PostsApp();

    //new PostsMySqlRepository()
    $app->setPostsDBService(new \Tvoydenvnik\Posts\Models\Posts\PostsMySqlRepository());

    $feed = new \Tvoydenvnik\Posts\Models\Posts\PostsCommonFeed();
    $feed->setConnection($di->get('tarantool'));
    $app->setPostsCommonFeed($feed);

    $feed = \Tvoydenvnik\Posts\Models\Posts\PostsCommonFeed::getFeedForHDru();
    $feed->setConnection($di->get('tarantool'));
    $app->setPostsCommonFeedHD($feed);

    $feed = \Tvoydenvnik\Posts\Models\Posts\PostsCommonFeed::getFeedForTvoydnevnik();
    $feed->setConnection($di->get('tarantool'));
    $app->setPostsCommonFeedTvoydnevnik($feed);


    $feed = new \Tvoydenvnik\Posts\Models\Posts\PostsAuthorFeed();
    $feed->setConnection($di->get('tarantool'));
    $app->setPostsAuthorFeed($feed);

    $feedSections = new \Tvoydenvnik\Posts\Models\Posts\PostsSectionsFeedService();
    $feedSections->setConnection($di->get('tarantool'));
    $app->setPostsSectionsFeedService($feedSections);


    $cache = new \Tvoydenvnik\Posts\Models\Posts\PostsCacheTarantool();
    $cache->setConnection($di->get('tarantool'));
    $app->setPostsCacheService($cache);


    $parentIndex = new \Tvoydenvnik\Posts\Models\Posts\PostsParentsIndexTarantool();
    $parentIndex->setConnection($di->get('tarantool'));
    $app->setPostsParentsIndexTarantool($parentIndex);

    
    return $app;
    
}, true);










