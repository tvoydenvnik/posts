function PostsCommonFeedServiceGetPostsByDateForAuthor(nAuthorId)

   local result = {};


   for _, tuple in
   box.space.posts_common.index.author:pairs({nAuthorId}, {
       iterator = box.index.EQ}) do

       if result[tuple[3]] == nill then
           result[tuple[3]] = {
               count = 0,
               list = {}
           };
       end

       result[tuple[3]]["count"] = result[tuple[3]]["count"] + 1;
       table.insert(result[tuple[3]]["list"], tuple)

   end

   return result;
end

