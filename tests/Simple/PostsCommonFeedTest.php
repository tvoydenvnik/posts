<?php

namespace Tvoydenvnik\Posts\Tests\Simple;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsCommonFeed;
use Tvoydenvnik\Posts\Utils\Common;

class PostsCommonFeedTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PostsCommonFeed
     */
    private $feed = null;

    public function setUp()
    {

        $cache = new PostsCommonFeed();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->feed =  $cache;

    }

    public function tearDown()
    {

        $this->feed = null;
    }


    public static function tearDownAfterClass(){
        $cache = new PostsCommonFeed();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $cache->truncate();
    }


    public function testTruncate(){
        $this->feed->truncate();
        $this->assertEquals($this->feed->spaceLength(), 0);
    }


    public function testGetOffsetLua(){
        $str = Common::getOffsetLua();

        $str = $str . "\n" . "return __getOffset(...)";
        $result = $this->feed->_connection->evaluate($str, array(1, 100, 10))[0];
        $this->assertEquals(array(
            "numberOfBasket"=>1,
            "offset"=>90,
            "countOfBasket"=>10,
            "limit"=>10,
        ), $result);
    }


    public function testAddPost(){

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->assertEquals($this->feed->spaceLength(), 1);

        $this->feed->addPost(EntityPost::create(array("id"=>2, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->assertEquals($this->feed->spaceLength(), 2);


        $this->feed->addPost(EntityPost::create(array("id"=>3, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>4, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>333334, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>5, "author_id"=>12, 'created_at'=>'2010-01-01 12:12:10')));

        $this->assertEquals($this->feed->getCountOfPostsInFeed(), 6);

    }

    public function testDeletePost(){


        $this->feed->truncate();

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->deletePost(1);

        $this->assertEquals($this->feed->spaceLength(), 0);

    }


    public function testGetFeed(){

        $this->feed->truncate();
        $date = new \DateTime("2012-07-08 01:01:01");

        for($i=1;$i<=1000; $i++){


            if($i<=100){
                $author_id = 1;
            }elseif($i<=500){
                $author_id = 2;
            }else{
                $author_id = 300;
            }

            $date = $date->add(\DateInterval::createFromDateString('1 seconds'));
            $createdAt = $date->format("Y-m-d H:i:s");

            $this->feed->addPost(EntityPost::create(array("id"=>$i, "author_id"=>$author_id, 'created_at'=>$createdAt)));

        }

        $this->assertEquals(1000, $this->feed->getCountOfPostsInFeed());



        /*
         * ОБЩАЯ ЛЕНТА
         */


        $arResultCommon = $this->feed->getFeed(null, 10,true);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            "limit"=>10
        ),$arResultCommon );


        $arResultCommon = $this->feed->getFeed(100);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            "limit"=>10,
            'offset' => 0
        ),$arResultCommon );


        /*
         * Такой корзны нет, то выдаем последнюю
         */
        $arResultCommon = $this->feed->getFeed(1000);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            "limit"=>10
        ),$arResultCommon );

        $arResultCommon = $this->feed->getFeed(1);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                10, 9, 8, 7, 6, 5 , 4, 3, 2, 1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10,
            "limit"=>10,
            'offset' => 990
        ),$arResultCommon );


        $arResultCommon = $this->feed->getFeed(99);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                990, 989, 988, 987, 986, 985 , 984, 983, 982, 981
            ),
            "numberOfBasket"=>99,
            "basketSize"=>10,
            'offset' => 10,
            'limit' => 10
        ),$arResultCommon );




        $arResultCommon = $this->feed->getFeed(null, 3);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>334,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998
            ),
            "numberOfBasket"=>334,
            "basketSize"=>3,
            'limit' => 3
        ),$arResultCommon );
        

    }


    public function testFeedPerformance(){

        $this->feed->truncate();
        $space = $this->feed->space_name;
        $count = 100000;

        $eval = <<<EOT
                  
           for i=1,$count,1 do
                box.space.$space:insert({i, i});            
           end

EOT;
        $this->assertTime(5, function() use ($eval, $count){

            $this->feed->_connection->evaluate($eval);
            $this->assertEquals($count, $this->feed->spaceLength());

        });

        $this->assertTime(1, function() use ($eval, $count){
            
            for($i=1;$i<300;$i++){
                $arResultCommon = $this->feed->getFeed($i);
            }

        });








    }

    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }


}
