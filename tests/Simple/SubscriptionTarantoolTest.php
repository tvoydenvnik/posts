<?php

namespace Tvoydenvnik\Posts\Tests;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Models\Subscription\SubscriptionTarantool;

class SubscriptionTarantoolTest extends \PHPUnit_Framework_TestCase{

    /**
     * @var SubscriptionTarantool
     */
    private $subscription = null;
    public function setUp()
    {

        $cache = new SubscriptionTarantool();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->subscription =  $cache;

    }

    public function tearDown()
    {
        $this->subscription = null;
    }


    public function test1Test(){
        $this->subscription->truncate();
        $this->assertEquals(true, true);
    }


    public function testSubscribeOn(){



        $this->assertEquals($this->subscription->isSubscribeOn(1,2), false);

        $this->subscription->subscribeOn(1, 2);
        $this->assertEquals($this->subscription->isSubscribeOn(1,2), true);
        $this->assertEquals($this->subscription->isSubscribeOn(2,1), false);

        $this->assertEquals(0, $this->subscription->getCountSubscribersOnWhom(1));
        $this->assertEquals(1, $this->subscription->getCountSubscribersOnWhom(2));
        $this->assertEquals(1, $this->subscription->getCountSubscribesWho(1));
        $this->assertEquals(0, $this->subscription->getCountSubscribesWho(2));




        $this->subscription->subscribeOn(2, 1);
        $this->assertEquals(1, $this->subscription->getCountSubscribersOnWhom(1));
        $this->assertEquals(1, $this->subscription->getCountSubscribersOnWhom(2));

        $this->assertEquals($this->subscription->isSubscribeOn(1,2), true);
        $this->assertEquals($this->subscription->isSubscribeOn(2,1), true);

        $this->subscription->subscribeOn(1, 3);
        $this->subscription->subscribeOn(1, 4);

        $this->assertEquals($this->subscription->isSubscribeOn(1,3), true);
        $this->assertEquals($this->subscription->isSubscribeOn(1,4), true);
        $this->assertEquals($this->subscription->isSubscribeOn(1,5), false);


        $this->subscription->subscribeOn(2, 3);
        $this->subscription->subscribeOn(2, 3);//повтор
        $this->subscription->subscribeOn(4, 3);
        $this->subscription->subscribeOn(5, 3);

        $this->assertEquals(4, $this->subscription->getCountSubscribersOnWhom(3));
        $this->assertEquals(3, $this->subscription->getCountSubscribesWho(1));

        $this->assertEquals($this->subscription->isSubscribeOn(2,3), true);
        $this->assertEquals($this->subscription->isSubscribeOn(4,3), true);
        $this->assertEquals($this->subscription->isSubscribeOn(5,3), true);

        //$this->assertEquals($this->subscription->getCountSubscribersForOnWhom(3), array(1,2,4,5));


    }

    public function testWhoSubscribedOn(){
        $this->assertEquals(
            array(
                'countOfBasket' => 1,
            'countOfUsers' => 4,
            'userIds' => Array (5,4,2,1),
            'numberOfBasket' => 1,
            'basketSize' => 10
            ),$this->subscription->whoSubscribedOn(3));
    }

    public function testOnWhomSubscribed(){
        $this->assertEquals(
            array(
                'countOfBasket' => 1,
                'countOfUsers' => 3,
                'userIds' => Array (4,3,2),
                'numberOfBasket' => 1,
                'basketSize' => 10
            ),$this->subscription->onWhomSubscribed(1));
    }


    public function testSubscribeOff(){


        $this->subscription->subscribeOff(2,3);
        $this->subscription->subscribeOff(4,3);
        $this->subscription->subscribeOff(444,3);
        $this->assertEquals($this->subscription->isSubscribeOn(2,3), false);
        $this->assertEquals($this->subscription->isSubscribeOn(4,3), false);


        $this->subscription->subscribeOff(1,4);
        $this->assertEquals(2, $this->subscription->getCountSubscribesWho(1));
        //$this->assertEquals($this->subscription->getCountSubscribersForOnWhom(3), array(1,5));
    }
}