<?php

namespace Tvoydenvnik\Posts\Tests;

use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsSectionsFeedService;


class PostsSectionsFeedServiceTest extends \PHPUnit_Framework_TestCase{


    /**
     * @var PostsSectionsFeedService
     */
    private $feed = null;
    private $temp = null;
    public function setUp()
    {

        $cache = new PostsSectionsFeedService();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->feed =  $cache;

    }

    public function tearDown()
    {
        $this->feed = null;
    }


    public function testTruncate(){
        $this->feed->truncate();
        $this->assertEquals($this->feed->spaceLength(), 0);
    }

    public function testAddPost(){
        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10', "sections"=>array(10,11))), true);
        $this->feed->addPost(EntityPost::create(array("id"=>2, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10', "sections"=>array(10,11))), true);
        $this->feed->addPost(EntityPost::create(array("id"=>3, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10', "sections"=>array(50,101,10))), true);
        $this->feed->addPost(EntityPost::create(array("id"=>4, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10', "sections"=>array(10,99,100))), true);
        $this->feed->addPost(EntityPost::create(array("id"=>5, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10', "sections"=>array(150, 10,11))), true);

        $this->assertEquals(13, $this->feed->spaceLength());
        $this->assertEquals(5, $this->feed->getCountOfPostsInFeedPerSection(10));
        $this->assertEquals(3, $this->feed->getCountOfPostsInFeedPerSection(11));

        $this->assertEquals($this->feed->getCountOfPostsInFeedPerSection(1050), 0);


        $this->assertEquals(3, $this->feed->getCountOfPostsInFeedPerSection(10, 1));
        $this->assertEquals(5, $this->feed->getCountOfPostsInFeedPerSection(10));

        $this->assertEquals(2, $this->feed->getCountOfPostsInFeedPerSection(10, 2));
        $this->assertEquals(1, $this->feed->getCountOfPostsInFeedPerSection(150, 2));


    }

    public function testDeletePost(){
        $this->feed->deletePost(1);
        $this->assertEquals(4, $this->feed->getCountOfPostsInFeedPerSection(10));
        $this->assertEquals(11, $this->feed->spaceLength());

    }


    public function testGetFeed()
    {


        $date = new \DateTime("2012-07-08 01:01:01");
        $this->feed->truncate();

        for($i=1;$i<=1000; $i++){


            if($i<=100){
                $author_id = 1;
            }elseif($i<=500){
                $author_id = 2;
            }else{
                $author_id = 300;
            }

            $date = $date->add(\DateInterval::createFromDateString('1 seconds'));
            $createdAt = $date->format("Y-m-d H:i:s");//date("Y-m-d G:H:s", $i+500000);

            $this->feed->addPost(EntityPost::create(array("id"=>$i, "author_id"=>$author_id, 'created_at'=>$createdAt, "sections"=>array(10,11))), true);

        }


        $this->assertEquals(1000, $this->feed->getCountOfPostsInFeedPerSection(10));
        $this->assertEquals(1000, $this->feed->getCountOfPostsInFeedPerSection(11));
        $this->assertEquals(0, $this->feed->getCountOfPostsInFeedPerSection(113232));


        /*
        * ОБЩАЯ ЛЕНТА
        */


        $arResultCommon = $this->feed->getFeedPerSection(10);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            'limit' => 10
        ),$arResultCommon );

        $arResultCommon = $this->feed->getFeedPerSection(10, null, 100);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            'offset' => 0,
            'limit' => 10
        ),$arResultCommon );


        $arResultCommon = $this->feed->getFeedPerSection(10, null, 1);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                10, 9, 8, 7, 6, 5 , 4, 3, 2, 1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10,
            'offset' => 990,
    'limit' => 10
        ),$arResultCommon );

        $arResultCommon = $this->feed->getFeedPerSection(11);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>100,
            "countOfPosts"=>1000,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>100,
            "basketSize"=>10,
            'limit' => 10
        ),$arResultCommon );



        $arResultCommon = $this->feed->getFeedPerSection(11, 1);
        unset($arResultCommon["posts"]);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                100, 99, 98, 97, 96, 95 , 94, 93, 92, 91
            ),
            "numberOfBasket"=>10,
            "basketSize"=>10,
            'limit' => 10
        ),$arResultCommon );

    }



    public function testFeedPerformance(){

        $this->feed->truncate();
        $space = PostsSectionsFeedService::$cSPACE_NAME_SECTIONS;
        $space_count = PostsSectionsFeedService::$cSPACE_NAME_SECTIONS_COUNTS;
        $count = 1000;




        $eval = <<<EOT


           local userIds = {}; 
           local sections = {}; 
           
           for i=1,$count,1 do
           
                local userId = math.abs(math.ceil(math.cos(i)*1000));
                local section = 2;
                local section2 = 3;
                table.insert(userIds, userId);
                
                if sections[section] == null then
                    sections[section] = 0;
                end
                if sections[section2] == null then
                    sections[section2] = 0;
                end
                
                sections[section] = sections[section] + 1;
                sections[section2] = sections[section2] + 1;

                box.space.$space:insert({i, userId, section, i});
                box.space.$space:insert({i, userId, section2, i});
                
                box.space.$space_count:inc{userId .. '-'..section};
                box.space.$space_count:inc{userId .. '-'..section2};
                
                box.space.$space_count:inc{'all' .. '-'..section};
                box.space.$space_count:inc{'all' .. '-'..section2};
                
           end
           
           return {
                sections= sections,
                userIds = userIds
            };

EOT;


        $arResult = array();
        $this->assertTime(6, function() use ($eval, $count, $arResult){

            $this->temp = $this->feed->_connection->evaluate($eval)[0];
            $this->assertEquals($count*2, $this->feed->spaceLength());

        });

        $this->assertTime(4, function() use ($eval, $count){
            $this->temp;
            for($i=1;$i<1000;$i++){
                $arResultCommon = $this->feed->getFeedPerSection(2, $this->temp["userIds"][$i] );
                $this->assertGreaterThan(0, count($arResultCommon["postIds"]));

                $arResultCommon = $this->feed->getFeedPerSection(3, null, $this->temp["userIds"][$i]);
                $this->assertEquals(10, count($arResultCommon["postIds"]));
            }

        });








    }

    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }


}
