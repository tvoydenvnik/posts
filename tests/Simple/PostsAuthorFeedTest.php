<?php

namespace Tvoydenvnik\Posts\Tests;

use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsAuthorFeed;


class PostsAuthorFeedTest  extends \PHPUnit_Framework_TestCase{



    /**
     * @var PostsAuthorFeed
     */
    private $feed = null;


//    public static function setUpBeforeClass(){
//        $cache = new PostsAuthorFeed();
//        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));
//        $cache->drop();
//    }


    public function setUp()
    {

        $cache = new PostsAuthorFeed();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->feed =  $cache;

    }

    public function tearDown()
    {
        $this->feed = null;
    }

    public static function tearDownAfterClass(){
        $cache = new PostsAuthorFeed();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));
        $cache->truncate();
    }


    public function testTruncate(){
        $this->feed->truncate();
        $this->assertEquals($this->feed->spaceLength(), 0);
    }

    public function testAddPost(){

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>2, "author_id"=>55555, 'created_at'=>'2010-01-01 12:12:10')), 1);
        $this->feed->addPost(EntityPost::create(array("id"=>3, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>4, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>5, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>6, "author_id"=>2, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>7, "author_id"=>12, 'created_at'=>'2010-01-01 12:12:10')));



        $this->assertEquals(3, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(3, $this->feed->getCountOfPostsInFeed(2));
        $this->assertEquals(1, $this->feed->getCountOfPostsInFeed(12));
        $this->assertEquals(0, $this->feed->getCountOfPostsInFeed(12333));



    }

    public function testDeletePost(){


        $this->feed->truncate();

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>100, 'created_at'=>'2010-01-01 12:12:10')), 1);
        $this->assertEquals($this->feed->spaceLength(), 2);
        $this->feed->deletePost(1);

        $this->assertEquals($this->feed->spaceLength(), 0);




    }


    public function testGetFeed(){

        $this->feed->truncate();
        $date = new \DateTime("2012-07-08 01:01:01");


        $ar = array();
        $ar2 = array();
        for($i=1;$i<=1000; $i++){


            if($i<=100){
                $author_id = 1;
            }elseif($i<=500){
                $author_id = 2;
            }else{
                $author_id = 300;
            }
            $date = $date->add(\DateInterval::createFromDateString('1 seconds'));

            $createdAt = $date->format("Y-m-d H:i:s");//date("Y-m-d G:H:s", $i+500000);

//            array_push($ar, $createdAt);
//            array_push($ar2,  strtotime($createdAt));
            $entityPost = EntityPost::create(array("id"=>$i, "author_id"=>$author_id, 'created_at'=>$createdAt));
            $this->feed->addPost($entityPost);
            $entityPost2 = $entityPost;

        }


        $this->assertEquals(100, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(400, $this->feed->getCountOfPostsInFeed(2));
        $this->assertEquals(500, $this->feed->getCountOfPostsInFeed(300));



        $byDate = $this->feed->getPostsByDateForAuthor(300);
        $this->assertEquals(500, $byDate["2012-07-08"]["count"]);



        /*
         * ЛЕНТА В РАЗРЕЗЕ ПОЛЬЗОВАТЕЛЕЙ
         */


        $arResultUser = $this->feed->getFeed(1);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                100, 99, 98, 97, 96, 95 , 94, 93, 92, 91
            ),
            "numberOfBasket"=>10,
            "basketSize"=>10,
            "isOwnerFilter"=>null
        ),$arResultUser );


        $arResultUser = $this->feed->getFeed(1, 10);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                100, 99, 98, 97, 96, 95 , 94, 93, 92, 91
            ),
            "numberOfBasket"=>10,
            "basketSize"=>10,
            "isOwnerFilter"=>null
        ),$arResultUser );

        $arResultUser = $this->feed->getFeed(1,1);
        $this->assertEquals(array(
            "countOfBasket"=>10,
            "countOfPosts"=>100,
            "postIds"=>array(
                10, 9, 8, 7, 6, 5 , 4, 3, 2, 1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10,
            "isOwnerFilter"=>null
        ),$arResultUser );



        $arResultUser = $this->feed->getFeed(300);
        $this->assertEquals(array(
            "countOfBasket"=>50,
            "countOfPosts"=>500,
            "postIds"=>array(
                1000, 999, 998, 997, 996, 995 , 994, 993, 992, 991
            ),
            "numberOfBasket"=>50,
            "basketSize"=>10,
            "isOwnerFilter"=>null
        ),$arResultUser );





    }

    public function testReposts(){
        $this->feed->truncate();

        $this->feed->addPost(EntityPost::create(array("id"=>1, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>2, "author_id"=>150, 'created_at'=>'2010-01-01 12:12:10')),1, true);
        $this->feed->addPost(EntityPost::create(array("id"=>3, "author_id"=>150, 'created_at'=>'2010-01-01 12:12:10')),1, true);

        $this->assertEquals(3, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(1, $this->feed->getCountOfPostsInFeed(1, true));
        $this->assertEquals(2, $this->feed->getCountOfPostsInFeed(1, false));

        $arResultUser = $this->feed->getFeed(1, null, 10, true);
        $this->assertEquals(array(
            "countOfBasket"=>1,
            "countOfPosts"=>1,
            "postIds"=>array(
                1
            ),
            "numberOfBasket"=>1,
            "basketSize"=>10,
            "isOwnerFilter"=>true
        ),$arResultUser );

        $arResultUser = $this->feed->getFeed(1, null, 10, false);

        $arResultUser["postIds"] = array_sum($arResultUser["postIds"]);
        $this->assertEquals(array(
            "countOfBasket"=>1,
            "countOfPosts"=>2,
            "postIds"=>array_sum(array(
                2,3
            )),
            "numberOfBasket"=>1,
            "basketSize"=>10,
            "isOwnerFilter"=>false
        ),$arResultUser );



        $this->assertEquals(0, $this->feed->getCountOfPostsInFeed(150));
        $this->feed->addPost(EntityPost::create(array("id"=>4, "author_id"=>150, 'created_at'=>'2010-01-01 12:12:10')));
        $this->feed->addPost(EntityPost::create(array("id"=>5, "author_id"=>1, 'created_at'=>'2010-01-01 12:12:10')),150, true);

        $this->assertEquals(3, $this->feed->getCountOfPostsInFeed(1));
        $this->assertEquals(2, $this->feed->getCountOfPostsInFeed(150));

        $this->assertEquals(1, $this->feed->getCountOfPostsInFeed(150, false));
    }

}
