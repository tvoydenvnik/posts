<?php

namespace Tvoydenvnik\Posts\Tests;

use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Constants\PostTypes;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsCacheTarantool;


class PostsCacheTarantoolTest  extends \PHPUnit_Framework_TestCase{



    /**
     * @var PostsCacheTarantool
     */
    private $cacheTarantool = null;

    public function setUp()
    {

        $cache = new PostsCacheTarantool();
        $cache->setConnection(FactoryDefault::getDefault()->get('tarantool'));

        $this->cacheTarantool =  $cache;

    }

    public function tearDown()
    {
        $this->cacheTarantool = null;
    }


    public function testTruncate(){
        $this->cacheTarantool->truncate();
        $this->assertEquals($this->cacheTarantool->spaceLength(), 0);
    }



    public function testGetPosts_Empty(){
        $arResult = $this->cacheTarantool->getPostsFromCache(array(1, 2, 3));
        $this->assertEquals($arResult, array(
            'posts'=>array(),
            'lastComments' => array (),
            'needAccounts' => array (),
            'notInCache'=>array(1, 2, 3),
            'lastPhotos' => Array (),
            'accounts' => array ()
        ));
        
    }

    public function testById_Empty(){
        $arResult = $this->cacheTarantool->byId(1);
        $this->assertEquals(false, $arResult);

    }

    public function testIsExist_Empty(){
        $arResult = $this->cacheTarantool->isExist(1);
        $this->assertEquals(false, $arResult);

    }

    private function getEntity($id, $postType = 1, $parentId = null){
        return $entity = EntityPost::create(
            array("id"=>$id,
                "author_id"=>150,
                "parent_id"=>$parentId,
                "post_type_id"=>$postType,
                "title"=>"Мясо маринованное киви",
                "message"=>array(array("block"=>1, "value"=>"sss"),"some русский текст text! "),
                "sections"=>array(1,2,4),
                "params"=> array("some_data"=>array(1,2,3)),
                "like_pos"=>1,
                "like_neg"=>111,
                "reposts"=>22,
                "views"=>222,
                "favorite"=>3232,
                "external_server"=>1,
                "external_id"=>33,
                "external_date"=>"2015-01-01",
                //"comments"=>1,
                "joined"=>33,
                "created_at"=>"2015-01-01",
                "updated_at"=>"2015-01-01",
            ));
    }
    public function testAdd(){

        $entity = $this->getEntity(12);


        $this->cacheTarantool->addPost($entity);

        $arResult = $this->cacheTarantool->getPostsFromCache(array(155, 12, 365, 12));

        $this->assertEquals(array(155, 365), $arResult['notInCache']);

        $this->assertEquals(PostsCacheTarantool::entityPostToArray($entity), PostsCacheTarantool::entityPostToArray($arResult['posts'][12]));

        $this->assertEquals(1, $this->cacheTarantool->spaceLength());

    }

    /**
     * Повторное добавление одного итогоже соообщения, не вызовет ошибки
     */
    public function testAddDouble()
    {

        $entity = EntityPost::create(
            array("id"=>150,
                "author_id"=>2,
                "message"=>"[b][[lin1
[b]\nline2\nline]]3",

            ));

        $this->cacheTarantool->addPost($entity);
        $this->cacheTarantool->addPost($entity);

        $arResult = $this->cacheTarantool->getPostsFromCache(array(150));

        $this->assertEquals(PostsCacheTarantool::entityPostToArray($arResult['posts'][150]), PostsCacheTarantool::entityPostToArray($entity), "Повторное добавление одного итогоже соообщения, не вызовет ошибки");
    }


    public function testById(){
        $arResult = $this->cacheTarantool->byId(12);
        $this->assertEquals($this->getEntity(12)->postToArray(), $arResult->postToArray());
    }

    public function testIsExist(){
        $arResult = $this->cacheTarantool->isExist(1);
        $this->assertEquals(false, $arResult);

        $arResult = $this->cacheTarantool->isExist(12);
        $this->assertEquals(true, $arResult);
    }


    /**
     * Просто тестируем добавление несколько сообщений
     */
    public function testAddMulty(){

        $this->cacheTarantool->truncate();
        $this->assertEquals($this->cacheTarantool->spaceLength(), 0);

        for($i=1;$i<=100; $i++){
            $entity = $this->getEntity($i);
            $this->cacheTarantool->addPost($entity);
        }


        $this->assertEquals($this->cacheTarantool->spaceLength(), 100);
    }


    public function testAddComments(){

        $this->cacheTarantool->truncate();
        $this->assertEquals($this->cacheTarantool->spaceLength(), 0);

        $mainPost = $this->getEntity(51);
        $comment = $this->getEntity(151, PostTypes::$cPOST_TYPE_COMMENT,  51);
        $comment2 = $this->getEntity(152, PostTypes::$cPOST_TYPE_COMMENT,  51);
        $comment3 = $this->getEntity(153, PostTypes::$cPOST_TYPE_COMMENT,  51);
        $comment4 = $this->getEntity(154, PostTypes::$cPOST_TYPE_COMMENT,  51);

        $this->cacheTarantool->addPost($mainPost);

        $updated_at_time = $this->cacheTarantool->getPostsFromCache(array(51))["posts"][51]->data["updated_at_time"];
        $this->cacheTarantool->addPost($comment);
        $updated_at_time2 = $this->cacheTarantool->getPostsFromCache(array(51))["posts"][51]->data["updated_at_time"];
        $this->assertEquals(true, $updated_at_time2>$updated_at_time, "При добавлении комментария, время должно измениться.");

        $this->cacheTarantool->addPost($comment2);
        $this->cacheTarantool->addPost($comment3);
        $this->cacheTarantool->addPost($comment4);

        //$arResult = $this->cacheTarantool->getPostsFromCache(array(51, 151, 152, 153, 154));


        $parentPost = $this->cacheTarantool->getPostsFromCache(array(51))["posts"][51];
        $this->assertEquals(4, $parentPost->getComments(), "При добавлении комментариев, у родителя увеличивается их кол-во.");
        $this->assertEquals(array(152, 153, 154), $parentPost->getLastComments(), "При добавлении комментариев, у родителя заполняются последние комментарии, но не более 3.");


        $arResult = $this->cacheTarantool->getPostsFromCache(array(51));
        //$parentPost = $this->cacheTarantool->getPostsFromCache(array(51))["posts"][51];

        $this->assertEquals(153, $arResult["lastComments"][153]->getId(), "Должны присутсвовать последние комментарии, для родителя.");
        $this->assertEquals(154, $arResult["lastComments"][154]->getId());
        $this->assertEquals(152, $arResult["lastComments"][152]->getId());

      //  $this->assertEquals(array(), $arResult);
    }


    public function testAddPhotos(){
        $this->cacheTarantool->truncate();
        $this->assertEquals($this->cacheTarantool->spaceLength(), 0);

        $mainPost = $this->getEntity(51, PostTypes::$cPOST_TYPE_PHOTO_ALBUM);
        $comment = $this->getEntity(151, PostTypes::$cPOST_TYPE_PHOTO_ALBUM_ITEM,  51);
        $comment2 = $this->getEntity(152, PostTypes::$cPOST_TYPE_PHOTO_ALBUM_ITEM,  51);
        $comment3 = $this->getEntity(153, PostTypes::$cPOST_TYPE_PHOTO_ALBUM_ITEM,  51);
        $comment4 = $this->getEntity(154, PostTypes::$cPOST_TYPE_PHOTO_ALBUM_ITEM,  51);

        $this->cacheTarantool->addPost($mainPost);
        $this->cacheTarantool->addPost($comment);
        $this->cacheTarantool->addPost($comment2);
        $this->cacheTarantool->addPost($comment3);
        $this->cacheTarantool->addPost($comment4);

        $parentPost = $this->cacheTarantool->getPostsFromCache(array(51))["posts"][51];
        $this->assertEquals(0, $parentPost->getComments());
        $this->assertEquals(array(152, 153, 154), $parentPost->getLastPhotos(), "При добавлении комментариев, у родителя заполняются последние фотографии, но не более 3.");

        $arResult = $this->cacheTarantool->getPostsFromCache(array(51));
        $this->assertEquals(153, $arResult["lastPhotos"][153]->getId(), "Должны присутсвовать последние фото, для родителя.");
        $this->assertEquals(154, $arResult["lastPhotos"][154]->getId());
        $this->assertEquals(152, $arResult["lastPhotos"][152]->getId());

    }


    public function testUpdate(){

        $this->cacheTarantool->truncate();

        $entity = $this->getEntity(12);
        $this->cacheTarantool->addPost($entity);


        $entity->setAuthorId(55555);

        $lResult = $this->cacheTarantool->updatePost($entity, false);
        $this->assertEquals(true,  $lResult->hasError(), "автор не совпадает");


        $entity->setAuthorId(150);
        $entity->setTitle("Новый заголовок");
        $entity->setParams(array("data2"=>true));
        $entity->setMessage(array("Новое сообщение"));
        $entity->setAttachedImages(array("1.gif"));

        $lResult = $this->cacheTarantool->updatePost($entity, false);
        $this->assertEquals(false, $lResult->hasError());


        /**
         * @var $entity EntityPost
         */
        $entityUpdate = $lResult->getResult()["entity"];

        $this->assertEquals($entity->getTitle(),$entityUpdate->getTitle());
        $this->assertEquals($entity->getMessage(),$entityUpdate->getMessage());
        $this->assertEquals($entity->getAttachedImages(),$entityUpdate->getAttachedImages());
        $this->assertEquals(array("data2"=>true, "some_data"=>array(1,2,3)),$entityUpdate->getParams());

        return;

        $this->assertEquals(array(1,2,3),$this->repository->getPosts(array(self::$_lastId), false)[0]->getSections());

        $this->assertEquals(array("Новое сообщение"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getMessage());
        $this->assertEquals(1,$this->repository->getPosts(array(self::$_lastId), false)[0]->getPostTypeId());
        $this->assertEquals(array("/path/2.gif"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getAttachedImages());
        $this->assertEquals(array("params1"=>1, 'params2' => 2),$this->repository->getPosts(array(self::$_lastId), false)[0]->getParams());


        return;
        $this->assertEquals($post->getSections(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getSections());
        $this->assertEquals($post->getTitle(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getTitle());
        $this->assertEquals($post->getMessage(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getMessage());
        $this->assertEquals($post->getPostTypeId(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getPostTypeId());
        $this->assertEquals($post->getAttachedImages(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getAttachedImages());

        //обновим повторно, чтобы проверить, что параметры не затираются при update
        $post = EntityPost::create(array(
            "author_id"=>1,
            "id"=>self::$_lastId,
            "params"=>array("params2"=>2),
            "attached_images"=>array("/path/2.gif")
        ));
        $this->assertEquals(true,  $this->repository->updatePost($post));



        $this->assertEquals(array("params1"=>1, 'params2' => 2),$this->repository->getPosts(array(self::$_lastId), false)[0]->getParams());


        $this->assertEquals(array(1,2,3),$this->repository->getPosts(array(self::$_lastId), false)[0]->getSections());
        $this->assertEquals("Новый заголовок",$this->repository->getPosts(array(self::$_lastId), false)[0]->getTitle());
        $this->assertEquals(array("Новое сообщение"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getMessage());
        $this->assertEquals(1,$this->repository->getPosts(array(self::$_lastId), false)[0]->getPostTypeId());
        $this->assertEquals(array("/path/2.gif"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getAttachedImages());
        $this->assertEquals(array("params1"=>1, 'params2' => 2),$this->repository->getPosts(array(self::$_lastId), false)[0]->getParams());

//        //todo
//        $entity = $this->getEntity(12);
//        $entity->setTitle("Новое наименование");
//        $this->cacheTarantool->addPost($entity);
//        $this->cacheTarantool->addPost($entity);
//        $this->cacheTarantool->addPost($entity);
//
//
//        $arResult = $this->cacheTarantool->byId(12);
//
//        $this->assertEquals($arResult->postToArray(), $entity->postToArray());
    }


//    /*
//     * Каждое обновление, получение из кэша делает инкремент __expired
//     */
//    public function testExpiredInc(){
//        $this->cacheTarantool->truncate();
//
//        $entity = $this->getEntity(12);
//        $this->cacheTarantool->add($entity); //__expired:1
//        $this->assertEquals(1, $this->cacheTarantool->byId(12)->data["__expired"]);
//
//        $this->cacheTarantool->add($entity); //__expired:3
//        $this->assertEquals(3, $this->cacheTarantool->byId(12)->data["__expired"]);
//        $this->assertEquals(4, $this->cacheTarantool->byId(12)->data["__expired"]);
//        $this->assertEquals(5, $this->cacheTarantool->byId(12)->data["__expired"]);
//    }

//
//    public function te9stDeleteItemsByExpired(){
//        $this->cacheTarantool->truncate();
//
//
//        for($i=1;$i<=100; $i++){
//            $entity = EntityPost::create(array("id"=>$i));
//            $this->cacheTarantool->addPost($entity);
//            $this->cacheTarantool->addPost($entity);
//
//            $rand = rand(1, 25);
//            for($k=1;$k<$rand; $k++){
//                $this->cacheTarantool->byId($entity->getId());
//            }
//
//        }
//
//
//        $this->cacheTarantool->deleteItemsByExpired(50);
//        $this->assertEquals(50, $this->cacheTarantool->spaceLength());
//    }



    public function testGetPostsPerformance(){

        $this->cacheTarantool->truncate();

        $entity = $this->getEntity(12);

        $count = 1000;
        $func = PostsCacheTarantool::getLuaFuncGetPosts(false);
        $bigText = $this->_getText();
        $eval = <<<EOT
        
            $func
           for i=1,$count,1 do
                box.space.posts_v3:insert({i, i, i-1, i, 'где будем делать талию?))', '$bigText', {1,3,4}, {params=1}, 1, 1,123,2,nill, 11, 11, nill, nill});
                local data = {i, i, i};
               
                local test = __PostsCacheTarantoolGetPostsV1(data, true, true, true);
           end

EOT;
        $this->assertTime(0.2, function() use ($eval, $count){

            $this->cacheTarantool->_connection->evaluate($eval);
            $this->assertEquals($count, $this->cacheTarantool->spaceLength());
//            for($i=0;$i<10000; $i++){
//                $arResult =  $arResult = $this->cacheTarantool->getPostsFromCache(array(12));
//
//            }
        });





    }

    public function testTruncate2(){
        $this->cacheTarantool->truncate();
        $this->assertEquals($this->cacheTarantool->spaceLength(), 0);
    }


    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }


    private function _getText(){

            $eval = <<<EOT
        
          Если вы решили начать бегать, то научитесь это делать правильно!

Итак, вы решили начать строить тело своей мечты. Самое простое, с чего можно начать – это бег. Это доступный вид аэробной нагрузки, рассчитанный на похудение и активизацию всех мышц нашего организма. Полезные аспекты: это и тренировка сердца, улучшение кровообращения, а, соответственно, и снабжение клеток кислородом, благотворное влияние на имунную систему, центральную нервную систему, помощь в борьбе с депрессиями и многое другое.

Но в нашем современном и высокотехнологичном мире начали забывать (а кто-то вообще не знал), как правильно нужно бегать. Потому что все модные производители кроссовок навязали свои правила бега. Да, может результаты и достижения отдельных людей радуют, но цена этих результатов - больные суставы и возможные проблемы с позвоночником. Современная обувь предполагает бег с приземлением на пятку, что совершенно расходится с естественной техникой бега. Поэтому на большинстве моделей беговых кроссовок пятка идет с амортизирующими свойствами, а колодка - со стабилизирующими.

Вот основные принципы естественной техники бега:

приземляться нужно аккуратно сначала на широкую фронтальную часть стопы под центром тяжести, а не перед ним. Потом вся стопа мягко приземляется и происходит легкое быстрое касание пяткой
колени всегда слегка согнуты, чтобы был эффект пружины, угол в голеностопе – естественный, носок тянуть не нужно
бежать нужно плавно и легко
руки расслаблены, в локте угол приближен к прямому, при движении руки не пересекают центральную линию (середину грудной клетки)
плечи расслаблены и опущены, осанка ровная, пресс слегка напряжен
старайтесь держать постоянный темп (оптимальный каденс (частота шагов) – 180 в минуту), делать амплитудные движения, без прыжков вверх-вниз
голова находится в естественном положении, на нужно ее опускать вниз, запрокидывать назад
чем быстрее ваш темп, тем больше наклоняйтесь вперед.

Ваши энергозатраты при естественной технике бега будут ниже, а, соответственно, вы будете способны бежать дольше и с меньшей усталостью.

Естественность – вот ключ к успеху, это утверждение можно применять не только к бегу, но и следовать ему в повседневной жизни. То есть, чтобы правильно бегать, попробуйте это делать босиком или в обуви с тонкой подошвой. Ведь в Кении и Эфиопии дети растут без обуви. Тем самым у них вырабатывается правильная техника и в результате они становятся выдающимися бегунами. Поэтому учите своих детей бегать правильно, чтобы не развивалось плоскостопия, проблем с суставами и позвоночником, потому что именно в детском возрасте формируются и привычки и структура тела. Ваши дети будут вам благодарны, а вы уверены в своем здоровом потомстве.

Всем здоровья и благополучия)

С подробностями можно ознакомиться самостоятельно:
Дэнни Дрейер «Ци-бег»
Дэнни Эбшир «Естественный бег»
Николай Романов «Позный метод бега»
Николай Романов «Бегайте быстрее, дольше и без травм»
Гордон Пири «Бегай быстро и без травм» 

EOT;

        $eval = urlencode($eval);
        return $eval;
    }





}