<?php

namespace Tvoydenvnik\Posts\Tests\Simple;


use Phalcon\Di\FactoryDefault;
use Tvoydenvnik\Posts\Entity\EntityPost;
use Tvoydenvnik\Posts\Models\Posts\PostsMySqlRepository;


class PostsMySqlRepositoryTest extends \PHPUnit_Framework_TestCase{


    /**
     * @var PostsMySqlRepository
     */
    private $repository = null;

    public static function setUpBeforeClass(){
        $repository = new PostsMySqlRepository();
        $repository->truncate();
    }

    public static function tearDownAfterClass(){
        $repository = new PostsMySqlRepository();
        //$repository->truncate();
    }

    public function setUp()
    {
        $repository = new PostsMySqlRepository();
        $this->repository =  $repository;
    }

    public function tearDown()
    {
        $this->repository = null;
    }

    public function testGetPostsEmpty(){

        $arResult = $this->repository->getPosts(array(156487878, "fdks232sz"));
        $this->assertEquals(0, count($arResult));

        $arResult = $this->repository->getPosts(array(156487878, "fdks232sz"), false);
        $this->assertEquals(0, count($arResult));

    }



    private static $_lastId = 0;
    public function testAddPost(){

        $post = EntityPost::create(array(
            "author_id"=>1
        ));

        $this->repository->addPost($post);

        $this->assertGreaterThan(0, $post->getId());
        $this->assertNotEmpty($post->getCreatedAt());

        self::$_lastId = $post->getId();

    }

    public function testGetPosts(){


        $arResult = $this->repository->getPosts(array(self::$_lastId, self::$_lastId, "sdsdsds"));

        $this->assertEquals(1, count($arResult));
        $this->assertEquals(self::$_lastId, $arResult[self::$_lastId]->getId());


    }

    public function testUpdatePost(){


        $post = EntityPost::create(array(
            "author_id"=>1,
            "id"=>self::$_lastId,
            "sections"=> "[1,2,3]",
            "title"=>"Новый заголовок",
            "message"=>array("Новое сообщение"),
            "post_type_id"=>1,
            "params"=>array("params1"=>1),
            "attached_images"=>array("/path/1.gif")
        ));

        $lResult = $this->repository->updatePost($post);
        $this->assertEquals(false,  $lResult->hasError());
        $this->assertEquals($post->getSections(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getSections());
        $this->assertEquals($post->getTitle(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getTitle());
        $this->assertEquals($post->getMessage(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getMessage());
//        $this->assertEquals($post->getPostTypeId(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getPostTypeId());
        $this->assertEquals($post->getAttachedImages(),$this->repository->getPosts(array(self::$_lastId), false)[0]->getAttachedImages());

        //обновим повторно, чтобы проверить, что параметры не затираются при update
        $post = EntityPost::create(array(
            "author_id"=>100,//ИЗМЕНИЛ
            "id"=>self::$_lastId,
            "params"=>array("params2"=>2),
            "attached_images"=>array("/path/2.gif")
        ));

        $arResult = $this->repository->updatePost($post);
        $this->assertEquals(true, $arResult->hasError(),"Пользователь не совпадает с автором" );


        $post = EntityPost::create(array(
            "author_id"=>1,
            "id"=>self::$_lastId,
            "params"=>array("params2"=>2),
            "attached_images"=>array("/path/2.gif")
        ));

        $arResult = $this->repository->updatePost($post);
        $this->assertEquals(false, $arResult->hasError() );



        $this->assertEquals(array("params1"=>1, 'params2' => 2),$this->repository->getPosts(array(self::$_lastId), false)[0]->getParams());


        $this->assertEquals(array(1,2,3),$this->repository->getPosts(array(self::$_lastId), false)[0]->getSections());
        $this->assertEquals("Новый заголовок",$this->repository->getPosts(array(self::$_lastId), false)[0]->getTitle());
        $this->assertEquals(array("Новое сообщение"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getMessage());
//        $this->assertEquals(1,$this->repository->getPosts(array(self::$_lastId), false)[0]->getPostTypeId());
        $this->assertEquals(array("/path/2.gif"),$this->repository->getPosts(array(self::$_lastId), false)[0]->getAttachedImages());
        $this->assertEquals(array("params1"=>1, 'params2' => 2),$this->repository->getPosts(array(self::$_lastId), false)[0]->getParams());

    }

    public function testMarkDeleted(){



        $arResult = $this->repository->getPosts(array(self::$_lastId), false);

        $this->assertEquals(1, count($arResult));
        $this->assertEquals(0, $arResult[0]->getDeleted());



        //мометим на удаление
        $this->assertEquals(true, $this->repository->markDeleted(self::$_lastId, 1),"мометим на удаление");

        $this->assertEquals(1, $this->repository->getPosts(array(self::$_lastId), false)[0]->getDeleted());


        $this->assertEquals(true, $this->repository->unMarkDeleted(self::$_lastId, 1, false, false),"снимем пометку удаления");

        $this->assertEquals(0, $this->repository->getPosts(array(self::$_lastId), false)[0]->getDeleted());

        //попытаемся пометить на удаление, но автор другой пользователь
        $this->assertEquals(false, $this->repository->markDeleted(self::$_lastId, 2), "попытаемся пометить на удаление, но автор другой пользователь");

        //попытаемся пометить на удаление, и успешно, т.к. права администратора
        $this->assertEquals(true, $this->repository->markDeleted(self::$_lastId, 2, true),
            "попытаемся пометить на удаление, и успешно, т.к. права администратора");
        $this->assertEquals(1, $this->repository->getPosts(array(self::$_lastId), false)[0]->getDeleted());



    }






    public function testRepostsIncrement(){
        $this->assertEquals(1, $this->repository->repostsIncrement(self::$_lastId));
        $this->assertEquals(2, $this->repository->repostsIncrement(self::$_lastId));
    }
    public function testRpostsDecrement(){
        $this->assertEquals(1, $this->repository->repostsDecrement(self::$_lastId));
        $this->assertEquals(0, $this->repository->repostsDecrement(self::$_lastId));
    }


//    public function testUpdateViews(){
//        $this->assertNotEquals(false, $this->repository->updateViews(self::$_lastId, 100));
//        $this->assertEquals(100, $this->repository->getPosts(array(self::$_lastId), false)[0]->getViews());
//    }

    public function testDeleteById(){

        $this->assertEquals(true, $this->repository->deleteById(self::$_lastId));

    }


//    public function testGetPosts(){
//
//        $arResult = $this->repository->getPosts(array(14139, 1792, 36836, 4586, "fdks232sz"));
//
//        $this->assertEquals(4, count($arResult));
//
//        $this->assertEquals("Полезные Свойства Фрукта Свити", $arResult[0]->getTitle());
//        $this->assertEquals("Дневник питания и тренировок за 24.05.2012", $arResult[1]->getTitle());
//        $this->assertEquals("Рецепт Салат с адыгейским сыром ", $arResult[2]->getTitle());
//        $this->assertEquals("Рецепт бурито", $arResult[3]->getTitle());
//        $this->assertEquals(json_decode('{"__error":"\u21161231929у129","food":{"id":72112,"name":"Рецепт бурито","name_group":"бутерброд","units":[],"nutrients":{"68":1.3,"74":0.472,"11":160.4,"12":65,"13":10.7,"14":6.6,"15":14.6,"18":1,"19":0.5,"84":4.1747,"85":1110.4,"86":0.54,"23":0.33,"24":0.435,"25":0.209,"26":35.639,"27":12.43,"29":0.218,"22":0.119,"31":2.454,"32":300.12,"33":175.33,"35":30.34,"36":380.09,"37":83.04,"38":186.8,"39":160.51,"41":25.3,"42":1.42,"43":2.73,"44":3.969,"45":0.1106,"46":91.24,"47":5.896,"50":6.3,"52":2.97,"53":1.659,"55":21.3,"56":1.646,"57":25.93,"58":6.375,"62":0.132},"recipe_description":"","id_base_unit":0,"recipe":{"weight_cooking":1791,"weight_raw":1945,"food":[{"cc":0,"fn":"Лаваш армянский тонкий (пшеничная мука 1 сорта)","fs":{"id_s":0,"id":21272},"w":300},{"cc":0,"fn":"фасоль с грибами","fs":{"id_s":1,"id":68457},"w":332},{"cc":7,"fn":"Помидоры (томаты) (Пассерование)","fs":{"id_s":0,"id":397},"w":100},{"cc":0,"fn":"Бобы (фасоль фава), зрелые семена, консервированные","fs":{"id_s":0,"id":18391},"w":50},{"cc":7,"fn":"Лук репчатый (Пассерование)","fs":{"id_s":0,"id":186},"w":100},{"cc":7,"fn":"Лук порей (Пассерование)","fs":{"id_s":0,"id":188},"w":100},{"cc":7,"fn":"Чеснок луковица (Пассерование)","fs":{"id_s":0,"id":436},"w":10},{"cc":0,"fn":"Перец черный молотый","fs":{"id_s":0,"id":21224},"w":1},{"cc":0,"fn":"Перец молотый красный","fs":{"id_s":0,"id":21227},"w":1},{"cc":0,"fn":"Сыр голландский, брусковый","fs":{"id_s":0,"id":1091},"w":275},{"cc":0,"fn":"Шампиньоны","fs":{"id_s":0,"id":440},"w":154},{"cc":0,"fn":"Сладкая молочная кукуруза, желтая, консервированная, без добавления соли","fs":{"id_s":0,"id":17180},"w":125},{"cc":4,"fn":"Печень куриная (Тушение)","fs":{"id_s":0,"id":275},"w":150},{"cc":0,"fn":"Куриный фарш, рубленный, приготовленный на пару","fs":{"id_s":0,"id":14831},"w":151},{"cc":0,"fn":"Огурец соленый","fs":{"id_s":0,"id":244},"w":87},{"cc":0,"fn":"Масло оливковое","fs":{"id_s":0,"id":249},"w":9}]},"type_of_food":1,"id_nature":0,"id_account":80947,"deleted":0,"translit":"burito","post_id":36836,"url_of_post":"\/people\/user\/80947\/blog\/36836\/"},"HAS_IMAGES":"N"}', true), $arResult[3]->getParams());
//
//    }
//
//
//    public function testGetPostsPerformance(){
//
//        $this->assertTime(1.5, function(){
//            for($i=0;$i<10000; $i++){
//                $arResult = $this->repository->getPosts(array(14139));
//
//            }
//        });
//
//
//    }


    /**
     * assert that a given callback function does not need more time to execute
     * than the given $maxTimeInSeconds
     *
     * @param int $maxTimeInSeconds max time the execution is allowed to last
     * @param Closure $callback a closure to be measured
     *
     * @return void
     *
     * @throws Exception when $callBack is not callable
     */
    protected function assertTime($maxTimeInSeconds, \Closure $callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('no valid callback given');
        }

        $startTime = microtime(true);
        $callback();
        $time = microtime(true) - $startTime;

        $this->assertLessThanOrEqual(
            $maxTimeInSeconds,
            $time,
            sprintf(
                'failed asserting that execution does not need longer than %f seconds, needed %f seconds',
                $maxTimeInSeconds,
                $time
            )
        );
    }

}