<?php
namespace Tvoydenvnik\Posts\Tests\Simple;


use Tvoydenvnik\Posts\Entity\EntityPost;

class EntityPostTest extends \PHPUnit_Framework_TestCase{

    public function test1Test(){
        $this->assertEquals(true, true);
    }

    public function testGetId(){

        $entity = EntityPost::create(array("id"=>"1"));
        $this->assertEquals($entity->getId(), 1);
        $entity->setId(3);
        $this->assertEquals($entity->getId(), 3);

    }

    public function testAuthorId(){

        $entity = EntityPost::create(array("author_id"=>"1"));
        $this->assertEquals($entity->getAuthorId(), 1);
        $entity->setAuthorId(3);
        $this->assertEquals($entity->getAuthorId(), 3);

    }

    public function testSetSections(){

        $data = array(1,2,3,3,1);
        $data2 = array(1,2,3);

        $entity = EntityPost::create(array());
        $entity->setSections($data);
        $this->assertEquals($data2, $entity->getSections());

        $entity = EntityPost::create(array());
        $entity->setSections(json_encode($data));
        $this->assertEquals($data2, $entity->getSections());

        $this->assertEquals(json_encode($data2), $entity->getSectionsForSaveInRepository());
    }

    public function testSetParams(){
        $data = array("params"=>true);
        $entity = EntityPost::create(array());
        $entity->setParams($data);
        $this->assertEquals($data, $entity->getParams());

        $entity = EntityPost::create(array());
        $entity->setParams(json_encode($data));
        $this->assertEquals($data, $entity->getParams());


        $this->assertEquals(json_encode($data), $entity->getParamsForSaveInRepository());

    }


    public function testSetAttachedImages(){

        $data = array("/test/1.gif");
        $entity = EntityPost::create(array());
        $entity->setAttachedImages($data);
        $this->assertEquals($data, $entity->getAttachedImages());

        $entity = EntityPost::create(array());
        $entity->setAttachedImages(json_encode($data));
        $this->assertEquals($data, $entity->getAttachedImages());


        $this->assertEquals(json_encode($data), $entity->getAttachedImagesForSaveInRepository());

    }


    //todo Добавить все тесты
}